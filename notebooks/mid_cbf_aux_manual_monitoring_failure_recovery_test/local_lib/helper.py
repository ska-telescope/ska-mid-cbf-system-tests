import logging
import os
import subprocess
import sys
import time
from typing import Any

import yaml
from assertpy import assert_that
from ska_tango_testing.integration import TangoEventTracer

abs_file_dir_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(abs_file_dir_path, "..", "..", "..", "tests"))

# Ignore linting for top of module import due to need to include namespace
# of tests first
from lib import powerswitch, utils  # noqa: E402 pylint: disable=E0401,C0413


def get_tango_host(namespace: str) -> str:
    """Returns tango host address"""
    cmd = (
        f"kubectl -n {namespace}"
        + " get svc databaseds-tango-base --output jsonpath='{.status.loadBalancer.ingress[0].ip}'"
    )
    res = subprocess.run(cmd.split(), stdout=subprocess.PIPE, check=False)
    ip_addr = res.stdout.decode().replace("'", "")
    tango_host = f"{ip_addr}:10000"
    return tango_host


def ping_talon_boards(talon_list: str, hw_config: str) -> None:
    """Pings relevant talon boards and prints results"""
    number_of_ping_attempts = 1
    talon_ping_results = utils.return_talon_ping_results(
        talon_list, number_of_ping_attempts, hw_config
    )
    for ping_result in talon_ping_results:
        print(ping_result.replace("\n", ""))


def robust_lru_on(
    lru: str,
    hw_config: str,
    logger: logging.Logger,
) -> None:
    """Turn talon on and after a delay if talon is still not on retry max_attempts times"""
    max_attempts = 5
    delay_s = 20

    for i in range(max_attempts):
        logger.info(f"Attempt {i+1} to turn LRU on")
        powerswitch.turn_lrus_on_off([lru], "on", hw_config)
        time.sleep(delay_s)
        if powerswitch.lru_status(lru, hw_config):
            return

    raise AssertionError("LRU did not successfully power on")


def robust_talon_qspi_update(
    script_dir: str,
    fpga_image_path: str,
    talon_target: str,
    talon_ip: str,
    logger: logging.Logger,
):
    """Try twice to program qspi."""
    update_qspi_status_str = f"{os.path.join(script_dir, 'update_qspi_remote.sh')} {fpga_image_path} {talon_target} {talon_ip} software 2>&1"
    logger.info(update_qspi_status_str)
    logger.info(f"Attempt 1 to update talon {talon_ip} QSPI")
    result = subprocess.run(
        [update_qspi_status_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )
    if result.returncode != 0:
        logger.info(f"Attempt 1 stdout: {result.stdout}")
        logger.info(f"Attempt 2 to update talon {talon_ip} QSPI")
        result = subprocess.run(
            [update_qspi_status_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )
        if result.returncode != 0:
            raise AssertionError("Talon {talon_ip} failed to update")


def get_sv_ip(namespace: str):
    """Return ip of sv pod, for the purpose of dumping data to a known place"""
    kube_exec_str = f"kubectl get pod sv -n {namespace} -o yaml"

    pod_yaml_proc = subprocess.run(
        [kube_exec_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )

    pod_yaml = yaml.safe_load(pod_yaml_proc.stdout)

    return pod_yaml["status"]["podIP"]


class TracerChecker:
    """Checks given tracer for successful commands and state changes"""

    def __init__(self, tracer: TangoEventTracer):
        self.tracer = tracer

    def check_lrc_command_complete_w_state_change(
        self,
        result: subprocess.CompletedProcess,
        device_name: str,
        command_str: str,
        state_str: str,
        new_state: Any,
        timeout_state_change: int,
        timeout_lrc: int,
    ) -> None:
        """Check Long Running Command completes and a state change occurs"""
        assert_that(self.tracer).within_timeout(
            timeout_state_change
        ).has_change_event_occurred(
            device_name=device_name,
            attribute_name=state_str,
            attribute_value=new_state,
        )

        assert_that(self.tracer).within_timeout(
            timeout_lrc
        ).has_change_event_occurred(
            device_name=device_name,
            attribute_name="longRunningCommandResult",
            attribute_value=(
                f"{result[1][0]}",
                f'[0, "{command_str} completed OK"]',
            ),
        )

    def check_lrc_command_complete(
        self,
        result: subprocess.CompletedProcess,
        device_name: str,
        command_str: str,
        timeout: int,
    ) -> None:
        """Check Long Running Command completes"""
        assert_that(self.tracer).within_timeout(
            timeout
        ).has_change_event_occurred(
            device_name=device_name,
            attribute_name="longRunningCommandResult",
            attribute_value=(
                f"{result[1][0]}",
                f'[0, "{command_str} completed OK"]',
            ),
        )
