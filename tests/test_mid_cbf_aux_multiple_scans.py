"""Auxiliary testing for multiple scans"""
import json
import logging
import os
import pprint
import subprocess
import time
import warnings
from typing import List

import pytest
from assertpy import assert_that
from dotenv import load_dotenv
from lib import (
    datacollector,
    get_parameters,
    scan_operations,
    utils,
    vis_pod_operations,
)
from lib.constant import LOG_FORMAT
from pytango_client_wrapper import PyTangoClientWrapper
from pytest_bdd import given, parsers, scenario, then, when
from ska_control_model import ObsState, ResultCode
from ska_tango_testing.integration import TangoEventTracer

load_dotenv()

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)

##########################################################################################################

# two_same_command_scans definitions


@pytest.mark.nightly
@pytest.mark.aux_multiple_scans
@pytest.mark.two_same_command_scans
@scenario(
    "features/mid_cbf_aux_two_same_command_scans_test.feature",
    "Two scans for same scan configuration command",
)
def test_two_scans_for_same_scan_configuration_command(
    test_report_generator,
) -> None:
    """Tests two scans for same scan configuration command."""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.two_same_command_scans_corr_pst
@scenario(
    "features/aa1/mid_cbf_aux_two_same_command_scans_corr_pst_test.feature",
    "Two scans for same scan configuration command (CORR & PST)",
)
def test_two_scans_for_same_scan_configuration_command_corr_pst(
    test_report_generator,
) -> None:
    """Tests two scans for same scan configuration command (CORR and PST)."""


@given(
    parsers.parse(
        "{main_scan_test_id} and {matching_scan_test_id} test variables match"
    )
)
def main_scan_test_id_and_matching_scan_test_id_test_parameters_match(
    original_test_params_dir: str,
    main_scan_test_id: str,
    matching_scan_test_id: str,
) -> None:
    """
    Checks that test variables from main_scan_test_id and matching_scan_test_id match.
    """
    tests_file_path = os.path.join(original_test_params_dir, "tests.json")

    with open(tests_file_path, "r", encoding="utf-8") as tests_file:
        tests_vars = json.load(tests_file)

    main_test_vars = tests_vars["tests"][main_scan_test_id]
    matching_test_vars = tests_vars["tests"][matching_scan_test_id]

    assert set(main_test_vars.keys()) == set(
        matching_test_vars.keys()
    ), f"{main_scan_test_id} and {matching_scan_test_id} must have matching variables."

    for key in main_test_vars.keys():
        if key not in {"description", "scan"}:
            assert (
                main_test_vars[key] == matching_test_vars[key]
            ), f"{main_scan_test_id} and {matching_scan_test_id} must have matching variables, differ on variable {key}."


@when(parsers.parse("the next scan is delayed {delay:d} seconds"))
def the_next_scan_is_delayed_delay_seconds(delay: int) -> None:
    """Delay by delay seconds."""
    time.sleep(delay)
    _logger.info(f"Delayed next scan {delay} seconds")


@when(
    parsers.parse("the visibility pod config is prepared to capture {test_id}")
)
def the_visibility_pod_config_is_prepared_to_capture_test_id(
    namespace: str,
    results_dir: str,
    test_id: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Prepare the visibility pod config file to capture test_id."""
    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    test_params[test_id] = test_param_for_test_id

    utils.save_updated_test_parameters(
        test_params=test_params, test_id=test_id
    )


##########################################################################################################

# two_different_scans_idle_in_between definitions


@pytest.mark.nightly
@pytest.mark.aux_multiple_scans
@pytest.mark.two_different_scans_idle_in_between
@scenario(
    "features/mid_cbf_aux_two_different_scans_idle_in_between_test.feature",
    "Two different scan configurations with an idle in between",
)
def test_two_different_scan_configurations_with_an_idle_in_between(
    test_report_generator,
) -> None:
    """Tests two different scan configurations with an idle in between."""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.two_different_scans_idle_in_between_corr_pst
@scenario(
    "features/aa1/mid_cbf_aux_two_different_scans_idle_in_between_corr_pst_test.feature",
    "Two different scan configurations with an idle in between (CORR & PST)",
)
def test_two_different_scan_configurations_with_an_idle_in_between_corr_pst(
    test_report_generator,
) -> None:
    """Tests two different scan configurations with an idle in between (CORR and PST)."""


##########################################################################################################

# partial_release definitions


@pytest.mark.nightly
@pytest.mark.aux_multiple_scans
@pytest.mark.partial_release
@scenario(
    "features/mid_cbf_aux_partial_release_of_resources_test.feature",
    "Partial release of resources between two scans test",
)
def test_partial_release_of_resources_between_two_scans(
    test_report_generator,
) -> None:
    """Tests a partial release of resources between two scans."""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.partial_release_corr_pst
@scenario(
    "features/aa1/mid_cbf_aux_partial_release_of_resources_corr_pst_test.feature",
    "Partial release of resources between two scans test (CORR & PST)",
)
def test_partial_release_of_resources_between_two_scans_corr_pst(
    test_report_generator,
) -> None:
    """Tests a partial release of resources between two scans (CORR and PST)."""


@when(
    parsers.parse(
        "the CBF executes the command in {test_id} to partially release resources"
    )
)
def the_cbf_executes_the_command_in_test_id_to_partially_release_resources(
    test_id: str, results_dir: str, event_tracer: TangoEventTracer
) -> None:
    """Execute the command in test_id to partially release resources."""

    subarray_proxy = pytest.subarray_proxy

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]
    release_resources = test_param_for_test_id["release_resources"]

    receptors_to_release = release_resources["receptor_ids"]

    current_receptors = subarray_proxy.read_attribute("receptors")
    _logger.info(f"Current receptors are: {current_receptors}")

    _logger.info("Partially removing receptors")

    _logger.info(f"Receptors to release are: {receptors_to_release}")

    result = subarray_proxy.command_read_write(
        "RemoveReceptors", receptors_to_release
    )

    utils.assert_result_code_is_expected(
        "subarray_proxy", "RemoveReceptors", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=pytest.subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.IDLE,
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_LONG
    ).has_change_event_occurred(
        device_name=pytest.subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "RemoveReceptors completed OK"]',
        ),
    )

    remaining_receptors = subarray_proxy.read_attribute("receptors")

    _logger.info(f"Remaining Receptors are: {remaining_receptors}")


##########################################################################################################

# reconfigure_scan definitions


@pytest.mark.nightly
@pytest.mark.aux_multiple_scans
@pytest.mark.reconfigure_scan
@scenario(
    "features/mid_cbf_aux_reconfigure_scan_test.feature",
    "Overwriting a scan configuration test",
)
def test_overwriting_a_scan_configuration(
    test_report_generator,
) -> None:
    """Tests overwriting a scan configuration."""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.reconfigure_scan_corr_pst
@scenario(
    "features/aa1/mid_cbf_aux_reconfigure_scan_corr_pst_test.feature",
    "Overwriting a scan configuration test (CORR & PST)",
)
def test_overwriting_a_scan_configuration_corr_pst(
    test_report_generator,
) -> None:
    """Tests overwriting a scan configuration (CORR and PST)."""


@given(
    parsers.parse(
        "{scan_2_test_id} has the same cbf_input_data as {scan_1_test_id}"
    )
)
def test_scan_2_test_id_has_the_same_cbf_input_data_as_scan_1_test_id(
    original_test_params_dir: str,
    scan_1_test_id: str,
    scan_2_test_id: str,
):
    """
    Check that test parameters from scan_2_test_id and scan_1_test_id match on
    cbf_input_data
    """
    tests_file_path = os.path.join(original_test_params_dir, "tests.json")

    with open(tests_file_path, "r", encoding="utf-8") as tests_file:
        tests_vars = json.load(tests_file)

    scan_1_test_vars = tests_vars["tests"][scan_1_test_id]
    scan_2_test_vars = tests_vars["tests"][scan_2_test_id]

    assert (
        scan_2_test_vars["cbf_input_data"]
        == scan_1_test_vars["cbf_input_data"]
    ), f"{scan_1_test_id} and {scan_2_test_id} must have matching cbf_input_data."


@when(
    parsers.parse(
        "the CBF starts a visibility capture pod and configures for scan 1 for test parameters of {test_id}"
    )
)
def the_cbf_starts_a_visibility_capture_pod_and_configures_for_scan_1_for_test_parameters_of_test_id(
    results_dir: str,
    namespace: str,
    test_id: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Starts a visibility capture pod and configures for scan 1 for test parameters of test_id."""

    vis_pod_operations.create_vis_pod(namespace)

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    lstv_replay_before_configure_scan = True

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    scan_operations.configure_subarray_sequence(
        test_param_for_test_id,
        test_id,
        namespace,
        pytest.subarray_proxy,
        pytest.bite_proxy,
        event_tracer,
        pytest.subarray_fqdn,
        lstv_replay_before_configure_scan,
    )


##########################################################################################################

# Multiple scan test shared functionality


@pytest.fixture
def results_dir_created() -> List[bool]:
    return [False]


def make_check_file_name(test_id: str) -> str:
    """Produce standardized checks file name for multiple scan tests."""
    return f"{test_id}_checkpoints_checks.json"


def capture_data_and_validate_checkpoints(
    test_param_for_test_id: dict,
    test_id: str,
    namespace: str,
    results_dir: str,
    sys_tests_results_dir: str,
    svdc_results_dir: str,
    checks_file_name: str,
    copy_vis_config: bool,
    tango_host: str,
    subarray_proxy: PyTangoClientWrapper,
    event_tracer: TangoEventTracer,
    capture_subprocs: List[subprocess.Popen],
):
    """
    Uses scan_operations and datacollector to capture and validate data for
    checkpoints.
    """
    # enabling the timing of data capture to be based on scv tests
    scv_timing = True

    scan_operations.checkpoint_data_capture(
        test_param_for_test_id,
        namespace,
        subarray_proxy,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        results_dir,
        capture_subprocs,
        scv_timing,
        event_tracer,
    )

    checkpoints = test_param_for_test_id["checkpoints"]
    # complete checkpoints for visibilities, which is now collected
    # copies visibilities zip file to sv for validation
    if "visibilities" in checkpoints.keys():
        datacollector.run_data_collector_for_checkpoint(
            "visibilities",
            checkpoints["visibilities"],
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    scan_operations.validate_checkpoints(
        test_param_for_test_id,
        namespace,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        checks_file_name,
    )


@given(parsers.parse("a report on {test_id} is desired"))
def that_a_report_on_test_id_is_desired(
    test_id: str,
    all_test_ids_for_scenario: List[str],
):
    """Marks test_id for report generation following test execution."""
    _logger.info(
        "system-tests: appending test_id to a list of test ids for report generation"
    )
    all_test_ids_for_scenario.append(test_id)
    _logger.info(
        f"system-tests: test ids for the scenario now include: {all_test_ids_for_scenario}"
    )


@given(
    parsers.parse(
        "{scan_2_test_id} has the same resources and delay models as {scan_1_test_id}"
    )
)
def test_scan_2_test_id_has_the_same_resources_and_delay_models_as_scan_1_test_id(
    original_test_params_dir: str,
    scan_1_test_id: str,
    scan_2_test_id: str,
):
    """
    Check that test parameters from scan_2_test_id and scan_1_test_id match on
    assign_resources and delay_model_config
    """
    tests_file_path = os.path.join(original_test_params_dir, "tests.json")

    with open(tests_file_path, "r", encoding="utf-8") as tests_file:
        tests_vars = json.load(tests_file)

    scan_1_test_vars = tests_vars["tests"][scan_1_test_id]
    scan_2_test_vars = tests_vars["tests"][scan_2_test_id]

    assert (
        scan_2_test_vars["assign_resources"]
        == scan_1_test_vars["assign_resources"]
    ), f"{scan_1_test_id} and {scan_2_test_id} must have matching assign_resources."

    assert (
        scan_2_test_vars["delay_model_config"]
        == scan_1_test_vars["delay_model_config"]
    ), f"{scan_1_test_id} and {scan_2_test_id} must have matching delay_model_config."


@given("the BITE is initiated and test parameters are validated and sent")
def the_bite_is_initiated_and_test_parameters_are_validated_and_sent(
    run_parameter_guardrail_for_k_values,
    validate_test_parameters,
    send_test_parameters_to_cbf_tools,
):
    """Initiate the BITE and validate/send test parameters."""
    pytest.bite_proxy = get_parameters.create_device_proxy(
        "mid_csp_cbf/ec/bite"
    )


def the_cbf_initiates_the_subarrays_for_test_id(test_id: str) -> None:
    """Initiate the subarray(s) for test_id."""
    pytest.subarray_fqdn = get_parameters.get_subarray_fqdn(test_id)
    pytest.subarray_proxy = get_parameters.create_device_proxy(
        pytest.subarray_fqdn
    )


@when("the subarray(s) are sent to EMPTY")
def the_subarrays_are_sent_to_empty(event_tracer: TangoEventTracer) -> None:
    """Send subarray(s)to EMPTY."""
    try:
        scan_operations.subarray_to_empty_state(
            pytest.subarray_proxy, pytest.subarray_fqdn, event_tracer
        )
    except AssertionError as e:
        warnings.warn(f"Failed to send subarray(s) to EMPTY; {e}")


@when(
    parsers.parse(
        "the CBF prepares the subarray(s) for test parameters of {test_id}"
    )
)
def the_cbf_prepares_the_subarrays_for_test_parameters_of_test_id(
    test_id: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Prepare subarray(s) proxies for test parameters of test_id."""
    the_cbf_initiates_the_subarrays_for_test_id(test_id)
    the_subarrays_are_sent_to_empty(event_tracer)


@when(
    parsers.parse(
        "the CBF starts the visibility capture pod and executes scan {scan_no:d} for test "
        "parameters of {test_id}"
    )
)
def the_cbf_starts_the_visibility_capture_pod_and_executes_scan_scan_no_for_test_parameters_of_test_id(
    results_dir_created: List[bool],
    results_dir: str,
    namespace: str,
    scan_no: str,
    test_id: str,
    talon_list: str,
    tango_host: str,
    event_tracer: TangoEventTracer,
) -> None:
    """
    Start the visibility capture pod and executes scan scan_no for test
    parameters of test_id, also creating a results dir if one has not been
    created yet (determined by results_dir_created[0])
    """

    checks_file_name = make_check_file_name(test_id)

    create_sys_test_dir = not results_dir_created[0]

    scan_operations.scan_normal_sequence(
        pytest.subarray_proxy,
        pytest.bite_proxy,
        namespace,
        results_dir,
        test_id,
        talon_list,
        tango_host,
        checks_file_name,
        create_sys_test_dir,
        True,
        event_tracer,
    )

    if create_sys_test_dir:
        results_dir_created[0] = True


@when("the CBF receives the command to go to idle ObsState")
def the_cbf_receives_the_command_to_go_to_idle_obsstate(
    event_tracer: TangoEventTracer,
) -> None:
    """Go to idle ObsState."""

    subarray_proxy = pytest.subarray_proxy
    subarray_fqdn = pytest.subarray_fqdn

    # --- GoToIdle command --- #

    _logger.info(f"Issuing GoToIdle command to {subarray_proxy}")

    result = subarray_proxy.command_read_write("GoToIdle")
    utils.assert_result_code_is_expected(
        "subarray_proxy", "GoToIdle", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.IDLE,
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "GoToIdle completed OK"]',
        ),
    )

    _logger.info("Subarray is in IDLE")


@when(
    parsers.parse(
        "the CBF configures for scan 2 for test parameters of {test_id}"
    )
)
def the_cbf_configures_for_scan_2_for_test_parameters_of_test_id(
    results_dir: str,
    namespace: str,
    test_id: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Configure for scan 2 for test parameters of test_id."""

    utils.talon_bite_stop_replay(pytest.bite_proxy)

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    configure_scan = test_param_for_test_id["configure_scan"]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    utils.generate_bite_data(pytest.bite_proxy, test_id, namespace)
    utils.talon_bite_lstv_replay(pytest.bite_proxy)

    # TODO: Sleep copied from SCV. Figure out if we need this (in SCV and here).
    time.sleep(5)

    # --- ConfigureScan Command --- #

    configure_json = json.dumps(configure_scan)
    configure_string = configure_json.replace("\n", "")
    pprint.pprint(configure_string)
    _logger.info("executing configure scan")

    result = pytest.subarray_proxy.command_read_write(
        "ConfigureScan", configure_string
    )
    utils.assert_result_code_is_expected(
        "subarray_proxy", "ConfigureScan", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=pytest.subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.READY,
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=pytest.subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "ConfigureScan completed OK"]',
        ),
    )


@when(
    parsers.parse(
        "the CBF, on the same visibility capture pod, performs scan 2 for test parameters of {test_id}"
    )
)
def the_cbf_on_the_same_visibility_capture_pod_performs_scan_2_for_test_parameters_of_test_id(
    results_dir: str,
    namespace: str,
    test_id: str,
    tango_host: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Perform scan 2 for test parameters of test_id."""

    # Delete any existing visibilities data in pod
    vis_pod_name = f"visibilities-{pytest.vis_uuid}"

    _logger.info(
        f"Removing existing Visibility Capture files from {vis_pod_name}"
    )
    # sh -c included so that wildcard expansion occurs in pod
    kubectl_rm_str = (
        f"kubectl exec {vis_pod_name} -n {namespace} -- sh -c 'rm ./data/*'"
    )
    _logger.info(kubectl_rm_str)
    result = subprocess.run(
        [kubectl_rm_str],
        shell=True,
        check=False,
    )
    _logger.info(f"vis rm data stderr: {result.stderr}")
    _logger.info(f"vis rm data stdout: {result.stdout}")

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    capture_subprocs = scan_operations.vis_pod_setup_capture_procs(
        test_param_for_test_id,
        namespace,
    )

    sys_tests_results_dir = datacollector.create_data_directory_for_sys_tests(
        namespace, results_dir, test_id
    )

    svdc_results_dir = (
        datacollector.create_data_directory_for_sv_data_collector(
            namespace, results_dir, test_id
        )
    )

    checks_file_name = make_check_file_name(test_id)

    dm_config = test_param_for_test_id["delay_model_config"]
    assign_resources = test_param_for_test_id["assign_resources"]
    publisher_proc = (
        scan_operations.subarray_update_delay_models_and_begin_publishing(
            namespace, dm_config, results_dir, assign_resources
        )
    )

    capture_data_and_validate_checkpoints(
        test_param_for_test_id,
        test_id,
        namespace,
        results_dir,
        sys_tests_results_dir,
        svdc_results_dir,
        checks_file_name,
        True,
        tango_host,
        pytest.subarray_proxy,
        event_tracer,
        capture_subprocs,
    )

    _logger.info(
        "Visibility capture step completed. Killing DM publisher process..."
    )
    publisher_proc_stout = (
        scan_operations.subarray_kill_delay_model_publishing(
            namespace, publisher_proc
        )
    )
    _logger.info(f"captured stdout: {publisher_proc_stout}")


@when(parsers.parse("the CBF stops the visibility capture pod"))
def the_cbf_closes_the_visibility_capture_pod(namespace: str) -> None:
    """Stop the visibility capture pod"""
    vis_pod_operations.delete_vis_pod(namespace)


@then(
    parsers.parse(
        "the CBF visibilities generated for scan {scan_no} for test parameters of {test_id} should meet expectations"
    )
)
def the_visibilities_generated_for_scan_scan_no_for_test_parameters_of_test_id_should_meet_expectations(
    results_dir: str,
    scan_no: str,
    test_id: str,
) -> None:
    """Check if the CBF visibilities generated for scan scan_no for test parameters of test_id meet expectations."""

    sys_tests_results_dir = os.path.join(results_dir, test_id)

    checks_json_scan = os.path.join(
        sys_tests_results_dir,
        make_check_file_name(test_id),
    )

    with open(f"{checks_json_scan}", "r", encoding="utf-8") as params_file:
        checks_dic_scan = json.load(params_file)

    high_level_result = checks_dic_scan["high_level_result"]

    if high_level_result == "PASSED":
        assert (
            True
        ), f"All checkpoints passed for : {test_id} during scan {scan_no}"
    else:
        assert (
            False
        ), f"One or more checkpoints failed for : {test_id} during scan {scan_no}"


# conftest.py: "the observing state(s) of the applicable CBF subarray(s) for {test_id} should be returned to empty."

# pytest_bdd_after_scenario hook - perform clean-up before next iteration of the test
