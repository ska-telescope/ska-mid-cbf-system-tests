import json
import logging
import os
import os.path
import shutil
import subprocess
from time import localtime, strftime
from typing import Generator, List

import plantuml
import pytest
import tango
from dotenv import load_dotenv
from lib import (
    get_parameters,
    param_guardrails,
    powerswitch,
    scan_operations,
    utils,
    vis_pod_operations,
)
from lib.constant import (
    CBF_INPUT_JSON,
    INIT_SYS_PARAM_JSON,
    LOG_FORMAT,
    TESTS_JSON,
)
from lib.sequence_diagrams import EventPrinter, EventsFileParser, TrackedDevice
from pytest_bdd import given, parsers, then
from ska_tango_testing.integration import TangoEventTracer

# ska-mid-cbf-internal-schemas imports
# pylint: disable=no-name-in-module
from test_parameters import test_parameters_validation

load_dotenv()  # Load environment variables from .env file

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)

TEST_PARAMS_DIR = os.path.join(os.getcwd(), "test_parameters")

TEST_PARAMS_DIRS = [
    TEST_PARAMS_DIR,
    os.path.join(TEST_PARAMS_DIR, "delay_model_package"),
    os.path.join(TEST_PARAMS_DIR, "assign_resources"),
    os.path.join(TEST_PARAMS_DIR, "release_resources"),
    os.path.join(TEST_PARAMS_DIR, "configure_scan"),
    os.path.join(TEST_PARAMS_DIR, "scan"),
    os.path.join(TEST_PARAMS_DIR, "checkpoints"),
    os.path.join(TEST_PARAMS_DIR, "slim_config"),
]


def pytest_addoption(parser):
    parser.addoption("--namespace", action="store", default="default")
    parser.addoption("--talon_list", action="store", default="default")
    parser.addoption("--target_site", action="store", default="default")
    parser.addoption("--hw_config_file", action="store", default="default")
    parser.addoption(
        "--slim_fs_config_file", action="store", default="default"
    )
    parser.addoption(
        "--slim_vis_config_file", action="store", default="default"
    )
    parser.addoption("--sys_param", action="store", default="4_boards")
    parser.addoption(
        "--cluster_domain", action="store", default="cluster.local"
    )
    parser.addoption(
        "--tango_host", action="store", default="databaseds-tango-base:10000"
    )
    parser.addoption("--test_id", action="store", default="Test_1")
    parser.addoption(
        "--corr_bitstream_version",
        action="store",
        default="default",
        help="correlation bitstream to use specified by version X.Y.Z, defaults to version in EC with keyword default",
    )


@pytest.fixture(scope="session")
def fqdn_cbfcontroller() -> str:
    return "mid_csp_cbf/sub_elt/controller"


@pytest.fixture(scope="session")
def fqdn_subarray() -> str:
    return "mid_csp_cbf/sub_elt/subarray_01"


@pytest.fixture(scope="session")
def fqdn_talon_board(talon_list) -> list[str]:
    talons = talon_list.split(",")
    return [f"mid_csp_cbf/talon_board/{talon}" for talon in talons]


@pytest.fixture(scope="module")
def event_tracer(
    fqdn_cbfcontroller, fqdn_subarray, fqdn_talon_board
) -> TangoEventTracer:
    tracer = TangoEventTracer()

    tracer.subscribe_event(fqdn_cbfcontroller, "longRunningCommandResult")
    tracer.subscribe_event(fqdn_cbfcontroller, "adminMode")
    tracer.subscribe_event(fqdn_cbfcontroller, "state")

    tracer.subscribe_event(fqdn_subarray, "obsstate")
    tracer.subscribe_event(fqdn_subarray, "longRunningCommandResult")

    for fqdn in fqdn_talon_board:
        tracer.subscribe_event(fqdn, "healthState")

    return tracer


@pytest.fixture(scope="session")
def all_test_ids_for_scenario() -> List[str]:
    return []


@pytest.fixture(scope="session")
def test_report_generator(
    all_test_ids_for_scenario: List[str], namespace: str, results_dir: str
) -> Generator[None, None, None]:
    yield
    # Report generation is placed after the yield so it will only run after the test scenario has completed and is able to acquire data from the completed test run
    generate_reports_for_specified_test_ids(
        results_dir, namespace, all_test_ids_for_scenario
    )
    check_reports_are_generated_for_specified_test_ids(
        results_dir, all_test_ids_for_scenario
    )


@pytest.fixture(scope="session")
def namespace(request):
    namespace_value = request.config.option.namespace
    if namespace_value is None:
        pytest.skip()
    return namespace_value


@pytest.fixture(scope="session")
def talon_list(request):
    return request.config.getoption("--talon_list")


@pytest.fixture(scope="session")
def hw_config_file(request):
    return request.config.getoption("--hw_config_file")


@pytest.fixture(scope="session")
def slim_fs_config_file(request):
    return request.config.getoption("--slim_fs_config_file")


@pytest.fixture(scope="session")
def slim_vis_config_file(request):
    return request.config.getoption("--slim_vis_config_file")


@pytest.fixture(scope="session")
def sys_param(request):
    return request.config.getoption("--sys_param")


@pytest.fixture(scope="session")
def cluster_domain(request):
    return request.config.getoption("--cluster_domain")


@pytest.fixture(scope="session")
def tango_host(request):
    return request.config.getoption("--tango_host")


@pytest.fixture(scope="session")
def test_id(request):
    return request.config.getoption("--test_id")


@pytest.fixture(scope="session")
def results_dir():
    current_dir = os.getcwd()
    results_dir = current_dir + "/results"
    os.makedirs(results_dir, exist_ok=True)
    return results_dir


@pytest.fixture(scope="session")
def original_test_params_dir():
    return TEST_PARAMS_DIR


@pytest.fixture(scope="session")
def state_str():
    return "State"


@pytest.fixture(scope="session")
def off_state_str():
    return "OFF"


@pytest.fixture(scope="session")
def obs_state_str():
    return "ObsState"


@pytest.fixture(scope="session")
def subarray_proxy_default(fqdn_subarray):
    return get_parameters.create_device_proxy(fqdn_subarray)


@pytest.fixture(scope="session")
def cbfcontroller_proxy(fqdn_cbfcontroller):
    timeout_millis = 100000
    cbfcontroller_proxy = get_parameters.create_device_proxy(
        fqdn_cbfcontroller, timeout_millis
    )
    cbfcontroller_proxy.write_attribute("simulationMode", 0)
    cbfcontroller_proxy.write_attribute("adminMode", 0)
    return cbfcontroller_proxy


@pytest.fixture(scope="session")
def vcc_max(cbfcontroller_proxy):
    max_capabilities = cbfcontroller_proxy.get_property("MaxCapabilities")
    if max_capabilities is None:
        num_vcc = 4
        _logger.info(
            f"max_capabilities is None - using default vcc: {num_vcc}"
        )
    else:
        num_vcc = max_capabilities["MaxCapabilities"][0].removeprefix("VCC:")
    return num_vcc


@pytest.fixture(scope="session")
def fsp_max(cbfcontroller_proxy):
    max_capabilities = cbfcontroller_proxy.get_property("MaxCapabilities")
    if max_capabilities is None:
        num_fsp = 4
        _logger.info(
            f"max_capabilities is None - using default fsp: {num_fsp}"
        )
    else:
        num_fsp = max_capabilities["MaxCapabilities"][1].removeprefix("FSP:")
    return num_fsp


@pytest.fixture(scope="function")
def run_parameter_guardrail_for_k_values(sys_param, test_id) -> None:
    """Compares the k value information from the init_sys_param file and selected
    via the system_param key to the corresponding entry in cbf_input for the test being run.

    Uses the cbf_input and init_sys_param file referred to by lib/constant.py
    Args:
    - sys_param: init_sys_param key to determine which k value information to grab
    - test_id: test key to determine which test is being used
    """
    with open(CBF_INPUT_JSON, "r", encoding="utf-8") as cbf_input_data_file:
        cbf_input_dict = json.load(cbf_input_data_file)
        cbf_input_data = cbf_input_dict["cbf_input_data"]

    with open(TESTS_JSON, "r", encoding="utf-8") as tests_file:
        tests_dict = json.load(tests_file)
        tests = tests_dict["tests"]
        cbf_in = tests[test_id]["cbf_input_data"]
        dishid = cbf_input_data[cbf_in]["receptors"][0]["dish_id"]
        kvaluecbf = cbf_input_data[cbf_in]["receptors"][0]["sample_rate_k"]

    with open(
        INIT_SYS_PARAM_JSON, "r", encoding="utf-8"
    ) as init_sys_param_file:
        init_sys_dict = json.load(init_sys_param_file)
        init_sys_param = init_sys_dict["init_sys_param"][sys_param]

    cbf_in = tests[test_id]["cbf_input_data"]
    dishid = cbf_input_data[cbf_in]["receptors"][0]["dish_id"]
    kvaluecbf = cbf_input_data[cbf_in]["receptors"][0]["sample_rate_k"]
    initkval = init_sys_param["dish_parameters"][dishid]["k"]

    assert (
        kvaluecbf == initkval
    ), f"The K_value is not consistent between config files.\n For dish_id: {dishid}, The k_value in cbf_input_file: {kvaluecbf}, does not match the k value for system_param: {sys_param} in init_sys_param: {initkval}.\n  "

    _logger.info(
        "k_value is consistent between cbf_input_data and init_sys_param config files"
    )


@pytest.fixture(scope="session")
def validate_test_parameters(
    namespace,
    results_dir,
    hw_config_file,
    slim_fs_config_file,
    slim_vis_config_file,
    talon_list,
    sys_param,
):
    # Run validation script from the ska-mid-cbf-internal-schemas package
    _logger.info("system-tests: validating test parameters...")
    test_parameters_validation.validate_test_parameters(TEST_PARAMS_DIR)
    _logger.info("system-tests: done validating test parameters!")

    _logger.info("system-tests: generating test parameters summary...")
    test_params_dir = os.path.join(results_dir, "test_parameters")

    # Ensure that the contents of test_params_dir was not from a previous
    # run of the test by deleting test_params_dir and its contents
    shutil.rmtree(test_params_dir, ignore_errors=True)
    os.makedirs(test_params_dir, exist_ok=False)

    # Generate test_parameters_summary.json and test_parameters.json
    curr_dir = os.getcwd()
    generate_test_params_summary_str = f"cp {curr_dir}/test_parameters/tests.json {test_params_dir}/test_parameters_summary.json"
    _logger.info(generate_test_params_summary_str)
    generate_test_params_summary_result = subprocess.run(
        [generate_test_params_summary_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(f"system-tests: {generate_test_params_summary_result.stdout}")

    generate_test_params_str = f"{curr_dir}/test_parameters/test_parameters.py -t -o {test_params_dir}"
    _logger.info(generate_test_params_str)
    generate_test_params_result = subprocess.run(
        [generate_test_params_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(f"system-tests: {generate_test_params_result.stdout}")

    param_guardrails.run_parameter_guardrail_for_subarray_id(test_params_dir)

    # copy the init_sys_param.json to the results dir
    copy_initsysparam_str = f"cp {curr_dir}/test_parameters/init_sys_param.json {test_params_dir}/init_sys_param.json"
    _logger.info(copy_initsysparam_str)
    copy_initsysparam_result = subprocess.run(
        [copy_initsysparam_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(
        f"system-tests init_sys_param.json cp stdout: {copy_initsysparam_result.stdout}"
    )

    # copy the hw_config_file so it can be saved to artifacts
    hw_config_dir = os.path.join(test_params_dir, "hw_config")
    os.makedirs(hw_config_dir, exist_ok=False)
    kube_cp_str = f"cp {hw_config_file} {hw_config_dir}/{os.path.basename(hw_config_file)}"
    _logger.info(kube_cp_str)
    cp_result = subprocess.run(
        [kube_cp_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(f"system-tests hw_config_file cp stdout: {cp_result.stdout}")

    # copy the slim_fs_config_file so it can be saved to artifacts
    slim_config_dir = os.path.join(test_params_dir, "slim_config")
    os.makedirs(slim_config_dir, exist_ok=False)
    kube_cp_str = f"cp {slim_fs_config_file} {slim_config_dir}/{os.path.basename(slim_fs_config_file)}"
    _logger.info(kube_cp_str)
    cp_result = subprocess.run(
        [kube_cp_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(
        f"system-tests slim_fs_config_file cp stdout: {cp_result.stdout}"
    )

    # copy the slim_vis_config_file so it can be saved to artifacts
    kube_cp_str = f"cp {slim_vis_config_file} {slim_config_dir}/{os.path.basename(slim_vis_config_file)}"
    _logger.info(kube_cp_str)
    cp_result = subprocess.run(
        [kube_cp_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    _logger.info(
        f"system-tests slim_vis_config_file cp stdout: {cp_result.stdout}"
    )

    talondx_config_dir = os.path.join(test_params_dir, "talondx_config")
    os.makedirs(talondx_config_dir, exist_ok=False)
    # Locate the talondx_boardmap.json file in the Engineering Console
    kubectl_exec_str = f"kubectl exec ds-deployer-deployer-0 -n {namespace} -- find . -name 'talondx_boardmap.json'"
    _logger.info(kubectl_exec_str)
    kubectl_exec_result = subprocess.run(
        [kubectl_exec_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    # Copy the talondx_boardmap.json file from the Engineering Console to system-tests
    talondx_boardmap_json_pod_loc = kubectl_exec_result.stdout.replace(
        "\n", ""
    )[2:]
    talondx_boardmap_json_kubectl_loc = (
        f"{namespace}/ds-deployer-deployer-0:{talondx_boardmap_json_pod_loc}"
    )
    cp_talondx_boardmap_str = f"kubectl cp {talondx_boardmap_json_kubectl_loc} {talondx_config_dir}/{os.path.basename(talondx_boardmap_json_pod_loc)}"
    subprocess.run(
        [cp_talondx_boardmap_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Get OS and Kernel information for each talon board
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talons = get_parameters.get_talon_boards_list(talon_list)
    for talon in talons:
        talon_ip = get_parameters.get_talon_ip_from_hw_config(talon, hw_config)
        get_talon_info_str = f"{curr_dir}/scripts/get_talon_board_info.sh {talon} {talon_ip} 2>&1"
        _logger.info(get_talon_info_str)
        subprocess.run(
            [get_talon_info_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )
    cp_talon_info_str = f"cp {curr_dir}/talon_board_info.txt {test_params_dir}/talon_board_info.txt"
    _logger.info(cp_talon_info_str)
    subprocess.run(
        [cp_talon_info_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )


@pytest.fixture(scope="session")
def send_test_parameters_to_cbf_tools(
    namespace, results_dir, slim_fs_config_file
):
    sv_dir = "/app"
    copy_test_parameters_to_pod(namespace, "sv", sv_dir)

    # sv also needs needs the test_parameters.json from results dir
    kube_cp_str = f"kubectl cp {results_dir}/test_parameters/test_parameters.json {namespace}/sv:{sv_dir}/test_parameters/test_parameters.json"
    _logger.info(kube_cp_str)
    subprocess.run(
        [kube_cp_str],
        shell=True,
        check=False,
    )

    # sv needs to know which slim file the test is using
    # save the pytest input variable to a file and copy it into the sv pod
    pytest_variable_dict = {}
    pytest_variable_dict["SLIM_FS_CONFIG_FILE"] = slim_fs_config_file

    var_file = "pytest_variables.json"
    pytest_variable_json = json.dumps(pytest_variable_dict, indent=2)
    with open(var_file, "w") as f:
        f.write(pytest_variable_json)

    pytest_var_cp_str = f"kubectl cp {var_file} {namespace}/sv:{sv_dir}/test_parameters/{var_file}"
    _logger.info(pytest_var_cp_str)
    subprocess.run(
        [pytest_var_cp_str],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )


def generate_reports_for_specified_test_ids(
    results_dir: str, namespace: str, test_ids: List[str]
) -> None:
    """
    the reports contain information for each test within the test ids.
    this function calls a utils function that starts the report generation
    within the sv (signal verification) container. Any old reports currently in the folder will be deleted.
    """

    # create folder specifically for reports and ensure it is new and empty
    reports_dir = os.path.join(results_dir, "reports")
    shutil.rmtree(reports_dir, ignore_errors=True)
    os.makedirs(reports_dir, exist_ok=False)

    # generate overall summary report
    utils.generate_report(namespace, "report_summary", reports_dir, test_ids)

    # generate more detailed report for each individual test
    utils.generate_report(namespace, "test_id_report", reports_dir, test_ids)


def check_reports_are_generated_for_specified_test_ids(
    results_dir: str, test_ids: List[str]
) -> None:
    """
    this function checks that the summary report has been generated along with
    an individual report for each test id.
    """

    report_name = os.path.join(results_dir, "reports", "report_summary.html")
    assert os.path.isfile(report_name), f"report not found: {report_name}"

    for test_id in test_ids:
        report_name = os.path.join(results_dir, "reports", f"{test_id}.html")
        assert os.path.isfile(report_name), f"report not found: {report_name}"


def copy_test_parameters_to_pod(
    namespace: str, pod: str, base_dir: str
) -> None:
    """Copies the entire test parameters folder to a given pod and directory.

    test_parameters directory will be spawned inside given directory. Any
    pre-existing test_parameters directory in that location will be deleted.

    Args:
        namespace
        pod: name of pod
        base_dir: full path to directory where test params dir should spawn

    Raises:
        e: suprocess.CalledProcessError when check on copy fails
    """

    _logger.info(
        f"Deleting then creating directory to store test parameters in {pod} pod..."
    )

    rmdir_test_parameters_str = f"kubectl exec {pod} -n {namespace} -- rm -rf {base_dir}/test_parameters"
    _logger.info(rmdir_test_parameters_str)
    subprocess.run(
        [rmdir_test_parameters_str],
        shell=True,
        check=False,
    )

    mkdir_test_parameters_str = f"kubectl exec {pod} -n {namespace} -- mkdir {base_dir}/test_parameters"
    _logger.info(mkdir_test_parameters_str)
    subprocess.run(
        [mkdir_test_parameters_str],
        shell=True,
        check=False,
    )

    _logger.info(
        f"Copying the test parameters to {pod} pod, dir: {base_dir}..."
    )
    for test_param_dir in TEST_PARAMS_DIRS:
        if test_param_dir == TEST_PARAMS_DIR:
            dest_path = "test_parameters/tests.json"
            cp_test_parameters_str = f"kubectl cp {test_param_dir}/tests.json {namespace}/{pod}:{base_dir}/test_parameters/tests.json"
        else:
            dest_path = f"test_parameters/{os.path.basename(test_param_dir)}"
            cp_test_parameters_str = f"kubectl cp {test_param_dir} {namespace}/{pod}:{base_dir}/test_parameters/"

        _logger.info(cp_test_parameters_str)
        subprocess.run(
            [cp_test_parameters_str],
            shell=True,
            check=False,
        )

        test_exists_path_str = (
            f"kubectl exec {pod} -n {namespace} -- test -e {dest_path}"
        )
        _logger.info(
            f"Checking for path {dest_path} in {namespace}/{pod}:{base_dir}/ ..."
        )
        _logger.info(test_exists_path_str)
        try:
            subprocess.run(
                [test_exists_path_str],
                shell=True,
                check=True,
                text=True,
            )
        except subprocess.CalledProcessError as e:
            _logger.error(
                f"Checking for path {namespace}/{pod}:{base_dir}/{dest_path} failed."
            )
            raise e

    _logger.info(
        f"Done copying the test parameters to pod: {pod}, dir: {base_dir}"
    )


def get_boardmap_path(namespace: str) -> str:
    """Retrieve boardmap path in deployer pod."""
    find_boardmap_result = subprocess.run(
        [
            f"kubectl exec ds-deployer-deployer-0 -n {namespace} -- find . -name 'talondx_boardmap.json'"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    return find_boardmap_result.stdout.replace("\n", "")


def get_boardmap_json(namespace: str, boardmap_path: str) -> str:
    """Retrieve boardmap path in deployer pod."""
    cat_boardmap_result = subprocess.run(
        [
            f"kubectl exec ds-deployer-deployer-0 -n {namespace} -- cat {boardmap_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    return json.loads(cat_boardmap_result.stdout.replace("\n", ""))


def pytest_sessionstart(session):
    talon_list = session.config.getoption("--talon_list")
    namespace = session.config.getoption("--namespace")
    system_param = session.config.getoption("--sys_param")
    target_site = session.config.getoption("--target_site")
    hw_config_file = session.config.getoption("--hw_config_file")
    fs_slim_config_file = session.config.getoption("--slim_fs_config_file")
    vis_slim_config_file = session.config.getoption("--slim_vis_config_file")
    tango_host = session.config.getoption("--tango_host")
    cluster_domain = session.config.getoption("--cluster_domain")
    corr_bitstream_version = session.config.getoption(
        "--corr_bitstream_version"
    )
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talon_lrus_under_test_list = get_parameters.get_talon_lrus_under_test_list(
        talon_list, hw_config
    )

    # Default None values for subarray and BITE proxies, and vis pod
    # identification for potential failing test clean up in after_scenario
    pytest.subarray_proxy = None
    pytest.bite_proxy = None
    pytest.vis_uuid = None

    curr_dir = os.getcwd()

    # Copy the hw_config into MCS CbfController
    kube_cp_str = f"kubectl cp {hw_config_file} {namespace}/ds-cbfcontroller-controller-0:/app/mnt/hw_config/hw_config.yaml"
    subprocess.run([kube_cp_str], shell=True, check=False)

    tango_hostname = tango_host.split(":")[0]
    tango_port = tango_host.split(":")[1]

    tango_hostname = tango_host.split(":")[0]
    tango_port = tango_host.split(":")[1]

    os.environ[
        "TANGO_HOST"
    ] = f"{tango_hostname}.{namespace}.svc.{cluster_domain}:{tango_port}"

    deployer_proxy = get_parameters.create_device_proxy(
        "mid_csp_cbf/ec/deployer", 250000
    )
    deployer_proxy.command_read_write("On")

    formatted_talon_list = get_parameters.get_talon_boards_list(talon_list)
    deployer_proxy.write_attribute(
        "targettalons", list(map(int, formatted_talon_list))
    )

    # power on the LRUs under test, in case they were initially off
    powerswitch.turn_lrus_on_off(talon_lrus_under_test_list, "on", hw_config)
    # reboot the talon boards to solve misc intermittent issues eg CIP-2020, CIP-2959
    # doing so before downloading artifacts gives time for reboot to happen
    talons = talon_list.split(",")
    for talon in talons:
        talon_ip = get_parameters.get_talon_ip_from_hw_config(talon, hw_config)
        reboot_str = f'ssh -o StrictHostKeyChecking=no root@{talon_ip} -n "reboot -n && exit"'
        _logger.info(reboot_str)
        subprocess.run(
            [reboot_str],
            shell=True,
            text=True,
            check=False,
        )

    # Set the fpga bitstream in the deployer pod if not default and not in certain environments
    if (corr_bitstream_version != "default") and (
        target_site not in {"itf", "karoo"}
    ):
        boardmap_path = get_boardmap_path(namespace)
        talondx_boardmap_json = get_boardmap_json(namespace, boardmap_path)

        for fpga_bitstream_dict in talondx_boardmap_json["fpga_bitstreams"]:
            if fpga_bitstream_dict["fsp_mode"] == "corr":
                fpga_bitstream_dict["version"] = corr_bitstream_version

        local_talondx_boardmap_json_path = os.path.join(
            curr_dir, os.path.basename(boardmap_path)
        )
        local_tdx_file = open(local_talondx_boardmap_json_path, "w")
        local_tdx_file.write(json.dumps(talondx_boardmap_json, indent=4))
        local_tdx_file.close()

        subprocess.run(
            [
                f"kubectl cp {local_talondx_boardmap_json_path} {namespace}/ds-deployer-deployer-0:{boardmap_path}"
            ],
            shell=True,
            stdout=subprocess.PIPE,
            text=True,
            check=False,
        )

        os.remove(local_talondx_boardmap_json_path)

    _logger.info(
        "Downloading artifacts and configuring DB, this could take a while..."
    )

    deployer_proxy.command_read_write("generate_config_jsons")
    deployer_proxy.command_read_write("download_artifacts")
    deployer_proxy.command_read_write("configure_db")

    utils.set_slim_config(namespace, fs_slim_config_file, vis_slim_config_file)

    # Only update QSPI if not in certain environments
    if target_site not in {"itf", "karoo"}:
        bitstream_dir = "mnt/talondx-config/fpga-talon"

        boardmap_path = get_boardmap_path(namespace)
        talondx_boardmap_json = get_boardmap_json(namespace, boardmap_path)

        fpga_bitstreams = json.loads(
            deployer_proxy.read_attribute("fpgaBitstreams")
        )["fpga_bitstreams"]
        assert len(fpga_bitstreams) >= 1, "No bitstreams in deployer"

        # Prioritize corr bitstream for QSPI update, assuming there is only 1 corr bitstream
        fpga_image_fname = f"{fpga_bitstreams[0]['raw']['base_filename']}-{fpga_bitstreams[0]['version']}.tar.gz"
        for bitstream_info in fpga_bitstreams[1:]:
            if bitstream_info["fsp_mode"] == "corr":
                fpga_image_fname = f"{bitstream_info['raw']['base_filename']}-{bitstream_info['version']}.tar.gz"

        _logger.info(
            f"FPGA image file for the QSPI Update: {fpga_image_fname}"
        )

        # Copy the bitstream file to local for the purpose of updating QSPI image version. Fail if could not fine it.
        controller_copy_path = f"{namespace}/ds-cbfcontroller-controller-0:/app/{bitstream_dir}/{fpga_image_fname}"
        cp_talondx_config_str = (
            f"kubectl cp {controller_copy_path} {curr_dir}/{fpga_image_fname}"
        )
        _logger.info(cp_talondx_config_str)
        result = subprocess.run(
            [cp_talondx_config_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )
        assert (
            "No such file or directory" not in result.stdout
        ), controller_copy_path

        # Use the bitstream file to update the QSPI image version on the boards.
        for talon in talons:
            talon_ip = get_parameters.get_talon_ip_from_hw_config(
                talon, hw_config
            )
            update_qspi_status_str = f"{curr_dir}/scripts/update_qspi_remote.sh {curr_dir}/{fpga_image_fname} {talon} {talon_ip} software 2>&1"
            _logger.info(update_qspi_status_str)
            result = subprocess.run(
                [update_qspi_status_str],
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
                check=False,
            )
            _logger.info(result.stdout)
            assert (
                result.returncode == 0
            ), f"Error: failed to switch to software partition on {talon}"

    # HACK
    # To copy in custom HPS binaries when running system-test locally, uncomment
    # the following and edit the USER, and the ds_paths and local_path variables.
    # The required folder structure is provided in the example:
    # eg.
    # - USER/
    #     - binaries/
    #         - ska-mid-cbf-vcc-app/
    #             - bin/
    #                 - ds-vcc-band-1-and-2.bin
    #                 - ds-vcc-controller.bin
    #                 - ska-mid-cbf-vcc-app.bin

    # ds_paths = ["ska-mid-cbf-vcc-app"]

    # local_path = "/home/USER/binaries" # no slash at the end

    # subprocess.run(
    #     [
    #         f"sudo chmod 777 -R {local_path}"
    #     ],
    #     shell=True,
    #     check=False,
    # )

    # for ds_path in ds_paths:
    #     kube_cp_str = f"kubectl cp {local_path}/{ds_path}/bin/ {namespace}/ds-cbfcontroller-controller-0:/app/mnt/talondx-config/{ds_path}"
    #     _logger.info(f"{kube_cp_str}")
    #     subprocess.run([kube_cp_str], shell=True, check=False)

    # HACK

    # Help generate sequence diagrams for test
    tracked_device_trls = [
        "mid_csp_cbf/sub_elt/controller",
        "mid_csp_cbf/sub_elt/subarray_01",
        "mid_csp_cbf/power_switch/001",
        "mid_csp_cbf/power_switch/002",
        "mid_csp_cbf/talon_lru/001",
        "mid_csp_cbf/talon_lru/002",
        "mid_csp_cbf/vcc/001",
        "mid_csp_cbf/vcc/002",
        "mid_csp_cbf/vcc/003",
        "mid_csp_cbf/vcc/004",
        "mid_csp_cbf/fspcorrsubarray/01_01",
        "mid_csp_cbf/fspcorrsubarray/02_01",
        "mid_csp_cbf/fspcorrsubarray/03_01",
        "mid_csp_cbf/fspcorrsubarray/04_01",
    ]

    tracked_devices = [
        TrackedDevice(
            tango.DeviceProxy(device_trl),
            (
                "longrunningcommandstatus",
                "longrunningcommandresult",
                "longrunningcommandprogress",
            ),
        )
        for device_trl in tracked_device_trls
    ]

    time_now = localtime()
    date = strftime("%Y%m%d", time_now)
    time_now = strftime("%H%M%S", time_now)
    pytest.events_file_name = f"generated_events-{date}-{time_now}.txt"
    event_printer = EventPrinter(pytest.events_file_name, tracked_devices)

    pytest.event_printer = event_printer

    pytest.event_printer.__enter__()

    # Need to create a specific tracer for sending the ON command, as pytest_sessionstart can not access the event_tracer fixture
    tracer = TangoEventTracer()
    fqdn_cbfcontroller = "mid_csp_cbf/sub_elt/controller"
    tracer.subscribe_event(fqdn_cbfcontroller, "longRunningCommandResult")
    tracer.subscribe_event(fqdn_cbfcontroller, "state")
    for talon in talons:
        tracer.subscribe_event(
            f"mid_csp_cbf/talon_board/{talon}", "healthState"
        )

    # Power on the LRUs, program the talon bitstreams, etc.
    utils.mcs_on(
        namespace, system_param, tango_host, cluster_domain, talons, tracer
    )

    # Check that the LRUs under test are powered ON
    for lru in talon_lrus_under_test_list:
        lru_status_check = powerswitch.lru_status(lru, hw_config)
        _logger.info(f"lrus are on= {lru_status_check}")
        assert lru_status_check


# check that the EC container is running
@given("that the CBF Test Tools are in a nominal state")
def that_the_cbf_test_tools_are_in_a_nominal_state(namespace):
    _logger.info("Executing CBF Test Tools are in a nominal state check")
    assert (
        utils.return_pod_status("ds-deployer-deployer-0", namespace)
        == "Running"
    )
    assert utils.return_pod_status("ds-bite-bite-0", namespace) == "Running"
    assert utils.return_pod_status("sv", namespace) == "Running"
    _logger.info(
        "Finished Executing CBF Test Tools are in a nominal state check"
    )


# check that the MCS pods are running and talons under test are pingable
@given("that the CBF is in a nominal state")
def that_the_cbf_is_in_a_nominal_state(namespace, talon_list, hw_config_file):
    _logger.info("Executing the CBF is in a nominal state check")
    mcs_pod_list = os.getenv("MCS_POD_LIST").split(",")
    number_of_ping_attempts = 1
    number_of_ping_replies_required_for_pass = 1
    talon_ping_results = []

    for pod in mcs_pod_list:
        assert (
            utils.return_pod_status(str(pod), namespace) == "Running"
        ), f"Error: pod {pod} status not Running"

    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talon_lrus_under_test_list = get_parameters.get_talon_lrus_under_test_list(
        talon_list, hw_config
    )

    for lru in talon_lrus_under_test_list:
        lru_status_check = powerswitch.lru_status(lru, hw_config)
        _logger.info(f"lru_status_check = {lru_status_check}")
        assert lru_status_check

    talon_ping_results = utils.return_talon_ping_results(
        talon_list, number_of_ping_attempts, hw_config
    )

    for ping_result in talon_ping_results:
        talon = ping_result.split(" ")[1]
        number_of_replies_received = ping_result.split(" ")[5]
        assert (
            int(number_of_replies_received)
            >= number_of_ping_replies_required_for_pass
        ), (str(talon) + "failed")

    # Ping each device server
    utils.device_server_ping_test(timeout=30, namespace=namespace)

    # check rolled up healthState of cbf controller and subarray
    # TODO: Upon completion of CIP-2851 and CIP-2868 (controller and subarray health state roll up), uncomment the below code
    # cbfcontroller = get_parameters.create_device_proxy(
    #     "mid_csp_cbf/sub_elt/controller", 100000
    # )

    # cbfsubarray = get_parameters.create_device_proxy(
    #     "mid_csp_cbf/sub_elt/subarray_01", 100000
    # )

    # controller_health_state = cbfcontroller.read_attribute("healthState")
    # subarray_health_state = cbfsubarray.read_attribute("healthState")

    # assert (
    #    controller_health_state == 0
    # ), f"CBF Controller healthState not OK: {controller_health_state}"
    # _logger.info("CBF Controller healthState OK.")
    # assert (
    #    subarray_health_state == 0
    # ), f"CBF Subarray healthState not OK: {subarray_health_state}"
    # _logger.info("CBF Subarray healthState OK.")


@then(
    parsers.parse(
        "the observing state(s) of the applicable CBF subarray(s) for {test_id} should be returned to empty."
    )
)
def the_observing_states_of_the_applicable_cbf_subarrays_for_test_ids_should_be_returned_to_empty(
    test_id,
    results_dir,
    namespace,
    fqdn_subarray,
    event_tracer,
):
    """the observing state(s) of the applicable CBF subarray(s) for {test_id} should be returned to empty."""
    # the subarray_to_empty_state utility will assert each expected state change was successful
    scan_operations.subarray_to_empty_state(
        pytest.subarray_proxy, fqdn_subarray, event_tracer
    )


def pytest_bdd_after_scenario(
    request, feature, scenario
):  # pylint: disable=R0912
    namespace = request.config.getoption("--namespace")
    system_param = request.config.getoption("--sys_param")
    tango_host = request.config.getoption("--tango_host")
    cluster_domain = request.config.getoption("--cluster_domain")

    # Retrieve fixture values
    hw_config_file = request.getfixturevalue("hw_config_file")
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    event_tracer = request.getfixturevalue("event_tracer")
    fqdn_subarray = request.getfixturevalue("fqdn_subarray")
    talon_list = request.getfixturevalue("talon_list")
    talon_lrus_under_test_list = get_parameters.get_talon_lrus_under_test_list(
        talon_list, hw_config
    )

    _logger.info(
        f"pytest_bdd_after_scenario hook: cleaning up the {feature.name} BDD test"
    )

    if feature.name == "CBF Controller Off Command":
        _logger.info("after scenario clean for CBF Controller off test")
        utils.mcs_on(
            namespace,
            system_param,
            tango_host,
            cluster_domain,
            talon_list.split(","),
            event_tracer,
        )

    # Sanity check that the PDU is still on.
    for lru in talon_lrus_under_test_list:
        lru_status_check = powerswitch.lru_status(lru, hw_config)
        if not lru_status_check:
            _logger.error(f"lru{lru} powerstate is OFF")

    # Cleanup subarray_proxy if was set
    if pytest.subarray_proxy is not None:
        _logger.info(
            f"after scenario clean for subarray_proxy (fqdn: {pytest.subarray_proxy.get_dev_name()})"
        )

        # if all has gone well should be in EMPTY state after scan ends
        # if not, attempt best-case-scenario path from READY to EMPTY:
        # READY -> GoToIdle -> IDLE -> RemoveAllReceptors -> EMPTY
        # else forcibly send subarray back to empty state from any state
        # will use abort pathway with some edge-case exceptions
        try:
            scan_operations.subarray_to_empty_state(
                pytest.subarray_proxy,
                fqdn_subarray,
                event_tracer,
            )
        except AssertionError as assertion_error:
            _logger.error(
                f"Failed to send subarray to EMPTY; {assertion_error}"
            )
            raise assertion_error

        # Set global pytest var back to None
        pytest.subarray_proxy = None

    # Clean up bite_proxy if was set
    if pytest.bite_proxy is not None:
        _logger.info(
            "after scenario clean for bite_proxy (fqdn: mid_csp_cbf/ec/bite)"
        )

        # Stop lstv replay, does nothing if already stopped
        pytest.bite_proxy = get_parameters.create_device_proxy(
            "mid_csp_cbf/ec/bite"
        )
        utils.talon_bite_stop_replay(pytest.bite_proxy)

        # Set global pytest var back to None
        pytest.bite_proxy = None

    # Clean up visibility pod if vis_uuid was used and thus visibility pod
    # was needed
    if pytest.vis_uuid is not None:
        _logger.info(
            f"after scenario clean for visibility pod (vis_uuid: {pytest.vis_uuid})"
        )

        # Delete visibility pod if one exists still
        check_for_vis_pod_cmd = (
            f"kubectl get pod visibilities-{pytest.vis_uuid} -n {namespace}"
        )
        result = subprocess.run(
            [check_for_vis_pod_cmd],
            shell=True,
            check=False,
            capture_output=True,
        )
        if result.returncode == 0:  # cmd returns 0 if finds vis_pod
            vis_pod_operations.delete_vis_pod(namespace)

        # Set global pytest var back to None
        pytest.vis_uuid = None

    _logger.info("pytest_bdd_after_scenario hook: clean-up complete.")


# note that if sessionstart fails this step will not be executed
def pytest_sessionfinish():
    fqdn_cbfcontroller = "mid_csp_cbf/sub_elt/controller"
    timeout_millis = 100000
    cbfcontroller = get_parameters.create_device_proxy(
        fqdn_cbfcontroller, timeout_millis
    )

    # as part of the clean up at the end of the session we want to undo everything that was done during the ON command, including setting the AdminMode back to off
    cbfcontroller.write_attribute("adminMode", 1)

    pytest.event_printer.__exit__(None, None, None)

    time_now = localtime()
    date = strftime("%Y%m%d", time_now)
    time_now = strftime("%H%M%S", time_now)

    current_dir = os.getcwd()
    results_dir = current_dir + "/results"

    sequence_diagram_file_name = (
        f"{results_dir}/sequence-diagram-{date}-{time_now}.puml"
    )

    file_parser = EventsFileParser(
        show_events=False,
        device_hierarchy=[
            ["pytest", "sub_elt.controller"],  # 0
            ["sub_elt.controller", "talon_lru.001", "power_switch.004"],
            ["sub_elt.controller", "talon_lru.002", "power_switch.004"],
            ["pytest", "sub_elt.subarray_01"],
            ["sub_elt.subarray_01", "vcc.001"],  # 1
            ["sub_elt.subarray_01", "vcc.002"],  # 1
            ["sub_elt.subarray_01", "vcc.003"],  # 1
            ["sub_elt.subarray_01", "vcc.004"],  # 1
            [
                "sub_elt.subarray_01",
                "fspcorrsubarray.01_01",
            ],  # 1
            ["sub_elt.subarray_01", "fspcorrsubarray.02_01"],  # 1
            ["sub_elt.subarray_01", "fspcorrsubarray.03_01"],  # 1
            ["sub_elt.subarray_01", "fspcorrsubarray.04_01"],  # 1
        ],
    )

    file_parser.parse(
        pytest.events_file_name, sequence_diagram_file_name, actor="pytest"
    )

    plant_uml_seq = plantuml.PlantUML(
        url="http://www.plantuml.com/plantuml/img/"
    )
    plant_uml_seq.processes_file(sequence_diagram_file_name)
