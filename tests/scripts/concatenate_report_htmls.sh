#!/bin/bash
# This script takes an as input a report directory. The report directory will
# be recursively searched for files ending in *_report.html. All such files
# will be concatenated into "full_report.html", which is written into the
# report directory that was specified as the input.

# report directory to resurvisely search, should be passed in as first and
# only argument
report_dir=$1

# the name for the full concatenated report
full_report="$report_dir/full_report.html"
# clear any old concatenated report so it doesn't infinitely  add to itself
rm $full_report

echo "In directory: $report_dir"

# get a list of all the report html files inot a temporary txt
list="$report_dir/temp_html_report_list.txt"
find $report_dir -name "*_report.html" > $list

# create and then write to full report html file
> $full_report

for file in $(< $list)
do
	echo "adding file: $file"
	echo "CONTENTS OF REPORT: $file" >> $full_report
	grep -iv "<body>" $file | grep -iv "<html>" | grep -iv "</body>" | grep -iv "</html>" >> $full_report
done

echo "Report htmls concatenated into: $full_report"
rm $list
