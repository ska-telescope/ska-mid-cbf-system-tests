import logging
import re
import socket

import paramiko
import requests
from lib.constant import LOG_FORMAT
from pysnmp import error as snmp_error
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.hlapi import UsmUserData
from pysnmp.proto import rfc1902
from requests.structures import CaseInsensitiveDict

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def turn_lrus_on_off(lrus, cmd, hw_config):
    talon_lrus = hw_config["talon_lru"]
    for lru in lrus:
        pdu1 = talon_lrus[lru]["PDU1"]
        outlet1 = talon_lrus[lru]["PDU1PowerOutlet"]
        if cmd == "on":
            pdu_outlet_on(pdu1, outlet1, hw_config)
        else:
            pdu_outlet_off(pdu1, outlet1, hw_config)
        if (
            talon_lrus[lru]["PDU2"] == pdu1
            and talon_lrus[lru]["PDU2PowerOutlet"] == outlet1
        ):
            continue
        pdu2 = talon_lrus[lru]["PDU2"]
        outlet2 = talon_lrus[lru]["PDU2PowerOutlet"]
        if cmd == "on":
            pdu_outlet_on(pdu2, outlet2, hw_config)
        else:
            pdu_outlet_off(pdu2, outlet2, hw_config)


def lru_status(lru, hw_config):
    talon_lrus = hw_config["talon_lru"]

    pdu1 = talon_lrus[lru]["PDU1"]
    outlet1 = talon_lrus[lru]["PDU1PowerOutlet"]
    out = pdu_outlet_state(pdu1, outlet1, hw_config)

    # LRU is on if either outlet is on
    if out is False and not (
        talon_lrus[lru]["PDU2"] == pdu1
        and talon_lrus[lru]["PDU2PowerOutlet"] == outlet1
    ):
        pdu2 = talon_lrus[lru]["PDU2"]
        outlet2 = talon_lrus[lru]["PDU2PowerOutlet"]
        out = pdu_outlet_state(pdu2, outlet2, hw_config)
    return out


def pdu_outlet_on(pdu, outlet, hw_config):
    pdu_config = hw_config["power_switch"][pdu]
    model = pdu_config["PowerSwitchModel"]
    powerswitch_ip = pdu_config["PowerSwitchIp"]
    login = pdu_config["PowerSwitchLogin"]
    powerswitch_pw = pdu_config["PowerSwitchPassword"]

    if model == "DLI LPC9":
        endpoint = f"restapi/relay/outlets/{outlet}/state/"
        data = "value=true"
        out = dli_st_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, data, endpoint
        )
    elif model == "Server Technology Switched PRO2":
        endpoint = f"jaws/control/outlets/{outlet}"
        data = '{"control_action": "on"}'
        out = dli_st_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, data, endpoint
        )
    elif model == "APC AP8681 SSH":
        out, _ = apc_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, "olOn"
        )
    elif model == "APC AP8681 SNMP":
        data = 1
        endpoint = outlet
        auth = UsmUserData(
            userName=login,
            authKey=powerswitch_pw,
        )
        out = apc_snmp_cmd(powerswitch_ip, auth, data, endpoint)
    assert out


def pdu_outlet_off(pdu, outlet, hw_config):
    pdu_config = hw_config["power_switch"][pdu]
    model = pdu_config["PowerSwitchModel"]
    powerswitch_ip = pdu_config["PowerSwitchIp"]
    login = pdu_config["PowerSwitchLogin"]
    powerswitch_pw = pdu_config["PowerSwitchPassword"]

    if model == "DLI LPC9":
        endpoint = f"restapi/relay/outlets/{outlet}/state/"
        data = "value=false"
        out = dli_st_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, data, endpoint
        )
    elif model == "Server Technology Switched PRO2":
        endpoint = f"jaws/control/outlets/{outlet}"
        data = '{"control_action": "off"}'
        out = dli_st_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, data, endpoint
        )
    elif model == "APC AP8681 SSH":
        out, _ = apc_pdu_cmd(
            powerswitch_ip, login, powerswitch_pw, outlet, "olOff"
        )
    elif model == "APC AP8681 SNMP":
        data = 2
        endpoint = outlet
        auth = UsmUserData(
            userName=login,
            authKey=powerswitch_pw,
        )
        out = apc_snmp_cmd(powerswitch_ip, auth, data, endpoint)
    assert out


def pdu_outlet_state(pdu, outlet, hw_config):
    pdu_config = hw_config["power_switch"][pdu]
    powerswitch_model = pdu_config["PowerSwitchModel"]
    powerswitch_ip = pdu_config["PowerSwitchIp"]
    powerswitch_login = pdu_config["PowerSwitchLogin"]
    powerswitch_pw = pdu_config["PowerSwitchPassword"]

    if powerswitch_model == "DLI LPC9":
        endpoint = f"restapi/relay/outlets/{outlet}/"
        expected_off_state = "False"
        expected_on_state = "True"
        out = dli_st_pdu_outlet_state(
            powerswitch_ip,
            powerswitch_login,
            powerswitch_pw,
            endpoint,
            expected_off_state,
            expected_on_state,
        )
    elif powerswitch_model == "Server Technology Switched PRO2":
        endpoint = f"jaws/monitor/outlets/{outlet}"
        expected_off_state = "Off"
        expected_on_state = "On"
        out = dli_st_pdu_outlet_state(
            powerswitch_ip,
            powerswitch_login,
            powerswitch_pw,
            endpoint,
            expected_off_state,
            expected_on_state,
        )
    elif powerswitch_model == "APC AP8681 SSH":
        out = apc_pdu_outlet_state(
            powerswitch_ip, powerswitch_login, powerswitch_pw, outlet
        )
    elif powerswitch_model == "APC AP8681 SNMP":
        expected_off_state = 2
        expected_on_state = 1
        endpoint = str(outlet)
        auth = UsmUserData(
            userName=powerswitch_login,
            authKey=powerswitch_pw,
        )
        out = apc_snmp_outlet_state(
            powerswitch_ip,
            auth,
            endpoint,
            expected_off_state,
            expected_on_state,
        )
    assert out is not None
    return out


# Returns True if outlet is on, False if outlet is off, None if there is an error
def dli_st_pdu_outlet_state(
    powerswitch_ip,
    powerswitch_login,
    powerswitch_pw,
    endpoint,
    expected_off_state,
    expected_on_state,
):
    base_url = f"https://{powerswitch_ip}"
    outlet_state_url = f"{base_url}/{endpoint}"
    header = CaseInsensitiveDict()
    header["Accept"] = "application/json"
    header["X-CSRF"] = "x"
    header["Content-Type"] = "application/x-www-form-urlencoded"
    power_mode = None

    try:
        response = requests.get(
            url=outlet_state_url,
            verify=False,
            headers=header,
            auth=(powerswitch_login, powerswitch_pw),
            timeout=6,
        )

        if response.status_code in [
            requests.codes.get("ok"),
            requests.codes.get("no_content"),
        ]:
            try:
                resp = response.json()
                state = str(resp["state"])
                if state == expected_on_state:
                    power_mode = True
                elif state == expected_off_state:
                    power_mode = False
                else:
                    power_mode = None
            except IndexError:
                power_mode = None
        else:
            _logger.error(f"HTTP response error: {response.status_code}")
        return power_mode
    except (
        requests.exceptions.ConnectTimeout,
        requests.exceptions.ConnectionError,
    ) as e:
        _logger.error(f"Failed to connect to the ServerTech power switch: {e}")
        return None


# Returns True if command is successful, False otherwise
def dli_st_pdu_cmd(
    powerswitch_ip, powerswitch_login, powerswitch_pw, cmd, data, endpoint
):
    base_url = f"https://{powerswitch_ip}"
    outlet_control_url = f"{base_url}/{endpoint}"

    _logger.info(outlet_control_url)

    header = CaseInsensitiveDict()
    header["Accept"] = "application/json"
    header["X-CSRF"] = "x"
    header["Content-Type"] = "application/x-www-form-urlencoded"

    try:
        response = requests.patch(
            url=outlet_control_url,
            verify=False,
            data=data,
            headers=header,
            auth=(powerswitch_login, powerswitch_pw),
            timeout=15,
        )

        if response.status_code in [
            requests.codes.get("ok"),
            requests.codes.get("no_content"),
        ]:
            return True
        _logger.error(f"HTTP response error: {response.status_code}")
        return False
    except (
        requests.exceptions.ConnectTimeout,
        requests.exceptions.ConnectionError,
    ) as e:
        _logger.error(f"Failed to connect to the ServerTech power switch: {e}")
        return False


def apc_pdu_outlet_state(
    powerswitch_ip, powerswitch_login, powerswitch_pw, outlet
):
    status, output = apc_pdu_cmd(
        powerswitch_ip, powerswitch_login, powerswitch_pw, outlet, "olStatus"
    )
    assert status
    # returns list of (outlet id, outlet name, On/Off)
    matches = re.findall("([0-9]+): ([A-Za-z0-9 ]+): (O[nf]+)", output)
    for match in matches:
        if match[2] == "On":
            return True
    return False


def apc_pdu_cmd(
    powerswitch_ip, powerswitch_login, powerswitch_pw, outlet, cmd
):
    """
    Executes command using ssh on the APC PDU

    Args:
    :outlet: the outlet ID string (1 to 24, or "all")
    :cmd: one of "olOn", "olOff", or "olStatus"
    Returns:
    - Boolean status, and command output. Command output is None if status is False.
    """

    if cmd not in ["olOn", "olOff", "olStatus"]:
        _logger.error(f"Invalid command {cmd} for APC PDU")
        return (False, None)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(
            powerswitch_ip, username=powerswitch_login, password=powerswitch_pw
        )
        channel = ssh.invoke_shell()
        _ = channel.recv(4096).decode("utf-8")  # ignore the log in banner
        channel.send(f"{cmd} {outlet}\n")
        out = channel.recv(1024).decode("utf-8")
        if "E000: Success" in out:
            return (True, out)
        _logger.error(f"{cmd} {outlet} failed")
        return (False, None)
    except (
        paramiko.ssh_exception.AuthenticationException,
        paramiko.ssh_exception.SSHException,
    ) as e:
        _logger.error(f"Failed to connect to PDU: {e}")
        return (False, None)
    except socket.timeout:
        _logger.error("APC PDU - Socket timeout error")
        return (False, None)
    finally:
        ssh.close()


def apc_snmp_outlet_state(
    powerswitch_ip, auth, endpoint, expected_off_state, expected_on_state
):
    outlet_status_oid = f"1.3.6.1.4.1.318.1.1.4.4.2.1.3.{endpoint}"
    try:
        cmd_gen = cmdgen.CommandGenerator()
        (
            error_indication,
            error_status,
            error_index,
            var_binds,
        ) = cmd_gen.getCmd(
            auth,
            cmdgen.UdpTransportTarget((powerswitch_ip, 161)),
            cmdgen.MibVariable(outlet_status_oid),
            lookupMib=False,
        )

        if error_indication:
            _logger.error(
                f"Outlet {endpoint} get power state error: {error_indication}, status: {error_status}, index: {error_index}"
            )

        for _, val in var_binds:
            state = val
        if state == expected_on_state:
            power_mode = True
        elif state == expected_off_state:
            power_mode = False
        else:
            power_mode = None
    except snmp_error.PySnmpError:
        power_mode = None
    return power_mode


def apc_snmp_cmd(powerswitch_ip, auth, data, endpoint):
    outlet_status_oid = f"1.3.6.1.4.1.318.1.1.4.4.2.1.3.{endpoint}"
    try:
        cmd_gen = cmdgen.CommandGenerator()
        (
            _,
            _,
            _,
            _,
        ) = cmd_gen.setCmd(
            auth,
            cmdgen.UdpTransportTarget((powerswitch_ip, 161)),
            (outlet_status_oid, rfc1902.Integer32(data)),
        )
        return True
    except snmp_error.PySnmpError:
        return False
