"""Constants to provide to other Python files for neatness"""

# Built up test parameter folders
TEST_PARAM_DIR = "test_parameters"
CBF_INPUT_DIR = TEST_PARAM_DIR + "/cbf_input_data"
BITE_CONFIG_DIR = CBF_INPUT_DIR + "/bite_config_parameters"

# Define JSON files for test data using folders
CBF_INPUT_JSON = CBF_INPUT_DIR + "/cbf_input_data.json"
BITE_CONFIG_JSON = BITE_CONFIG_DIR + "/bite_configs.json"
FILTERS_JSON = BITE_CONFIG_DIR + "/filters.json"
TESTS_JSON = TEST_PARAM_DIR + "/tests.json"
INIT_SYS_PARAM_JSON = TEST_PARAM_DIR + "/init_sys_param.json"

# repo-wide logging format
LOG_FORMAT = "[%(asctime)s|%(levelname)s|%(filename)s#%(lineno)s] %(message)s"
