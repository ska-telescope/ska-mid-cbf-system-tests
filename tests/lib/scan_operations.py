from __future__ import annotations

import concurrent.futures
import json
import logging
import os
import pprint
import shlex
import shutil
import subprocess
from time import sleep
from timeit import default_timer as timer
from typing import Any, Dict, List

import pytest
from assertpy import assert_that
from dotenv import load_dotenv
from lib import datacollector, get_parameters, utils, vis_pod_operations
from lib.constant import LOG_FORMAT
from pytango_client_wrapper import PyTangoClientWrapper
from ska_control_model import ObsState, ResultCode
from ska_tango_testing.integration import TangoEventTracer

load_dotenv()

DM_PACKAGE_DIR = os.path.join(
    os.getcwd(), "test_parameters/delay_model_package"
)

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def scan_normal_sequence(
    subarray_proxy: PyTangoClientWrapper,
    bite_proxy: PyTangoClientWrapper,
    namespace: str,
    results_dir: str,
    test_id: str,
    talon_list: str,
    tango_host: str,
    checks_file_name: str,
    create_sys_test_dir: bool,
    save_updated_test_parameters: bool,
    event_tracer: Any,
) -> None:
    """Performs normal sequence of commands for a scan, including creating and configuring vis pod, configuring the subarray, scanning and validating checkpoints.
    Args:
    : subarray_proxy: TANGO DeviceProxy to a subarray device server to write command to
    : bite_proxy: TANGO DeviceProxy to the BITE device server to use
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name
    : talon_list: list of the talons that are being used
    : tango_host: connection to the tango DS
    : checks_file_name: name of the file to save checkpoints results to
    : create_sys_test_dir: parameter to specify if a new sys test dir needs to be created to dave the results (deletes existing directory if found)
    : save_updated_test_parameters: parameter to specify if updated test parameters should be saved
    """

    subarray_fqdn = get_parameters.get_subarray_fqdn(test_id)
    vis_pod_operations.create_vis_pod(namespace)

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    capture_subprocs = vis_pod_setup_capture_procs(
        test_param_for_test_id,
        namespace,
    )

    lstv_replay = True

    configure_subarray_sequence(
        test_param_for_test_id,
        test_id,
        namespace,
        subarray_proxy,
        bite_proxy,
        event_tracer,
        subarray_fqdn,
        lstv_replay,
    )

    test_params[test_id] = test_param_for_test_id

    if save_updated_test_parameters:
        utils.save_updated_test_parameters(
            test_params=test_params, test_id=test_id
        )

    if create_sys_test_dir:
        sys_tests_results_dir = (
            datacollector.create_data_directory_for_sys_tests(
                namespace, results_dir, test_id
            )
        )
    else:
        sys_tests_results_dir = os.path.join(results_dir, test_id)

    svdc_results_dir = (
        datacollector.create_data_directory_for_sv_data_collector(
            namespace, results_dir, test_id
        )
    )

    dm_config = test_param_for_test_id["delay_model_config"]
    assign_resources = test_param_for_test_id["assign_resources"]
    publisher_proc = subarray_update_delay_models_and_begin_publishing(
        namespace, dm_config, results_dir, assign_resources
    )

    # enabling the timing of data capture to be based on scv tests
    scv_timing = True

    checkpoint_data_capture(
        test_param_for_test_id,
        namespace,
        subarray_proxy,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        results_dir,
        capture_subprocs,
        scv_timing,
        event_tracer,
    )

    checkpoints = test_param_for_test_id["checkpoints"]
    # complete checkpoints for visibilities, which is now collected
    # copies visibilities zip file to sv for validation
    if "visibilities" in checkpoints.keys():
        datacollector.run_data_collector_for_checkpoint(
            "visibilities",
            checkpoints["visibilities"],
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    validate_checkpoints(
        test_param_for_test_id,
        namespace,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        checks_file_name,
    )

    _logger.info("Killing DM publisher process...")
    publisher_proc_stout = subarray_kill_delay_model_publishing(
        namespace, publisher_proc
    )
    _logger.info(f"captured stdout: {publisher_proc_stout}")


def scan_sequence_delay_model_tests(
    subarray_proxy: PyTangoClientWrapper,
    bite_proxy: PyTangoClientWrapper,
    namespace: str,
    results_dir: str,
    test_id: str,
    talon_list: str,
    tango_host: str,
    checks_file_name: str,
    create_sys_test_dir: bool,
    save_updated_test_parameters: bool,
    event_tracer: Any,
) -> None:
    """
    Similar to scan_normal_sequence(), but is intended for test cases
    that use a delay model package generated by MATLAB. The delays are
    added in BITE using the LSTV Resampler Delay Tracker, and are corrected in
    FSP by the RDT.

    Because LSTV replay starts at a fixed timestamp, which is also the start
    time of the delay models. Therefore the DM publisher should start 5 seconds
    ahead of LSTV replay.

    Note: This function uses bite data

    Args:
    : subarray_proxy: TANGO DeviceProxy to a subarray device server to write command to
    : bite_proxy: TANGO DeviceProxy to the BITE device server to use
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name
    : talon_list: list of the talons that are being used
    : tango_host: connection to the tango DS
    : checks_file_name: name of the file to save checkpoints results to
    : create_sys_test_dir: parameter to specify if a new sys test dir needs to be created to dave the results (deletes existing directory if found)
    : save_updated_test_parameters: parameter to specify if updated test parameters should be saved
    """

    subarray_fqdn = get_parameters.get_subarray_fqdn(test_id)
    vis_pod_operations.create_vis_pod(namespace)

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    capture_subprocs = vis_pod_setup_capture_procs(
        test_param_for_test_id,
        namespace,
    )

    utils.generate_bite_data(bite_proxy, test_id, namespace)

    lstv_replay = False

    configure_subarray_sequence(
        test_param_for_test_id,
        test_id,
        namespace,
        subarray_proxy,
        bite_proxy,
        event_tracer,
        subarray_fqdn,
        lstv_replay,
    )

    if create_sys_test_dir:
        sys_tests_results_dir = (
            datacollector.create_data_directory_for_sys_tests(
                namespace, results_dir, test_id
            )
        )
    else:
        sys_tests_results_dir = os.path.join(results_dir, test_id)

    svdc_results_dir = (
        datacollector.create_data_directory_for_sv_data_collector(
            namespace, results_dir, test_id
        )
    )

    # publish delay model
    dm_config = test_param_for_test_id["delay_model_config"]
    assign_resources = test_param_for_test_id["assign_resources"]
    publisher_proc = subarray_update_delay_models_and_begin_publishing(
        namespace, dm_config, results_dir, assign_resources
    )

    # 5s buffer between publishing DMs and lstv replay, this is to ensure that the DMs are published before lstv replay starts (should not be needed after CIP-3165 is resolved)
    sleep(5)

    utils.talon_bite_lstv_replay(bite_proxy)

    # 5s buffer between publishing DMs and scanning, this is to ensure that the scan starts when delay models are valid since publish_lead_time_sec is 5s (see test_parameters README)
    sleep(5)

    # disabling the timing of data capture to be based on scv tests
    scv_timing = False

    checkpoint_data_capture(
        test_param_for_test_id,
        namespace,
        subarray_proxy,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        results_dir,
        capture_subprocs,
        scv_timing,
        event_tracer,
    )

    checkpoints = test_param_for_test_id["checkpoints"]
    # complete checkpoints for visibilities, which is now collected
    # copies visibilities zip file to sv for validation
    if "visibilities" in checkpoints.keys():
        datacollector.run_data_collector_for_checkpoint(
            "visibilities",
            checkpoints["visibilities"],
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    validate_checkpoints(
        test_param_for_test_id,
        namespace,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        checks_file_name,
    )

    _logger.info("Killing DM publisher process...")
    publisher_proc_stout = subarray_kill_delay_model_publishing(
        namespace, publisher_proc
    )
    _logger.info(f"captured stdout: {publisher_proc_stout}")


def scan_sequence_delay_model_tests_dm_published_before_config(
    subarray_proxy: PyTangoClientWrapper,
    bite_proxy: PyTangoClientWrapper,
    namespace: str,
    results_dir: str,
    test_id: str,
    talon_list: str,
    tango_host: str,
    checks_file_name: str,
    create_sys_test_dir: bool,
    save_updated_test_parameters: bool,
    event_tracer: Any,
) -> None:
    """
    Note: This function is the same as scan_sequence_delay_model_tests, except it published delay models before configure scan is executed. This will not be functinal until CIP-2991 is done.

    """

    subarray_fqdn = get_parameters.get_subarray_fqdn(test_id)
    vis_pod_operations.create_vis_pod(namespace)

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    capture_subprocs = vis_pod_setup_capture_procs(
        test_param_for_test_id,
        namespace,
    )

    utils.generate_bite_data(bite_proxy, test_id, namespace)

    # publish delay model
    dm_config = test_param_for_test_id["delay_model_config"]
    assign_resources = test_param_for_test_id["assign_resources"]
    publisher_proc = subarray_update_delay_models_and_begin_publishing(
        namespace, dm_config, results_dir, assign_resources
    )

    lstv_replay_before_configure_scan = False

    configure_subarray_sequence(
        test_param_for_test_id,
        test_id,
        namespace,
        subarray_proxy,
        bite_proxy,
        event_tracer,
        subarray_fqdn,
        lstv_replay_before_configure_scan,
    )

    if create_sys_test_dir:
        sys_tests_results_dir = (
            datacollector.create_data_directory_for_sys_tests(
                namespace, results_dir, test_id
            )
        )
    else:
        sys_tests_results_dir = os.path.join(results_dir, test_id)

    svdc_results_dir = (
        datacollector.create_data_directory_for_sv_data_collector(
            namespace, results_dir, test_id
        )
    )

    utils.talon_bite_lstv_replay(bite_proxy)

    # 5s buffer between publishing DMs and scanning, this is to ensure that the scan starts when delay models are valid since publish_lead_time_sec is 5s (see test_parameters README)
    sleep(5)

    # disabling the timing of data capture to be based on scv tests
    scv_timing = False

    checkpoint_data_capture(
        test_param_for_test_id,
        namespace,
        subarray_proxy,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        results_dir,
        capture_subprocs,
        scv_timing,
        event_tracer,
    )

    checkpoints = test_param_for_test_id["checkpoints"]
    # complete checkpoints for visibilities, which is now collected
    # copies visibilities zip file to sv for validation
    if "visibilities" in checkpoints.keys():
        datacollector.run_data_collector_for_checkpoint(
            "visibilities",
            checkpoints["visibilities"],
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    validate_checkpoints(
        test_param_for_test_id,
        namespace,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        checks_file_name,
    )

    _logger.info("Killing DM publisher process...")
    publisher_proc_stout = subarray_kill_delay_model_publishing(
        namespace, publisher_proc
    )
    _logger.info(f"captured stdout: {publisher_proc_stout}")


def configure_subarray_sequence(
    test_param_for_test_id: Dict[Any, str],
    test_id: str,
    namespace: str,
    subarray_proxy: PyTangoClientWrapper,
    bite_proxy: PyTangoClientWrapper,
    event_tracer: Any,
    subarray_fqdn: str,
    lstv_replay: bool,
) -> None:
    """Performs normal sequence of commands for configuring the subarray for a scan
    Note: This function uses bite data
    Args:
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : test_id: id for the test you are using, will become folder name
    : talon_list: list of the talons that are being used
    : subarray_proxy: TANGO DeviceProxy to a subarray device server to write command to
    : bite_proxy: TANGO DeviceProxy to the BITE device server to use
    : lstv_replay: Boolean to determine if lstv replay should be executed
    """
    assign_resources = test_param_for_test_id["assign_resources"]
    configure_scan = test_param_for_test_id["configure_scan"]

    if lstv_replay:
        utils.generate_bite_data(bite_proxy, test_id, namespace)
        utils.talon_bite_lstv_replay(bite_proxy)

        # TODO: Figure out if we need this (in SCV and auxiliary tests, needed for delay models test)
        sleep(5)

    # --- AddReceptors Command --- #

    receptors = assign_resources["dish"]["receptor_ids"]
    _logger.info(f"Adding receptors {receptors}")

    result = subarray_proxy.command_read_write("AddReceptors", receptors)
    utils.assert_result_code_is_expected(
        "subarray_proxy", "AddReceptors", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.IDLE,
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_LONG
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "AddReceptors completed OK"]',
        ),
    )

    # --- ConfigureScan Command --- #

    configure_json = json.dumps(configure_scan)
    configure_string = configure_json.replace("\n", "")
    pprint.pprint(configure_string)
    _logger.info("executing configure scan")

    result = subarray_proxy.command_read_write(
        "ConfigureScan", configure_string
    )
    utils.assert_result_code_is_expected(
        "subarray_proxy", "ConfigureScan", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.READY,
    )
    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "ConfigureScan completed OK"]',
        ),
    )


def checkpoint_data_capture(
    test_param: Dict[Any, str],
    namespace: str,
    subarray_proxy: PyTangoClientWrapper,
    sys_tests_results_dir: str,
    svdc_results_dir: str,
    test_id: str,
    tango_host: str,
    results_dir: str,
    capture_subprocs: List[subprocess.Popen],
    scv_timing: bool,
    event_tracer: Any,
) -> None:
    """This function collects data for all checkpoints, ends the scan, and collects visibilities
    Args:
    : test_param: test parameters generated based on test id
    : subarray_proxy: TANGO DeviceProxy to a subarray device server to write command to
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name
    : sys_tests_results_dir: directory to save checkpoint results to in system test
    : svdc_results_dir: directory where checkpoint results are saved in sv pod
    : tango_host: connection to the tango DS
    : capture_subprocs: a list of subprocesses for visibility capture per region
    : scv_timing: the timing of scan is based on whether scv tests are being run
    : configure_pod: parameter to specify if a new vis config file needs to be copied into the vis pod
    """
    # get the visibilities - these run in the visibilities pod rather than
    # sv-data-collector and are copied over to system-tests, then into
    # sv-data-collector if needed for the checkpoint criteria #TODO make this part of docstring
    subarray_fqdn = get_parameters.get_subarray_fqdn(test_id)

    vis_pod_name = f"visibilities-{pytest.vis_uuid}"
    vis_pod_ns = namespace

    # min_port_vals = []
    # max_port_vals = []
    # for region_num in region_list:
    #     fsp_output_ports = region_num["output_port"]
    #     min_port_vals.append((min(fsp_output_ports, key=lambda x: x[1]))[1])
    #     max_port_vals.append((max(fsp_output_ports, key=lambda x: x[1]))[1])

    # TODO: currently just grabs min to max ports, possibly upgrade to be more robust for non-consecutive ports within and between FSPs
    # 2024-10-01 - Commenting out below tcpdump as resulting pcap files are causing disk quota issues
    # kube_tcpdump_string = f"kubectl exec {vis_pod_name} -n {vis_pod_ns} -- /bin/bash -c \
    #        'mkdir -p /app/data && chmod 777 /app/data && \
    #        tcpdump -i net1 -s 12000 -C 200 -W 1 -w /app/data/vis.pcap udp dst portrange {min(min_port_vals)}-{max(max_port_vals)}'"
    # _logger.info(f"kube_tcpdump_string {kube_tcpdump_string}")
    # enable tcpdump
    # pylint: disable-next=consider-using-with
    # tcpdump_process = subprocess.Popen(
    #    [kube_tcpdump_string],
    #    shell=True,
    #    stdout=subprocess.PIPE,
    #    stderr=subprocess.PIPE,
    #    text=True,
    # )
    # _logger.info(f"tcpdump_process is {tcpdump_process}")
    # _logger.info(
    #    f"tcpdump_process stdout: {tcpdump_process.stdout}, \nstderr: {tcpdump_process.stderr}"
    # )
    # _logger.info(
    #    f"Running tcpdump on portrange {min(min_port_vals)}-{max(max_port_vals)}"
    # )

    # Sleep to guarantee that the scan is started after:
    #  1. The first delay model has become valid, plus some margin.
    #  2. The visibility capture is ready
    # Assume the first delay model has become valid, plus some margin, as follows:
    #  1. Updating the delay model and publishing it, as well as all steps between that and starting the scan are instantaneous.
    #     Sleep time must be dm_update_buffer_seconds + publish_lead_time_sec + margin.
    #  2. dm_update_buffer_seconds = 4 seconds (see subarray_update_delay_models_and_begin_publishing)
    #  3. publish_lead_time_sec is 5 seconds (see delay_model_config.json and subarray_update_delay_models_and_begin_publishing)
    #  4. margin = 6 seconds
    # Assume that the time required for the visibility capture to be ready is less than the time needed for the first delay model to become valid.
    if scv_timing:
        sleep(15)

    scan = test_param["scan"]
    checkpoints = test_param["checkpoints"]

    vis_capture_duration = 5
    if "visibilities" in checkpoints.keys():
        if "capture_duration" in checkpoints["visibilities"]:
            vis_capture_duration = checkpoints["visibilities"][
                "capture_duration"
            ]
        else:
            # capture duration not specified in parameters, assuming 5 seconds
            vis_capture_duration = 5

    # Create a ThreadPoolExecutor to run the visibility capture process and data collection concurrently
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit the visibility capture process and data collection to the executor
        executor.submit(
            _run_visibility_scan,
            namespace,
            subarray_proxy,
            scan,
            vis_capture_duration,
            capture_subprocs,
            vis_pod_name,
            vis_pod_ns,
            event_tracer,
            subarray_fqdn,
        )
        executor.submit(
            _run_data_collector_for_checkpoints,
            checkpoints,
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    for vis_capture_proc in capture_subprocs:
        _logger.info(
            f"Ending Visibilities Capture Thread for {vis_pod_name} pod for vis proc {vis_capture_proc}"
        )
        vis_capture_proc.kill()

    # 2024-10-01 - Commenting out below tcpdump as resulting pcap files are causing disk quota issues
    # kill tcpdump, as capture should have finished here
    # we need to kill it with pkill, specifically there seems to be problem with Popen.kill() and Popen.terminate() ending tcpdump and creating a .pcap with
    # file that is cut short in middle of a packet. pkill sends SIG.TERM, which is what we need
    # _logger.info(f"Ending tcpdump for {vis_pod_name}")
    # kubectl_pkill_tcpdump = f"kubectl exec {vis_pod_name} -n {vis_pod_ns} -- /bin/bash -c 'pkill tcpdump'"
    # subprocess.run(
    #    [kubectl_pkill_tcpdump],
    #    shell=True,
    #    text=True,
    #    check=False,
    # )
    # few seconds for SIG.TERM to do its thing
    # sleep(3)
    # guard, just incase tcpdump times-out when trying end it
    # try:
    #    tcpdump_process.communicate(timeout=10)
    # except subprocess.TimeoutExpired as timeout:
    #    _logger.error(f"Problem with stopping tcpdump: {timeout}")
    #    # SIG.KILL to make it stops running
    #    tcpdump_process.kill()

    capture_dir = os.path.join(results_dir, test_id, "visibilities")
    shutil.rmtree(capture_dir, ignore_errors=True)
    os.makedirs(capture_dir, exist_ok=False)

    network_info_dir = os.path.join(results_dir, test_id, "network_info")
    shutil.rmtree(network_info_dir, ignore_errors=True)
    os.makedirs(network_info_dir, exist_ok=False)

    # copies app data files from the visibilities pod to local capture_dir
    _logger.info(
        f"Transferring out Visibility Capture files from {vis_pod_name}"
    )
    kubectl_cp_str = (
        f"kubectl cp {vis_pod_ns}/{vis_pod_name}:/app/data/. {capture_dir}"
    )
    _logger.info(kubectl_cp_str)
    cp_proc = subprocess.run(
        [kubectl_cp_str],
        shell=True,
        check=False,
    )
    _logger.info(f"vis cp to sys tests stderr: {cp_proc.stderr}")
    _logger.info(f"vis cp to sys tests stdout: {cp_proc.stdout}")

    # Save out network snapshots and info of linux receiver
    mv_network_info_cmd = f"mv {capture_dir}/network_* {network_info_dir}"
    subprocess.run(
        [mv_network_info_cmd],
        shell=True,
        check=False,
    )


def _run_visibility_scan(
    namespace,
    subarray_proxy,
    scan,
    vis_capture_duration,
    vis_capture_procs,
    vis_pod_name,
    vis_pod_ns,
    event_tracer,
    subarray_fqdn,
):
    # mark start of visibility capture
    _logger.info("Visibility capture duration starting...")

    # --- Scan Command --- #

    _logger.info("Starting scan")
    scan_command = json.dumps(scan["command"])
    scan_string = scan_command.replace("\n", "")
    pprint.pprint(scan_string)
    result = subarray_proxy.command_read_write("Scan", scan_string)
    utils.assert_result_code_is_expected(
        "subarray_proxy", "Scan", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.SCANNING,
    )
    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "Scan completed OK"]',
        ),
    )

    # wait for vis_capture_duration
    sleep(vis_capture_duration)

    # end scan and visibility capture
    _logger.info("Done visibility capture duration...")

    # --- EndScan command --- #

    _logger.info(f"Issuing EndScan command to {subarray_proxy}")
    result = subarray_proxy.command_read_write("EndScan")
    utils.assert_result_code_is_expected(
        "subarray_proxy", "EndScan", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="obsState",
        attribute_value=ObsState.READY,
    )
    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=subarray_fqdn,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "EndScan completed OK"]',
        ),
    )

    # --- Visibility Capture --- #

    _logger.info("Waiting for visibilities capture to complete")

    proc_num = 0
    for vis_capture_proc in vis_capture_procs:
        _logger.info(f"vis capture proc: {vis_capture_proc}")
        try:
            capture_out = vis_capture_proc.communicate(timeout=400)
        except subprocess.TimeoutExpired:
            _logger.error(
                f"Timeout waiting for Visibility Capture {proc_num} to finish. Killing..."
            )
            kubectl_capture_pkill_cmd = f"kubectl exec {vis_pod_name} -n {vis_pod_ns} -- /bin/bash -c 'pkill capture.py'"
            _logger.info(kubectl_capture_pkill_cmd)
            subprocess.run(
                [kubectl_capture_pkill_cmd], shell=True, text=True, check=False
            )
        _logger.info(f"capture {proc_num} stdout: {capture_out[0]}")
        _logger.info(f"capture  {proc_num} stderr: {capture_out[1]}")
        _logger.info(f"Visibilities capture {proc_num} completed")
        proc_num += 1


def _run_data_collector_for_checkpoints(
    checkpoints,
    namespace,
    sys_tests_results_dir,
    svdc_results_dir,
    test_id,
    tango_host,
):
    # data collection for everything except visibilities
    _logger.info("Running data collector for checkpoints")
    for checkpoint, checkpoint_info in checkpoints.items():
        # visibilities data captured after other checkpoints, do at end
        if checkpoint == "visibilities":
            continue
        # Execute data collection for each checkpoint
        datacollector.run_data_collector_for_checkpoint(
            checkpoint,
            checkpoint_info,
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )


def vis_pod_setup_capture_procs(
    test_param: Dict[Any, str],
    namespace: str,
) -> List[subprocess.Popen]:
    """This function starts the capture script for visibilities
    Args:
    : test_param: test parameters generated based on test id
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    Returns:
    : capture_subprocs: List of subprocesses for visibilities capture per region
    """

    vis_pod_name = f"visibilities-{pytest.vis_uuid}"
    vis_pod_ns = namespace

    curr_dir = os.getcwd()

    # copy new vis config file into the vis pod
    temp_vis_capture_config = "vis_capture_cfg_temp.json"

    kubectl_cp_str = f"kubectl cp {curr_dir}/{temp_vis_capture_config} {vis_pod_ns}/{vis_pod_name}:/app"
    _logger.info(kubectl_cp_str)
    subprocess.run(
        [kubectl_cp_str],
        shell=True,
        text=True,
        check=False,
    )

    region_list = test_param["configure_scan"]["midcbf"]["correlation"][
        "processing_regions"
    ]
    _logger.info(
        f"Number of regions to capture visibilities for: {len(region_list)}"
    )

    # loop around all the processing regions with a chunk of output_ports specified in configure scan
    capture_subprocs = []
    for region_num in range(len(region_list)):
        # save one capture file per set of output_ports for ease of use
        kube_vis_capture_string = f"kubectl exec {vis_pod_name} -n {vis_pod_ns} -- /bin/bash -c \
                'python capture.py -c {temp_vis_capture_config} -o /app/data/visibility_capture_{region_num}.zip -d --region {region_num}'"
        _logger.info(kube_vis_capture_string)

        vis_capture_proc = subprocess.Popen(
            [kube_vis_capture_string],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        _logger.info(
            f"Finished with setting up {vis_pod_name} pod for region {region_num}"
        )
        _logger.info(
            f"vis_capture_proc is {vis_capture_proc}, region {region_num}"
        )
        _logger.info(
            f"vis_capture_proc stdout: {vis_capture_proc.stdout}, \nstderr: {vis_capture_proc.stderr}, region {region_num}"
        )
        capture_subprocs.append(vis_capture_proc)

    return capture_subprocs


def validate_checkpoints(
    test_param: Dict[Any, str],
    namespace: str,
    sys_tests_results_dir: str,
    svdc_results_dir: str,
    test_id: str,
    tango_host: str,
    checks_file_name: str,
) -> None:
    """This function validates checkpoints and copies the results file into sys test
    Args:
    : test_param: test parameters generated based on test id
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name
    : sys_tests_results_dir: directory to save checkpoint results to in system test
    : svdc_results_dir: directory where checkpoint results are saved in sv pod
    : tango_host: connection to the tango DS
    : checks_file_name: name of the file to save checkpoints results to
    """
    # run the checkpoints checks for this test ID.
    # this generates the <test_id>_checkpoints_checks.json file
    kube_exec_str = f"kubectl exec sv -n {namespace} --  \
        python3 run_checkpoint_criteria.py {test_id} \
            -i /app/dc-results/{test_id} \
            -o /app/dc-results/{test_id} \
            -p /app/test_parameters"
    _logger.info(kube_exec_str)
    subprocess.run([kube_exec_str], shell=True, check=False)

    # copies the <test_id>_checkpoints_checks.json file from signal verification pod to local directory
    kubectl_cp_str = f"kubectl cp -n {namespace} sv:/app/dc-results/{test_id}/{test_id}_checkpoints_checks.json {sys_tests_results_dir}/{checks_file_name}"
    _logger.info(kubectl_cp_str)
    subprocess.run(
        [kubectl_cp_str],
        shell=True,
        check=False,
    )

    assert os.path.isfile(f"{sys_tests_results_dir}/{checks_file_name}")


def subarray_update_delay_models_and_begin_publishing(
    namespace: str,
    delay_model_config: Any,
    results_dir: str,
    assign_resources: Dict[Any, Any],
) -> Any:
    """Formats delay models and begins publishing them to the TM emulator in a subprocess
    Returns the subprocess so that it can continue running asynchronously
    Args:
    : namespace: namespace you're using, bite pod will be accessed
    : delay_model_config: dictionary with delay model config details
    : results_dir: base results directory within system-tests
    : assign_resources: dictionary with information about the resources to be assigned (i.e. receptors)
    """
    dm_file_path = f'{DM_PACKAGE_DIR}/{delay_model_config["source_file"]}'
    dm_file = os.path.basename(dm_file_path)
    temp_file = "dm_package.json"

    with open(f"{dm_file_path}", "r") as f:
        models = json.load(f)["models"]

    if "drop_delay_models" in delay_model_config:
        models_to_exclude = " ".join(
            str(i) for i in delay_model_config["drop_delay_models"]
        )

    # update validity_period_sec values based on delay model config
    if "validity_period_override_sec" in delay_model_config:
        validity_period = delay_model_config["validity_period_override_sec"]
        for model in models:
            model["model"]["validity_period_sec"] = validity_period

    # remove unused receptors from delay models file
    receptors = assign_resources["dish"]["receptor_ids"]

    for obj in models:
        new_delay_details = []
        for item in obj["model"]["receptor_delays"]:
            if item["receptor"] in receptors:
                new_delay_details.append(item)
        if not new_delay_details:
            raise RuntimeError("receptor_delays must not be empty")
        obj["model"]["receptor_delays"] = new_delay_details

    # save results to a temporary file and copy into ec pod
    with open(temp_file, "w") as f:
        json.dump({"models": models}, f, indent=2)

    kubectl_cp_str = f"kubectl cp {temp_file} {namespace}/ds-bite-bite-0:/app/images/ska-mid-cbf-engineering-console-etc/{dm_file}"
    _logger.info(kubectl_cp_str)
    subprocess.run([kubectl_cp_str], shell=True, text=True, check=False)

    # as of CIP-2003, it takes between 2.2 and 3.1 seconds to update delay models
    # which is why dm_update_buffer_seconds is set to 4
    dm_update_buffer_seconds = 4

    start = timer()
    kube_exec_str = f"kubectl exec ds-bite-bite-0 -n {namespace} --  \
        python3 /app/images/ska-mid-cbf-engineering-console-etc/delay_models_formatter.py --update-delay-models  \
            --dm-template-full-path=/app/images/ska-mid-cbf-engineering-console-etc/{dm_file}  \
            --intermediate-dm-full-path=dm_package_with_epochs.json  \
            --dm-update-buffer-seconds={dm_update_buffer_seconds}"

    if delay_model_config["overwrite_start_validity_sec"]:
        kube_exec_str += " --overwrite-start-validity-sec"

    if "publish_lead_time_sec" in delay_model_config:
        kube_exec_str += f' --publish-lead-time-seconds={delay_model_config["publish_lead_time_sec"]}'

    if models_to_exclude:
        kube_exec_str += f" --drop-models {models_to_exclude}"
    _logger.info(kube_exec_str)
    args = shlex.split(kube_exec_str)
    result = subprocess.run(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
        check=False,
    )
    _logger.info(result.stdout)
    end = timer()
    _logger.info(
        f"Time it took to update delay models with the formatter script: {end - start} seconds"
    )

    _logger.info("publishing delay models...")

    pod = "ds-bite-bite-0"
    script_path = "/app/images/ska-mid-cbf-engineering-console-etc"
    intermediate_dm_path = "dm_package_with_epochs.json"

    if "publish_lead_time_sec" in delay_model_config:
        publish_lead_time = delay_model_config["publish_lead_time_sec"]
    else:
        publish_lead_time = 5

    publish_command = f"kubectl exec {pod} -n {namespace} --  \
        python3 {script_path}/delay_models_publisher.py --publish-delay-models  \
            --intermediate-dm-full-path={intermediate_dm_path}  \
            --publish-lead-time-seconds={publish_lead_time}"
    _logger.info(publish_command)
    args = shlex.split(publish_command)

    publisher_proc = subprocess.Popen(  # pylint: disable=R1732
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
    )

    kubectl_cp_str = f"kubectl cp {namespace}/ds-bite-bite-0:dm_package_with_epochs.json {temp_file}"
    _logger.info(kubectl_cp_str)
    subprocess.run([kubectl_cp_str], shell=True, text=True, check=False)

    test_params_dest = os.path.join(results_dir, "test_parameters", dm_file)
    # save updated delay models as a test artifact
    shutil.copy(temp_file, test_params_dest)

    return publisher_proc


def subarray_kill_delay_model_publishing(
    namespace: str, publisher_proc: subprocess.Popen
) -> str:
    """Kills the delay model publisher and the open process called it.
    Args:
    : namespace: namespace you're using, bite pod will be accessed
    : publisher_proc: subprocess running publish script on kubernetes cluster
    Returns:
    : stdout from publisher_proc by time of termination.
    """
    # Kill publishing job directly on pod
    kubectl_exec_str = f"kubectl exec ds-bite-bite-0 -n {namespace}"
    kubectl_find_publish_job_cmd = (
        f"{kubectl_exec_str} -- ps ax | grep delay_models_publisher.py"
    )
    _logger.info(kubectl_find_publish_job_cmd)
    cmd_result = subprocess.run(
        [kubectl_find_publish_job_cmd],
        capture_output=True,
        shell=True,
        check=False,
    )

    if cmd_result.returncode == 0:
        publish_job_id = int(cmd_result.stdout.split()[0])
        kubectl_kill_cmd = f"{kubectl_exec_str} -- kill 9 {publish_job_id}"
        _logger.info(kubectl_kill_cmd)
        subprocess.run(
            [kubectl_kill_cmd],
            capture_output=True,
            shell=True,
            check=False,
        )
    else:
        _logger.info(
            "No delay_models_publisher.py process found in ds-bite-bite-0"
        )

    # Kill subprocess which called publishing job
    publisher_proc.kill()
    capture_out = publisher_proc.communicate()

    return capture_out[0]


def subarray_to_empty_state(
    subarray_proxy: PyTangoClientWrapper,
    subarray_fqdn: str,
    event_tracer: TangoEventTracer,
) -> None:
    """Sends the subarray into Empty state. Note that it checks the current state of the subarray first and performs the sequence of commands that adheres to the ObsState model to send the subarray to Empty.
    Args:
    : subarray_proxy: proxy to write command to
    """
    _logger.info(
        f"Entering subarray_to_empty_state(); current subarray state {subarray_proxy.read_attribute('ObsState')}"
    )

    max_wait_time = 3
    sleep_time_between_reads = 1

    # if current state is READY go to IDLE
    if subarray_proxy.read_attribute("ObsState") == ObsState.READY:
        _logger.info(f"Issuing GoToIdle command to {subarray_proxy}")
        result = subarray_proxy.command_read_write("GoToIdle")
        utils.assert_result_code_is_expected(
            "subarray_proxy", "GoToIdle", result, ResultCode.QUEUED
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="obsState",
            attribute_value=ObsState.IDLE,
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="longRunningCommandResult",
            attribute_value=(
                f"{result[1][0]}",
                '[0, "GoToIdle completed OK"]',
            ),
        )

    # if current state is IDLE go to EMPTY
    if subarray_proxy.read_attribute("ObsState") == ObsState.IDLE:
        _logger.info(f"Issuing RemoveAllReceptors command to {subarray_proxy}")
        result = subarray_proxy.command_read_write("RemoveAllReceptors")
        utils.assert_result_code_is_expected(
            "subarray_proxy", "RemoveAllReceptors", result, ResultCode.QUEUED
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="obsState",
            attribute_value=ObsState.EMPTY,
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="longRunningCommandResult",
            attribute_value=(
                f"{result[1][0]}",
                '[0, "RemoveAllReceptors completed OK"]',
            ),
        )

    # if current state is RESOURCING/RESTARTING wait as they may go to EMPTY
    if subarray_proxy.read_attribute("ObsState") in [
        ObsState.RESOURCING,
        ObsState.RESTARTING,
    ]:
        utils.wait_to_exit_obs_states(
            subarray_proxy,
            [
                ObsState.RESOURCING,
                ObsState.RESTARTING,
            ],
            max_wait_time,
            sleep_time_between_reads,
        )

    # if not yet EMPTY, continue
    if subarray_proxy.read_attribute("ObsState") != ObsState.EMPTY:
        # check remaining transient states (ABORTING or RESETTING)
        # and wait for them to exit if necessary
        if subarray_proxy.read_attribute("ObsState") in [
            ObsState.ABORTING,
            ObsState.RESETTING,
        ]:
            utils.wait_to_exit_obs_states(
                subarray_proxy,
                [
                    ObsState.ABORTING,
                    ObsState.RESETTING,
                ],
                max_wait_time,
                sleep_time_between_reads,
            )
        # if subarray not yet in FAULT/ABORTED, issue Abort command to enable Restart
        if subarray_proxy.read_attribute("ObsState") not in [
            ObsState.FAULT,
            ObsState.ABORTED,
        ]:
            _logger.info(f"Issuing Abort command to {subarray_proxy}")
            result = subarray_proxy.command_read_write("Abort")
            utils.assert_result_code_is_expected(
                "subarray_proxy", "Abort", result, ResultCode.QUEUED
            )

            assert_that(event_tracer).within_timeout(
                utils.TIMEOUT_SHORT
            ).has_change_event_occurred(
                device_name=subarray_fqdn,
                attribute_name="obsState",
                attribute_value=ObsState.ABORTED,
            )

            assert_that(event_tracer).within_timeout(
                utils.TIMEOUT_SHORT
            ).has_change_event_occurred(
                device_name=subarray_fqdn,
                attribute_name="longRunningCommandResult",
                attribute_value=(
                    f"{result[1][0]}",
                    '[0, "Abort completed OK"]',
                ),
            )

        # now should be in either FAULT/ABORTED and can restart to EMPTY
        _logger.info(f"Issuing Restart command to {subarray_proxy}")
        result = subarray_proxy.command_read_write("Restart")
        utils.assert_result_code_is_expected(
            "subarray_proxy", "Restart", result, ResultCode.QUEUED
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="obsState",
            attribute_value=ObsState.EMPTY,
        )

        assert_that(event_tracer).within_timeout(
            utils.TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=subarray_fqdn,
            attribute_name="longRunningCommandResult",
            attribute_value=(
                f"{result[1][0]}",
                '[0, "Restart completed OK"]',
            ),
        )
