from __future__ import annotations

import json
import logging
import os
import subprocess
import uuid
from typing import Any, Dict

import pytest
import yaml
from dotenv import load_dotenv
from lib import utils
from lib.constant import LOG_FORMAT

load_dotenv()

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def create_vis_pod(namespace: str) -> None:
    """Creates a unique id for the vis pod, creates the pod and asserts that it has been created
    Args:
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    """
    # generate uuid for visibilities pod
    vis_uuid = str(uuid.uuid4())[0:6]
    pytest.vis_uuid = vis_uuid

    _logger.info(f"vis_uuid: {pytest.vis_uuid}")

    curr_dir = os.getcwd()

    # first create the visibilities pod in the default namespace
    vis_pod_name = f"visibilities-{vis_uuid}"
    vis_pod_yaml = f"{curr_dir}/visibilities_pod_temp.yaml"
    vis_pod_ns = namespace

    # Updating vis file based on uuid
    with open(vis_pod_yaml) as f:
        vis_pod_str = f.read().replace("UUID", vis_uuid)
    with open(vis_pod_yaml, "w") as f:
        f.write(vis_pod_str)

    _logger.info("Creating visibilities pod now")
    utils.create_pod_with_warnings(vis_pod_name, vis_pod_yaml, vis_pod_ns)

    # wait until pod status is Running
    assert (
        utils.wait_for_pod_status(vis_pod_name, vis_pod_ns, 120, "Running")
        == "Running"
    ), f"{vis_pod_name} pod status is {utils.return_pod_status(vis_pod_name, vis_pod_ns)}"


def create_capture_config_json(
    test_param: Dict[Any, str], namespace: str
) -> None:
    """Creates the capture json that contains the configure scan info.

    Specifically, the configure scan info for the test is written to a json file,
    with the output host ip addresses replaced with the single visibilities pod ip address.
    Args:
    : namespace: namespace you're using, visibilities pod will be accessed
    : test_param: test parameters generated based on test id
    """
    # set ip of configure scan
    vis_uuid = pytest.vis_uuid
    vis_pod_name = f"visibilities-{vis_uuid}"
    vis_pod_ns = namespace
    vis_ip = get_ip_from_pod(vis_pod_name, vis_pod_ns)

    # we need all processing regions (whether correlation, pst_bf, etc) to have the output host assigned to the vis ip
    if "correlation" in test_param["configure_scan"]["midcbf"].keys():
        try:
            for region in test_param["configure_scan"]["midcbf"][
                "correlation"
            ]["processing_regions"]:
                region["output_host"][0][1] = vis_ip
        except KeyError as e:
            _logger.exception(
                f"Unable to replace output host ip with vis pod ip for correlation capture due to KeyError exception {e}"
            )
    if "pst_bf" in test_param["configure_scan"]["midcbf"].keys():
        try:
            for region in test_param["configure_scan"]["midcbf"]["pst_bf"][
                "processing_regions"
            ]:
                for beam in region["timing_beams"]:
                    beam["output_host"][0][1] = vis_ip
        except KeyError as e:
            _logger.exception(
                f"Unable to replace output host with vis pod ip for pst_bf capture due to KeyError exception {e}"
            )

    curr_dir = os.getcwd()
    temp_vis_capture_config = f"{curr_dir}/vis_capture_cfg_temp.json"
    with open(temp_vis_capture_config, "w+") as json_file:
        json_file.write(json.dumps(test_param))


def delete_vis_pod(namespace: str) -> None:
    """deletes the vis pod
    Args:
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    """
    vis_uuid = pytest.vis_uuid
    vis_pod_name = f"visibilities-{vis_uuid}"
    vis_pod_ns = namespace

    # CIP-2493 TEMP SHOW ALL VISIBILITY POD NODES
    vis_nodes_exec_str = "kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces | grep -i visibilities"
    nodes_proc = subprocess.run(
        [vis_nodes_exec_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )
    vis_nodes = nodes_proc.stdout
    _logger.info(f"Current visibility nodes:\n {vis_nodes}")

    kubectl_delete_pod_str = (
        f"kubectl delete pod {vis_pod_name} -n {vis_pod_ns}"
    )
    _logger.info(kubectl_delete_pod_str)
    subprocess.run(
        [kubectl_delete_pod_str],
        shell=True,
        check=False,
    )

    curr_dir = os.getcwd()

    # Revert the visibilities_pod_temp.yaml UUID back to the placeholder, "UUID", for the next test
    vis_pod_yaml = f"{curr_dir}/visibilities_pod_temp.yaml"
    with open(vis_pod_yaml) as f:
        vis_pod_str = f.read().replace(vis_uuid, "UUID")
    with open(vis_pod_yaml, "w") as f:
        f.write(vis_pod_str)


def get_ip_from_pod(pod_name: str, namespace: str) -> str:
    """
    Gets the IP address of the visibilities pod using kubectl get pod command

    Args:
    : pod_name: the name of the visibilities pod
    : namespace: the namespace the pod is in
    Returns: The IP of the visibilities pod
    """

    kube_exec_str = f"kubectl get pod {pod_name} -n {namespace} \
                -o yaml"
    _logger.info(kube_exec_str)

    pod_yaml_proc = subprocess.run(
        [kube_exec_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )

    vis_pod_yaml_str = pod_yaml_proc.stdout

    vis_pod_yaml = yaml.safe_load(vis_pod_yaml_str)
    network_info = vis_pod_yaml["metadata"]["annotations"][
        "k8s.v1.cni.cncf.io/network-status"
    ]
    network_info = json.loads(network_info)

    # Within the network information of the pod. At least 1 entry will always have the net1 interface
    # linked to the 100G connection for the talon boards
    for info in network_info:
        if "interface" in info:
            if info["interface"] == "net1":
                vis_ip = info["ips"][0]

    _logger.info(f"Visibilities pod ip: {vis_ip}")
    return vis_ip
