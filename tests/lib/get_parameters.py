from __future__ import annotations

import json
import os
from typing import Any, Dict, List, TextIO

import yaml
from dotenv import load_dotenv
from pytango_client_wrapper import PyTangoClientWrapper

load_dotenv()


def get_talon_boards_list(talon_list: str) -> List[str]:
    """Parses a comma separated string into a list. Input strings can be "talon-001,talon-002,..." or "001,002,003"
    Returns a list of strings.
    Args:
    : talon_list: a comma separated string listing the talon targets that are being used, in one of the following formats: "talon-001,talon-002,..." or "001,002,003"
    Returns:
    : out: list of strings of the talons in target format (e.g. ["001", "002"])
    """
    talon_boards = []
    talon_boards_under_test_list = talon_list.split(",")
    for talon in talon_boards_under_test_list:
        talon_boards.append(talon.replace("talon-", ""))
    return talon_boards


def get_talon_lrus_under_test_list(
    talon_list: str, hw_config: TextIO
) -> List[str]:
    """Returns a list of LRUs containing the boards
    Args:
    : talon_list: list of the talons that are being used
    : hw_config: yaml file that contains the mapping of lrus and talons
    Return:
    : lrus_under_test: list of lrus being used
    """
    boards = get_talon_boards_list(talon_list)

    lrus = hw_config["talon_lru"]  # a dict of dict {'001': {...}}
    lrus_under_test = []
    for k, v in lrus.items():
        if any(v["TalonDxBoard1"] == b for b in boards) or any(
            v["TalonDxBoard2"] == b for b in boards
        ):
            lrus_under_test.append(k)
    return lrus_under_test


def get_talon_ip_from_hw_config(talon: str, hw_config: Dict[Any, Any]) -> str:
    """Returns an ip from the hw_config based on talon in talon list
    Args:
    : talon: talon target id to get the ip for
    : hw_config: content of hw_config yaml
    """
    talon_ip = hw_config["talon_board"][talon]
    return talon_ip


def read_yaml_file(filename: str) -> Dict[Any, Any]:
    """Opens a yaml file and returns the contents as a dictionary.
    Args:
    : filename: the yaml file path
    Returns:
    : data: the content of the file as a dict
    Raises:
    : FileNotFoundError if the file doesn't exist
    """
    try:
        with open(filename, "r") as f:
            data = yaml.safe_load(f)
    except FileNotFoundError as exc:
        raise FileNotFoundError(f"{filename} does not exist") from exc

    return data


def get_subarray_fqdn(test_id: str) -> str:
    """returns the subarray fqdn based on subarray_id
    Args:
    : test_id: id for the test you are running
    Returns:
    : subarray_fqdn: fqdn to be used to create the device proxy to subarray
    """
    curr_dir = os.getcwd()
    with open(f"{curr_dir}/test_parameters/tests.json") as file:
        scan_id = json.load(file)["tests"][test_id]["scan"]

    with open(f"{curr_dir}/test_parameters/scan/scan.json") as params_file:
        scan_params = json.load(params_file)

    subarray_id = scan_params["scan"][scan_id]["subarray_id"]
    if int(subarray_id) >= 10:
        subarray_fqdn = f"mid_csp_cbf/sub_elt/subarray_{subarray_id}"
    else:
        subarray_fqdn = f"mid_csp_cbf/sub_elt/subarray_0{subarray_id}"
    return subarray_fqdn


def create_device_proxy(device_fqdn: str, timeout=8) -> PyTangoClientWrapper:
    """creates the device proxy for a given fqdn
    Args:
    : device_fqdn: the fqdn string of the device
    : timeout: timeout of the DeviceProxy connection. The default timeout is 8 seconds unless otherwise specified.
    Returns:
    : device_proxy: creates and returns the device_proxy
    """
    device_proxy = PyTangoClientWrapper()
    device_proxy.create_tango_client(device_fqdn)
    device_proxy.set_timeout(timeout)
    return device_proxy
