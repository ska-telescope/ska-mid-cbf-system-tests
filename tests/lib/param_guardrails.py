import json
import logging
import os

from lib.constant import LOG_FORMAT, TEST_PARAM_DIR

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def check_models_in_delay_model_package(delay_model_package: dict) -> int:
    """parses through each model in delay_model_package and returns the subarray number for each model
    Args:
    delay_model_package(dict): Dictionary containing the description and the list of models containing relevant subarray values.
    Returns:
    int: returns subarray_val for each model in delay_model_package
    """
    for i in range(len(delay_model_package["models"])):
        subarray_val = int(
            delay_model_package["models"][i]["model"]["subarray"]
        )
        return subarray_val


def run_parameter_guardrail_for_subarray_id(directory: str) -> None:
    """Checks that the subarray_id in different config files are consistent
    Args:
    : directory: directory where the config files are stored
    Returns:
    asserts that the subarray_ids are consistent in all config files
    """
    parameters_file = os.path.join(directory, "test_parameters.json")
    with open(f"{parameters_file}", "r") as p_file:
        tests = json.load(p_file)

    for test in tests.keys():
        delay_model_config = tests[test]["delay_model_config"]
        delay_model_source = delay_model_config["source_file"]
        with open(
            TEST_PARAM_DIR + "/delay_model_package/" + delay_model_source
        ) as f:
            delay_model_package = json.load(f)

        assert (
            check_models_in_delay_model_package(delay_model_package)
            == int(tests[test]["assign_resources"]["subarray_id"])
            == int(tests[test]["configure_scan"]["common"]["subarray_id"])
            == int(tests[test]["scan"]["subarray_id"])
            == int(tests[test]["release_resources"]["subarray_id"])
        ), f"the subarray_id is not consistent between config files.\nfor {test}:\nsubarray_id defined in assign resources is {int(tests[test]['assign_resources']['subarray_id'])} \n subarry_id defined in configure scan is {int(tests[test]['configure_scan']['common']['subarray_id'])}\nsubarray_id defined in scan is {int(tests[test]['scan']['subarray_id'])}\nsubarray_id defined in release_resources is {int(tests[test]['release_resources']['subarray_id'])}\nsubarray defined in delay_model_package is {check_models_in_delay_model_package(delay_model_package)}"
