import json
import logging
import os
import subprocess
from time import sleep, time, time_ns
from typing import Any, Dict, List, TextIO

from assertpy import assert_that
from dotenv import load_dotenv
from lib import get_parameters
from lib.constant import (
    BITE_CONFIG_JSON,
    CBF_INPUT_JSON,
    FILTERS_JSON,
    LOG_FORMAT,
    TESTS_JSON,
)
from pytango_client_wrapper import PyTangoClientWrapper
from ska_control_model import HealthState, ResultCode
from ska_tango_testing.integration import TangoEventTracer
from tango import DevState

load_dotenv()

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)

TIMEOUT_DEFAULT = 3
TIMEOUT_SHORT = 60
TIMEOUT_MEDIUM = 120
TIMEOUT_LONG = 800


def return_pod_status(pod_name: str, namespace: str, print_status=True) -> str:
    """Returns the status of a given pod.
    Args:
    : pod_name: name of the pod to be checked
    : namespace: namespace where the pods are deployed
    : print_status: whether or not the status should be printed to terminal
    Returns:
    : kubectl_output.stdout.strip(): stdout of the pod status command
    """
    kubectl_output = subprocess.run(
        [
            f"kubectl get pod {pod_name} --no-headers \
            -o custom-columns=':status.phase' -n {namespace}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    if print_status is True:
        _logger.info(f"{pod_name}: {kubectl_output.stdout}")
    return kubectl_output.stdout.strip()


def return_talon_ping_results(
    talon_list: str, number_of_ping_attempts: int, hw_config: TextIO
) -> List[str]:
    """Pings the talons and returns the results.
    Args:
    : talon_list: list of talons being used for the test.
    : number_of_ping_attempts: number of times the ping should be attempted.
    : hw_config: yaml file that contains the mapping between talons and their ips.
    Returns:
    : talon_ping_results: list containing the ping results for each talon.
    """
    talon_ping_results = []
    talon_boards_under_test_list = talon_list.split(",")
    for talon in talon_boards_under_test_list:
        ping_result_stdout = ""
        talon_ip = get_parameters.get_talon_ip_from_hw_config(talon, hw_config)
        ping_result = subprocess.run(
            [
                f"ping -c {number_of_ping_attempts} {talon_ip} \
                | tail -n 2 | head -n 1 2>&1"
            ],
            shell=True,
            stdout=subprocess.PIPE,
            text=True,
            check=False,
        )
        ping_result_stdout = (
            "Pinging " + str(talon) + ": " + str(ping_result.stdout)
        )
        talon_ping_results.append(ping_result_stdout)
    return talon_ping_results


def set_slim_config(
    namespace: str, fs_slim_config_file: TextIO, vis_slim_config_file: TextIO
) -> None:
    """Copies the FS and VIS SLIM files into ds-cbfcontroller-controller-0 pod.
    Args:
    : namespace: the namespace where the pod is.
    : fs_slim_config_file: path to the fs_slim_config_file.
    : vis_slim_config_file: path to the TextIO, vis_slim_config_file.
    """
    if not os.path.isfile(fs_slim_config_file):
        msg = f"FS SLIM config file {fs_slim_config_file} is not a file"
        raise RuntimeError(msg)

    if not os.path.isfile(vis_slim_config_file):
        msg = f"VIS SLIM config file {vis_slim_config_file} is not a file"
        raise RuntimeError(msg)

    copy_file_str = f"kubectl cp {fs_slim_config_file} {namespace}/ds-cbfcontroller-controller-0:/app/mnt/slim/fs/slim_config.yaml"
    _logger.info(copy_file_str)
    subprocess.run(
        [copy_file_str],
        shell=True,
        check=False,
    )

    copy_file_str = f"kubectl cp {vis_slim_config_file} {namespace}/ds-cbfcontroller-controller-0:/app/mnt/slim/vis/slim_config.yaml"
    _logger.info(copy_file_str)
    subprocess.run(
        [copy_file_str],
        shell=True,
        check=False,
    )


def mcs_on(
    namespace: str,
    system_param: str,
    tango_host: str,
    cluster_domain: str,
    talons: list[str],
    event_tracer: TangoEventTracer,
) -> None:
    """Runs mcs_on command. Asserts if initsysparam and the on command are sent to the controller properly.
    Args:
    : namespace: the namespace that is being used.
    : system_param: key specifying desired system parameters from init sys param.
    : tango_host: connection to the tango DS.
    : cluster_domain: domain used for naming Tango Device Servers.
    : talons: list of Talon targets
    : event_tracer: event tracer to check if the command was executed properly.
    """
    fqdn_cbfcontroller = "mid_csp_cbf/sub_elt/controller"
    fqdn_talon_boards = [
        f"mid_csp_cbf/talon_board/{talon}" for talon in talons
    ]

    tango_hostname = tango_host.split(":")[0]
    tango_port = tango_host.split(":")[1]
    mcs_tango_host = (
        f"{tango_hostname}.{namespace}.svc.{cluster_domain}:{tango_port}"
    )

    os.environ["TANGO_HOST"] = mcs_tango_host

    cbfcontroller = get_parameters.create_device_proxy(
        fqdn_cbfcontroller, 100000
    )
    cbfcontroller.write_attribute("simulationMode", 0)
    cbfcontroller.write_attribute("adminMode", 0)

    curr_dir = os.getcwd()
    with open(f"{curr_dir}/test_parameters/init_sys_param.json") as f:
        init_sys_param_json = json.load(f)
    init_sys_param_str = json.dumps(
        init_sys_param_json["init_sys_param"][system_param]
    )
    _logger.info(f"InitSysParam input: {init_sys_param_str}")
    result = cbfcontroller.command_read_write(
        "InitSysParam", init_sys_param_str
    )
    assert_result_code_is_expected(
        "cbfcontroller", "InitSysParam", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        TIMEOUT_SHORT
    ).has_change_event_occurred(
        device_name=fqdn_cbfcontroller,
        attribute_name="longrunningcommandresult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "InitSysParam completed OK"]',
        ),
    )

    on_command_str = "On"
    result = cbfcontroller.command_read_write(on_command_str)
    assert_result_code_is_expected(
        "cbfcontroller", on_command_str, result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        TIMEOUT_MEDIUM
    ).has_change_event_occurred(
        device_name=fqdn_cbfcontroller,
        attribute_name="state",
        attribute_value=DevState.ON,
    )

    assert_that(event_tracer).within_timeout(
        TIMEOUT_MEDIUM
    ).has_change_event_occurred(
        device_name=fqdn_cbfcontroller,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "On completed OK"]',
        ),
    )

    for fqdn in fqdn_talon_boards:
        assert_that(event_tracer).within_timeout(
            TIMEOUT_SHORT
        ).has_change_event_occurred(
            device_name=fqdn,
            attribute_name="healthState",
            attribute_value=HealthState.OK,
        )


def wait_to_exit_obs_states(
    device: PyTangoClientWrapper,
    obs_states_to_exit: str,
    max_wait_time: int,
    sleep_time_between_reads: int,
) -> bool:
    """Checks if the device is not in the specified observation state.
    Args:
    : device: subarray device proxy
    : obs_state_to_exit: the specified observing state
    : max_wait_time: max time to wait for the subarray to exit the specified state
    : sleep_time_between_reads: how long to wait between checks
    Returns:
    a boolean indicating if the subarray is in the desired state or not
    """
    timeout = time_ns() + (max_wait_time * 1_000_000_000)
    while time_ns() < timeout:
        if int(device.read_attribute("ObsState")) not in obs_states_to_exit:
            return True
        sleep(sleep_time_between_reads)
    _logger.error(
        f"Timeout waiting for {device} to exit one of states: {obs_states_to_exit}"
    )
    _logger.error(f"Final state was: {device.read_attribute('ObsState')}")
    return False


def generate_bite_data(
    bite_proxy: PyTangoClientWrapper, test_id: str, namespace: str
) -> None:
    """Generates the BITE data (Long Sequence Test Vectors) and asserts that there were no failures.
    Args:
    : bite_proxy: a TANGO device proxy to the BITE device
    : test_id: id for the test you are using as a string
    """
    _logger.info(f"Configuring BITE via device for test ID: {test_id}")
    # Get the relevant cbf input data label for the test to use from the test file
    with open(TESTS_JSON) as file:
        cbf_id = json.load(file)["tests"][test_id]["cbf_input_data"]

    # Use cbf input data to get test data
    with open(CBF_INPUT_JSON) as file:
        cbf_input = json.load(file)["cbf_input_data"][cbf_id]

    # If the delay source files is provided, copy to EC-BITE.
    delay_source_files = None
    receptor = cbf_input["receptors"][0]
    delay_source_files = receptor.get("delay_source_files", None)
    if delay_source_files:
        _logger.info("Copying delay models to EC-BITE")
        kube_exec_str = f"kubectl exec ds-bite-bite-0 -n {namespace} -- mkdir -p /app/test_parameters"
        _logger.info(kube_exec_str)
        subprocess.run([kube_exec_str], shell=True, check=False)
        copy_file_str = f"kubectl cp test_parameters/delay_models_csv {namespace}/ds-bite-bite-0:/app/test_parameters/"
        _logger.info(copy_file_str)
        subprocess.run(
            [copy_file_str],
            shell=True,
            check=False,
        )

    result = bite_proxy.command_read_write(
        "load_cbf_input_data", json.dumps(cbf_input)
    )
    assert_result_code_is_expected(
        "bite", "load_cbf_input_data", result, ResultCode.OK
    )

    # Upload the bite config file
    with open(BITE_CONFIG_JSON) as file:
        bite_input = json.load(file)
    result = bite_proxy.command_read_write(
        "load_bite_config_data", json.dumps(bite_input)
    )
    assert_result_code_is_expected(
        "bite", "load_bite_config_data", result, ResultCode.OK
    )

    # Upload the filters config file
    with open(FILTERS_JSON) as file:
        filter_input = json.load(file)
    result = bite_proxy.command_read_write(
        "load_filter_data", json.dumps(filter_input)
    )
    assert_result_code_is_expected(
        "bite", "load_filter_data", result, ResultCode.OK
    )

    # Generate the bite data, and check to ensure it passes
    # To prevent the command timing out the long timeout has been increased to about 13 mins from 2 mins in order to allow time for longer lstv to be generated
    bite_proxy.set_timeout(TIMEOUT_LONG)
    _logger.info("Generating LSTV, this could take a while...")
    result = bite_proxy.command_read_write("generate_bite_data", None)
    # Reset the timeout
    bite_proxy.set_timeout(TIMEOUT_DEFAULT)
    assert_result_code_is_expected(
        "bite", "generate_bite_data", result, ResultCode.OK
    )


def save_updated_test_parameters(
    test_params: Dict[Any, Any], test_id: str
) -> None:
    """save updated test parameters so they can be downloaded as artifacts.
    Args:
    : test_params: dict containing the test parameters.
    : test_id: id for the test that is being used.
    """
    curr_dir = os.getcwd()
    updated_params_file = os.path.join(
        curr_dir, "results/test_parameters/test_parameters.json"
    )

    # if an updated test parameters file exists, only update the parameters for test_id
    if os.path.exists(updated_params_file):
        _logger.info(
            f"updating existing test parameters file: {updated_params_file}"
        )
        with open(updated_params_file, "r") as uparams:
            updated_params = json.load(uparams)
            updated_params[test_id] = test_params[test_id]
    else:
        updated_params = test_params
        _logger.info("new test parameters file will be created")

    with open(updated_params_file, "w+") as json_file:
        json.dump(updated_params, json_file, indent=2)


def talon_bite_lstv_replay(
    bite_proxy: PyTangoClientWrapper,
) -> None:
    """Replays the bite data (Long Sequence Test Vectors) and asserts if they were any errors.
    Args:
    : bite_proxy: Tango DeviceProxy to the configured BITE TANGO device
    """
    result = bite_proxy.command_read_write("start_lstv_replay", None)
    # Update to match new updated bite output
    assert_result_code_is_expected(
        "bite", "start_lstv_replay", result, ResultCode.OK
    )


def talon_bite_stop_replay(bite_proxy: PyTangoClientWrapper) -> None:
    """Stops the Test Data replay. Asserts if there were any errors.
    Args:
    : bite_proxy: Tango DeviceProxy to the configured BITE Tango device
    """
    result = bite_proxy.command_read_write("stop_lstv_replay", None)
    assert_result_code_is_expected(
        "bite", "stop_lstv_replay", result, ResultCode.OK
    )


def generate_report(
    namespace: str, report_type: str, results_dir: str, test_ids: List[str]
) -> None:
    """
    generates a specific report by calling sv and copies it back to results dir

    The reports can be a summary report, or more detailed reports for specific
    test ids. Note test_ids must be a list, even if there is only one.

    : namespace: the namespace being used
    : report_type: whether the report_summary is being generated or an individual
                  test_id_report that can have one or multiple test ids
    : results_dir: results directory in system-tests
    : test_ids: list of string test ids that need to be passed to and copied from sv
    """

    # generate executable string and run
    kube_exec_str = f"kubectl exec sv -n {namespace} --  python3 run_test.py {report_type} -o /app/reports -t {' '.join(test_ids)} -p /app/test_parameters"
    _logger.info(kube_exec_str)
    subprocess.run([kube_exec_str], shell=True, check=False)

    report_names = []
    if report_type == "test_id_report":
        for test_id in test_ids:
            # special case where report name is made of test ids
            report_name = f"{test_id}.html"
            report_names.append(report_name)
    else:
        report_name = f"{report_type}.html"
        report_names.append(report_name)

    for report_name in report_names:
        kube_cp_str = f"kubectl cp {namespace}/sv:/app/reports/{report_name} {results_dir}/{report_name}"
        _logger.info(kube_cp_str)
        subprocess.run([kube_cp_str], shell=True, check=False)


def device_server_ping_test(timeout: int, namespace: str) -> None:
    """Checks that all the devices in a given namespace are reachable. Asserts False if one or more devices are not reachable after timeout.
    Args:
    : timeout: time frame to wait for the devices to become reachable.
    : namespace: namespace where the devices are running.
    """
    reachable = False
    target_time = int(time()) + timeout
    # Give the HPS on the talon boards enough time for the
    # operating system to boot (takes approximately 23 seconds)
    deployer_proxy = get_parameters.create_device_proxy(
        "mid_csp_cbf/ec/deployer", 250000
    )
    while (not reachable) and (int(time()) < target_time):
        # Ping the device servers
        deployer_proxy.command_read_write("device_check")

        # The following kubectl gets the logs of all device_check runs
        result = subprocess.run(
            [f"kubectl logs ds-deployer-deployer-0 -n {namespace}"],
            shell=True,
            text=True,
            check=False,
            capture_output=True,
        )
        # This checks the most recent run of device_check for errors
        last_run = result.stdout[result.stdout.rindex("Check device status") :]
        if last_run.find("Error pinging DeviceProxy") == -1:
            reachable = True
        else:
            _logger.warning(
                "[device-check] Some devices not reached. Retrying..."
            )
        sleep(3)
    assert (
        reachable
    ), f"[device-check] Error: Timeout occurred. Some devices not reachable after {timeout}s.\nFailed Devices:\n{last_run}"
    _logger.info("[device-check] Successfully pinged all HPS device servers.")


def assert_result_code_is_expected(
    device_str: str, command_str: str, result: tuple, expected: ResultCode
) -> None:
    """
    Parses the result (msg, result_code) of the executed command and asserts that the result code is as expected

    Args:
    : device_str: the device that ran the command.
    : command: the command that was executed.
    : result: the result of the executed command.
    : expected: the expected result code.
    """

    # If the 'command_read_write' encountered an unexpected error, a None result is returned
    assert (
        result is not None
    ), f"{device_str}.command_read_write({command_str}) failed. Result was None."

    # Else, the result is not None and we can check the result code to determine success/fail.
    # The result code and message are stored in a tuple, as arrays with a single entry each
    result_code = result[0][0]
    msg = result[1][0]

    # The executed command returns the result code as an int, so cast the expected ResultCode to an int, too
    expected_result_code = int(expected)

    _logger.info(
        f"{device_str}.command_read_write('{command_str}'): result code {result_code} - {msg}"
    )
    assert (
        result_code == expected_result_code
    ), f"{device_str}.command_read_write('{command_str}') failed."
    _logger.info(
        f"{device_str}.command_read_write('{command_str}') result code {result_code} returned as expected."
    )


def check_dev_state(device, device_proxy, attribute, expected_attribute):
    result = device_proxy.read_attribute(attribute)
    _logger.info(f"{attribute} of {device} is: {result}")
    if attribute == "ObsState":
        assert result == expected_attribute
    elif attribute == "State":
        assert str(result) == expected_attribute


def create_pod_with_warnings(
    pod_name: str, pod_yaml: str, namespace: str
) -> None:
    """
    Creates a pod printing warnings if it already existed.

    Args:
    : pod_name: the name of the pod that will be created
    : pod_yaml: the filename (including path) to the yaml file for pod creation
    : pod_ns: the namespace to create the pod in
    """

    kube_pod_exists_str = (
        f"kubectl get pod {pod_name} -n {namespace} --ignore-not-found"
    )
    _logger.info(f"pre-existing {pod_name} pod check: {kube_pod_exists_str}")
    kube_pod_exists_str_result = subprocess.run(
        [kube_pod_exists_str],
        shell=True,
        text=True,
        check=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    _logger.info(
        f"{pod_name} pod check stdout: \n{kube_pod_exists_str_result.stdout}{pod_name} pod check stderr: {kube_pod_exists_str_result.stderr}"
    )

    if pod_name in kube_pod_exists_str_result.stdout:
        _logger.warning(
            f"Pre-existing {pod_name} pod detected prior to pod creation. This may cause errors during the use of this pod."
        )

    kube_pod_start_str = f"kubectl apply -f {pod_yaml} -n {namespace}"
    _logger.info(f"start {pod_name} pod: {kube_pod_start_str}")
    kube_pod_start_str_result = subprocess.run(
        [kube_pod_start_str],
        shell=True,
        text=True,
        check=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    _logger.info(
        f"{pod_name} pod start stdout: {kube_pod_start_str_result.stdout}{pod_name} pod start stderr: {kube_pod_start_str_result.stderr}"
    )


def wait_for_pod_status(
    pod_name: str, namespace: str, wait: int, expected_status: str
) -> str:
    """
    Waits for an existing pod to achieve the desired status for a given time. If the pod status is Pending after the given time, it will print out the pod's info.

    Args:
    : pod_name: the name of the pod that will be created
    : namespace: the namespace the pod is in
    : wait: max time in seconds to wait for pod creation to complete
    : expected_status: the desired status of the pod (eg. "Running")
    Returns: The status of the pod
    """
    curr_time = int(time())
    target_time = curr_time + wait

    while int(time()) < target_time:
        if return_pod_status(pod_name, namespace, False) == expected_status:
            _logger.info(f"{pod_name} pod status is {expected_status}")
            return return_pod_status(pod_name, namespace)

    _logger.warning(
        f"{pod_name} pod status is {return_pod_status(pod_name, namespace)}, expected_status is {expected_status}"
    )

    kubectl_number_of_pods_str = "kubectl get pods --all-namespaces | wc -l"
    _logger.warning(kubectl_number_of_pods_str)
    kubectl_output = subprocess.run(
        [kubectl_number_of_pods_str],
        shell=True,
        stdout=subprocess.PIPE,
        check=False,
    )
    _logger.warning(kubectl_output.stdout)
    kubectl_describe_pod_str = (
        f"kubectl describe pod {pod_name} -n {namespace}"
    )
    _logger.warning(kubectl_describe_pod_str)
    kubectl_output = subprocess.run(
        [kubectl_describe_pod_str],
        shell=True,
        stdout=subprocess.PIPE,
        check=False,
    )
    _logger.warning(kubectl_output.stdout)
    pod_status = return_pod_status(pod_name, namespace, False)
    kubectl_delete_pod_str = f"kubectl delete pod {pod_name} -n {namespace}"
    _logger.warning(kubectl_delete_pod_str)
    subprocess.run(
        [kubectl_delete_pod_str],
        shell=True,
        check=False,
    )
    return pod_status
