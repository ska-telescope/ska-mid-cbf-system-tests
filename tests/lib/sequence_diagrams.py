import re
from dataclasses import dataclass, field
from typing import List, Tuple

import tango

# Regex patterns
INCOMING_COMMAND_CALL_REGEX_PATTERN = r"-> (\w+\.\w+)\(\)"
RETURN_COMMAND_CALL_REGEX_PATTERN = r"^(.*) <- (\w+\.\w+)\(\)"
LRC_RETURN_VAL_REGEX_PATTERN = r"\(\[(.*)\], \['(.*)'\]\)"
LRC_TUPLE_REGEX_PATTERN = r"'([0-9a-zA-Z._]*)', '([^']*)'"
LOG_REGEX_PATTERN = (
    r"([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|(.*)"
)
EVENT_REGEX_PATTERN = r"([^\t]*)\t([^\t]*)\t([^\t]*)\t(.*)"


class PlantUMLSequenceDiagram:
    def __init__(self):
        self.diagram_code = ""

    def start_diagram(self, title, actor):
        self.diagram_code = "@startuml SequenceDiagram\n"
        self.diagram_code += f"title {title}\n"
        self.diagram_code += f"actor {actor}\n"

    def add_participant(self, participant):
        self.diagram_code += f"participant {self.clean_text(participant)}\n"

    def end_diagram(self):
        self.diagram_code += "@enduml"

    def clean_text(self, text):
        return text.replace("-", "_")

    def wrap_text(self, text, max_width=50, max_length=500):
        if max_width <= 0:
            raise ValueError("max_width must be a positive integer.")

        if len(text) > max_length:
            truncated_length = max_length - 3
            text = text[:truncated_length] + "..."

        lines = []
        for i in range(0, len(text), max_width):
            end_index = i + max_width
            segment = text[i:end_index]
            lines.append(segment)

        result = "\n".join(lines).encode("unicode_escape").decode("utf-8")
        return result

    def wrap_text_on_spaces(self, text, max_width=50):
        if max_width <= 0:
            raise ValueError("max_width must be a positive integer.")

        words = text.split(" ")
        line = ""
        lines = []

        for word in words:
            if len(line) + len(word) < max_width:
                line += " " + word
            else:
                lines.append(line)
                line = word
        lines.append(line)

        result = "\n".join(lines).encode("unicode_escape").decode("utf-8")
        return result

    def add_note_over(self, device, note, color="lightgreen"):
        device = self.clean_text(device)
        # note = self.wrap_text(note)

        self.diagram_code += f"rnote over {device} #{color}: {note}\n"

    def add_hexagon_note_over(self, device, note, color="lightgrey"):
        device = self.clean_text(device)
        # note = self.wrap_text(note)

        self.diagram_code += f"hnote over {device} #{color}: {note}\n"

    def add_command_call(self, from_device, to_device, note):
        from_device = self.clean_text(from_device)
        to_device = self.clean_text(to_device)
        note = self.wrap_text(note)

        self.diagram_code += f"{from_device} -> {to_device}: {note}\n"

    def add_command_response(self, from_device, to_device, note):
        from_device = self.clean_text(from_device)
        to_device = self.clean_text(to_device)
        note = self.wrap_text(note)

        self.diagram_code += f"{from_device} --> {to_device}: {note}\n"


class LogParser:
    def __init__(self):
        self.log_pattern_callbacks: list[tuple[str, callable]] = []

    def _parse_log_line(self, log_line):
        for pattern, pattern_cb in self.log_pattern_callbacks:
            match = re.search(pattern, log_line)
            if match:
                group_values = match.groups()
                pattern_cb(*group_values)
                break

    def parse_file(self, file_path):
        with open(file_path, "r", encoding="utf-8") as file:
            logs = file.readlines()

        for log in logs:
            self._parse_log_line(log)


class EventsFileParser(LogParser):
    def __init__(self, show_events=False, device_hierarchy=[]):
        super().__init__()

        self.show_events = show_events

        self.sequence_diagram = PlantUMLSequenceDiagram()
        self.device_hierarchy: list = device_hierarchy
        self.running_lrc_status_updates = {}

        self.log_pattern_callbacks = [
            [EVENT_REGEX_PATTERN, self.event_callback],
        ]

    def get_likely_caller_from_hierarchy(self, device) -> str:
        for hierarchy_list in self.device_hierarchy:
            if device not in hierarchy_list or device == hierarchy_list[0]:
                continue
            device_index = hierarchy_list.index(device)
            hierarchy_index = self.device_hierarchy.index(hierarchy_list)
            likely_caller = self.device_hierarchy[hierarchy_index][
                device_index - 1
            ]
            # print(f'Likely caller of device {device} is {likely_caller}')
            return likely_caller
        print(f"Setting unknown caller for device {device}")
        return "unknown"

    def get_method_from_lrc_id(self, lrc_id) -> str:
        return "_".join(lrc_id.split("_")[2:])

    def parse(self, file_path, output_file_path, actor="pytest"):
        log_file_name = file_path.split("/")[-1]

        cleaned_log_file_name = self.sequence_diagram.clean_text(log_file_name)
        title = (
            f"Sequence diagram generated from\n{cleaned_log_file_name}".encode(
                "unicode_escape"
            ).decode("utf-8")
        )

        self.test_started = False
        self.running_lrc_status_updates = {}
        self.sequence_diagram.start_diagram(title, actor)

        # Add participants to ensure order of swimlanes
        for hierarchy_list in self.device_hierarchy:
            for device in hierarchy_list[1:]:
                self.sequence_diagram.add_participant(device)

        self.parse_file(file_path)

        self.sequence_diagram.end_diagram()

        # Save the PlantUML diagram code to a file
        with open(output_file_path, "w", encoding="utf-8") as f:
            f.write(self.sequence_diagram.diagram_code)

    def event_callback(self, prefix, device: str, event_attr, val):
        # 1724660914.761 - Event - 2024-08-26 08:28:34.761448	DishManager(mid-dish/dish-manager
        # /ska001)	longrunningcommandstatus	('1724660914.663982_241979260268973_SetStowMode',
        # 'COMPLETED')
        # Event - 2024-09-18 08:44:43.859312	MidCspSubarray(mid-csp/subarray/01)	longrunningcommandstatus
        # ('1726641882.6817706_174896405886953_AssignResources', 'STAGING')

        cleaned_device = device.split("/", maxsplit=1)[
            1
        ]  # dish-manager/ska001

        # mid-csp and mid-sdp are both just called "subarray", to differentiate, add the first part of the trl
        # the spfrxpu devices also need the first element to get the dish number
        if cleaned_device.startswith("subarray/") or cleaned_device.startswith(
            "spfrxpu/"
        ):
            cleaned_device = (
                f'{device.split("(")[1].split("/")[0]}/{cleaned_device}'
            )

        cleaned_device = cleaned_device.replace("/", ".")[:-1]
        caller = self.get_likely_caller_from_hierarchy(cleaned_device)

        if "longrunningcommand" in event_attr:
            self.handle_lrc_event_log(cleaned_device, caller, event_attr, val)
        elif self.show_events:
            self.sequence_diagram.add_note_over(
                cleaned_device,
                f'Event\n""{event_attr} = {val.strip()}""'.encode(
                    "unicode_escape"
                ).decode("utf-8"),
            )

    def handle_lrc_event_log(self, device, caller, event_attr, val):
        if "longrunningcommandstatus" in event_attr:
            lrc_statuses = re.findall(LRC_TUPLE_REGEX_PATTERN, val)
            for index, (lrc_id, status) in enumerate(lrc_statuses):
                # If there are any newer updates for this lrc in the LRC statuses then skip this
                newer_status_found = False
                if index + 1 < len(lrc_statuses):
                    for i in range(index + 1, len(lrc_statuses)):
                        if lrc_statuses[i][0] == lrc_id:
                            newer_status_found = True
                            break

                if newer_status_found:
                    break

                method_name = self.get_method_from_lrc_id(lrc_id)

                if status == "STAGING":
                    # Only track methods which are called in the scope of the file
                    # This avoids some noise left over in LRC attributes from previous test / setup
                    self.running_lrc_status_updates[lrc_id] = []

                # Only update if its a method called in the scope of this file and its a new status
                if (
                    lrc_id in self.running_lrc_status_updates
                    and status not in self.running_lrc_status_updates[lrc_id]
                ):
                    self.running_lrc_status_updates[lrc_id].append(status)
                    self.sequence_diagram.add_command_response(
                        device, caller, f'""{method_name}"" -> {status}'
                    )
        elif "longrunningcommandprogress" in event_attr:
            lrc_progresses = re.findall(LRC_TUPLE_REGEX_PATTERN, val)
            for lrc_id, progress in lrc_progresses:
                # Only show progress updates for methods which have been staged
                if lrc_id in self.running_lrc_status_updates:
                    method_name = self.get_method_from_lrc_id(lrc_id)
                    self.sequence_diagram.add_command_call(
                        device, device, f'""{method_name}"" -> {progress}'
                    )
        elif event_attr == "longrunningcommandresult":
            pass


@dataclass
class TrackedDevice:
    """Class to group tracked device information"""

    device_proxy: tango.DeviceProxy
    attribute_names: Tuple[str]
    subscription_ids: List[int] = field(default_factory=list)


class EventPrinter:
    """Class that writes attribute changes to a file"""

    def __init__(
        self, filename: str, tracked_devices: Tuple[TrackedDevice] = ()
    ) -> None:
        self.tracked_devices = tracked_devices
        self.filename = filename
        self.events = []

    def __enter__(self):
        for tracked_device in self.tracked_devices:
            dp = tracked_device.device_proxy
            for attr_name in tracked_device.attribute_names:
                sub_id = dp.subscribe_event(
                    attr_name, tango.EventType.CHANGE_EVENT, self
                )
                tracked_device.subscription_ids.append(sub_id)

    def __exit__(self, exc_type, exc_value, exc_tb):
        for tracked_device in self.tracked_devices:
            try:
                dp = tracked_device.device_proxy
                for sub_id in tracked_device.subscription_ids:
                    dp.unsubscribe_event(sub_id)
            except tango.DevError:
                pass

    def add_event(self, timestamp, message):
        self.events.append((timestamp, message))
        with open(self.filename, "a") as open_file:
            open_file.write("\n" + message)

    def push_event(self, ev: tango.EventData):
        event_string = ""
        if ev.err:
            err = ev.errors[0]
            event_string = (
                f"\nEvent Error {err.desc} {err.origin} {err.reason}"
            )
        else:
            attr_name = ev.attr_name.split("/")[-1]
            attr_value = ev.attr_value.value
            if ev.attr_value.type == tango.CmdArgType.DevEnum:
                attr_value = ev.device.get_attribute_config(
                    attr_name
                ).enum_labels[attr_value]

            event_string = f"Event - {ev.reception_date}\t{ev.device}\t{attr_name}\t{attr_value}"

        self.add_event(ev.reception_date.totime(), event_string)
