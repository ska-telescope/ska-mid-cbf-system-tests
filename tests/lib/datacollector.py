import json
import logging
import os
import shutil
import subprocess

from dotenv import load_dotenv
from lib.constant import LOG_FORMAT

load_dotenv()

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def run_data_collector_for_checkpoint(
    checkpoint,
    checkpoint_info,
    namespace,
    results_dir,
    svdc_results_dir,
    test_id,
    tango_host,
):
    """Collects data for a given checkpoint.

    Collects all the data necessary to evaluate the criteria for a given
    checkpoint, using the sv-data-collector and sv pods.
    Inputs:
    :checkpoint: the checkpoint to collect data for
    :checkpoint_info: the checkpoint information including the talon board and criteria
    :namespace: the namespace for all the pods
    :results_dir: the results directory in system-tests for that test id
    :test_id: current test_id
    """

    if checkpoint == "histograms":
        checkpoint_histograms(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )

    elif checkpoint == "packet_stream_repair":
        checkpoint_packet_stream_repair(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )

    elif checkpoint == "wideband_state_count":
        checkpoint_wideband_state_count(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )

    elif checkpoint == "visibilities":
        checkpoint_visibilities(namespace, results_dir, test_id)

    elif checkpoint == "talon_status":
        checkpoint_talon_status(
            checkpoint_info,
            namespace,
            results_dir,
            svdc_results_dir,
            tango_host,
        )

    elif checkpoint == "slim_links":
        checkpoint_slim_links(namespace, results_dir, svdc_results_dir)

    elif checkpoint == "ddr4_corner_turner":
        checkpoint_dct(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )

    elif checkpoint == "resampler_delay_tracker":
        checkpoint_rdt(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )

    elif checkpoint == "one_hundred_g_ethernet":
        checkpoint_one_hundred_g_ethernet(
            checkpoint_info, namespace, results_dir, svdc_results_dir
        )


def checkpoint_slim_links(namespace, results_dir, svdc_results_dir):
    file_name = "slim_links_metadata.yaml"
    capture_dir = os.path.join(results_dir, "slim_links")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir and file name
    os.makedirs(capture_dir, exist_ok=False)
    capture_file = os.path.join(capture_dir, file_name)

    # generate slim mesh data for fs and vis
    device_string = "mid_csp_cbf/slim/slim-fs,mid_csp_cbf/slim/slim-vis"

    kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- \
            python3 collector.py slim_links -ds {device_string} \
                -o {svdc_results_dir}"
    _logger.info(kube_exec_str)

    subprocess.run(
        [kube_exec_str],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        check=False,
    )

    copy_from_data_collector_into_local_file(
        namespace, file_name, svdc_results_dir, capture_file
    )


def checkpoint_talon_status(
    checkpoint_info,
    namespace,
    results_dir,
    svdc_results_dir,
    tango_host,
):
    talon_status_info = checkpoint_info

    mcs_tango_host = tango_host
    file_name = "talondx_status.yaml"
    capture_dir = os.path.join(results_dir, "talondx_status")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)
    # Create capture_dir to hold the generated yaml file
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through each talon board and generate talon_status data
    for talon_target_id in talon_status_info["talons"]:
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        talondx_status_device = tango_device + "/ska-talondx-status/status"

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- \
                env TALONDX_STATUS_DEVICE={talondx_status_device} TANGO_HOST={mcs_tango_host} \
                python collector.py talondx_status -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        data_collector = subprocess.run(
            [kube_exec_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )

        data_collector_log = data_collector.stderr + data_collector.stdout

        assert not (
            "ERROR" in data_collector_log
        ), "Error while trying to run the signal verification data collector for SKA TalonDx Status device server"

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def checkpoint_visibilities(namespace, results_dir, test_id):
    # visibilities capture handled earlier, need to ensure copy of vis zip
    # file winds up in the same folder with the other data
    # copy contents of visibilities folder (vis capture zip files) from local to signal verification pod
    folder_name = "visibilities"
    capture_dir = os.path.join(results_dir, folder_name)

    copy_visibilities_str = f"kubectl cp {capture_dir}/. {namespace}/sv:/app/dc-results/{test_id}/."
    _logger.info(copy_visibilities_str)
    cp_proc = subprocess.run(
        [copy_visibilities_str],
        shell=True,
        check=False,
    )
    _logger.info(f"vis cp sterr: {cp_proc.stderr}")
    _logger.info(f"vis cp stdout: {cp_proc.stdout}")


def checkpoint_wideband_state_count(
    checkpoint_info, namespace, results_dir, svdc_results_dir
):
    wb_state_count_info = checkpoint_info

    file_name = "wb_state_count_metadata.yaml"
    capture_dir = os.path.join(results_dir, "wb_state_count")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through each talon board and generate wb_state_count data
    for talon_target_id in wb_state_count_info["talons"]:
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        wb_state_count_device = tango_device + "/wbstatecount/state_count"

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- python3 collector.py wb_state_count -d {wb_state_count_device} -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        subprocess.run([kube_exec_str], shell=True, check=False)

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def checkpoint_packet_stream_repair(
    checkpoint_info, namespace, results_dir, svdc_results_dir
):
    packet_stream_repair_info = checkpoint_info

    file_name = "packet_stream_repair.yaml"
    capture_dir = os.path.join(results_dir, "packet_stream_repair")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)
    # Create capture_dir to hold the generated yaml file
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through packet_stream_repair entries to generate data for each board
    for packet_stream_repair in packet_stream_repair_info:
        talon_target_id = packet_stream_repair["talon"]
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        psr_string = os.getenv("PACKET_STREAM_REPAIR_DEVICE_STRING")
        device_list = psr_string.split(",")
        psr_device_list = []
        for device in device_list:
            device = str(tango_device) + "/packetstreamrepair/" + str(device)
            psr_device_list.append(device)

        _logger.info(f"packet stream repair device list: {psr_device_list}")

        # turning the list back into a string to pass as an argument to collector script
        device_string = ""
        for device in psr_device_list:
            device_string = device_string + "," + str(device)

        # remove the comma at the beginning of the string
        device_string = device_string.replace(",", "", 1)

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- \
                python3 collector.py packet_stream_repair -ds {device_string} \
                    -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)

        data_collector = subprocess.run(
            [kube_exec_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )

        data_collector_log = data_collector.stderr + data_collector.stdout
        _logger.info(data_collector_log)

        assert not (
            "ERROR" in data_collector_log
        ), "Error while trying to run the signal verification data collector for Packet Stream Repair device server"

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def get_histogram_timeout_map() -> dict:
    """
    returns histogram timeout map based on HISTOGRAM_TIMEOUT_MAP env variable
    """
    timeout_string = os.getenv("HISTOGRAM_TIMEOUT_MAP")
    raw_map = json.loads(timeout_string)

    timeout_map = {}
    for device_name in raw_map:
        if "POST" in device_name:
            device_name_root = device_name[:-2]
            timeout_map[device_name_root] = raw_map[device_name]
        else:
            timeout_map[device_name] = raw_map[device_name]
    return timeout_map


def checkpoint_histograms(
    histograms_info, namespace, results_dir, svdc_results_dir
):
    capture_dir = os.path.join(results_dir, "histogram")
    file_name = "histogram_metadata.yaml"

    # Delete capture_dir to remove contents from previous test runs
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir
    os.makedirs(capture_dir, exist_ok=False)

    histogram_timeout_map = get_histogram_timeout_map()

    # Loop through histogram entries to generate data for each board
    for histogram in histograms_info:
        talon_target_id = histogram["talon"]
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        histogram_fqdn_timeout_map = {}
        for device in histogram["inspection_points"]:
            # For device names like E_POST_VCC_1, E_POST_RDT_2, remove the last
            # 2 characters to look for timeout values in the timeout map
            map_name = device
            if "POST" in device:
                map_name = device[:-2]

            timeout = histogram_timeout_map[map_name]
            device_fqdn = str(tango_device) + "/histogram/" + str(device)
            histogram_fqdn_timeout_map[device_fqdn] = timeout

        _logger.info(
            f"histogram timeout map is: {json.dumps(histogram_fqdn_timeout_map)}"
        )

        histogram_fqdn_timeout_map_string = json.dumps(
            histogram_fqdn_timeout_map
        )

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- python3 collector.py histogram -htm '{histogram_fqdn_timeout_map_string}' -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        subprocess.run([kube_exec_str], shell=True, check=False)

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def checkpoint_dct(checkpoint_info, namespace, results_dir, svdc_results_dir):
    dct_info = checkpoint_info

    file_name = "ddr4_corner_turner.yaml"
    capture_dir = os.path.join(results_dir, "ddr4_corner_turner")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through each talon board and collect dct data
    for ddr4_corner_turner in dct_info:
        talon_target_id = ddr4_corner_turner["talon"]
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        dct_device = tango_device + "/dct/dct"

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- python3 collector.py ddr4_corner_turner -d {dct_device} -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        data_collector = subprocess.run(
            [kube_exec_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )

        data_collector_log = data_collector.stderr + data_collector.stdout
        _logger.info(data_collector_log)

        assert not (
            "ERROR" in data_collector_log
        ), "Error while trying to run the signal verification data collector for DDR4 Corner Turner device server"

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def checkpoint_rdt(checkpoint_info, namespace, results_dir, svdc_results_dir):
    file_name = "fsp-corr-controller.yaml"
    capture_dir = os.path.join(results_dir, "resampler_delay_tracker")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through each talon board and collect fsp-corr-controller data
    for rdt in checkpoint_info:
        talon_target_id = rdt["talon"]
        tango_device = get_tango_device(talon_target_id)
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        fsp_corr_controller_device = (
            tango_device + "/fsp-app/fsp-corr-controller"
        )

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- python3 collector.py resampler_delay_tracker -d {fsp_corr_controller_device} -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        data_collector = subprocess.run(
            [kube_exec_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )

        data_collector_log = data_collector.stderr + data_collector.stdout
        _logger.info(data_collector_log)

        assert not (
            "ERROR" in data_collector_log
        ), "Error while trying to run the signal verification data collector for resampler delay tracker"

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def checkpoint_one_hundred_g_ethernet(
    checkpoint_info, namespace, results_dir, svdc_results_dir
):
    one_hundred_g_ethernet_info = checkpoint_info

    file_name = "one_hundred_g_ethernet.yaml"
    capture_dir = os.path.join(results_dir, "one_hundred_g_ethernet")

    # Ensure that the contents of capture_dir was not from a previous
    # run of the test by deleting capture_dir and its contents
    shutil.rmtree(capture_dir, ignore_errors=True)

    # Create capture_dir
    os.makedirs(capture_dir, exist_ok=False)

    # Loop through each talon board and collect 100G ethernet data
    for one_hundred_g_ethernet in one_hundred_g_ethernet_info:
        talon_target_id = one_hundred_g_ethernet["talon"]
        talon = f"talon-{talon_target_id}"
        svdc_talon_results_dir = os.path.join(svdc_results_dir, talon)
        capture_file = os.path.join(capture_dir, talon, file_name)

        talon_board_device_string = (
            "mid_csp_cbf/talon_board/" + talon_target_id
        )

        kube_exec_str = f"kubectl exec sv-data-collector -n {namespace} -- python3 collector.py one_hundred_g_ethernet -d {talon_board_device_string} -o {svdc_talon_results_dir}"
        _logger.info(kube_exec_str)
        data_collector = subprocess.run(
            [kube_exec_str],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            check=False,
        )

        data_collector_log = data_collector.stderr + data_collector.stdout
        _logger.info(data_collector_log)

        assert not (
            "ERROR" in data_collector_log
        ), "Error while trying to run the signal verification data collector for 100G Ethernet"

        copy_from_data_collector_into_local_file(
            namespace, file_name, svdc_talon_results_dir, capture_file
        )


def copy_from_data_collector_into_local_file(
    namespace, file_name, svdc_dir, capture_file
):
    # copy file from the data collector pod into local file in results dir
    kube_cp_str = f"kubectl cp {namespace}/sv-data-collector:{os.path.join(svdc_dir, file_name)} {capture_file}"
    _logger.info(kube_cp_str)
    subprocess.run([kube_cp_str], shell=True, text=True, check=False)


def get_tango_device(talon):
    """
    returns the tango_device part of the fqdn name from the talon target number
    """
    return f"talondx-{talon}"


def create_data_directory_for_sys_tests(
    namespace: str, results_dir: str, test_id: str
) -> str:
    """creates dir for test id results within system-tests and sv-data-collector

    WARNING: DELETES EXISTING DIRECTORIES if found
    System-tests needs a seperate directory to store results and reports from
    each consecutive test_id run.
    Args:
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name

    Returns:
    : the created results dir for the given test id in system tests
    """
    # create data directory within system-tests results folder
    # Using subprocess command instead of native python mkdir because native
    # does not support creating nested directories while unix command does
    sys_tests_dir = os.path.join(results_dir, test_id)

    if os.path.exists(sys_tests_dir):
        shutil.rmtree(sys_tests_dir, ignore_errors=True)

    _logger.info(f"New test_id specific results directory : {sys_tests_dir}")
    subprocess.run(
        [f"mkdir -p {sys_tests_dir}"],
        shell=True,
        text=True,
        check=False,
    )

    return sys_tests_dir


def create_data_directory_for_sv_data_collector(
    namespace: str, results_dir: str, test_id: str
) -> str:
    """creates dir for test id results within system-tests and sv-data-collector

    WARNING: DELETES EXISTING DIRECTORIES if found
    sv-data-collector needs a space to store data.
    Although running the collector can generate a directory, some data (eg. vis)
    are copied over from other pods and kubectl cp needs the dir to exist.
    So we create it here to forestall any problems.
    Args:
    : namespace: namespace you're using, sv-data-collector pod will be accessed
    : results_dir: base results directory within system-tests
    : test_id: id for the test you are using, will become folder name

    Returns:
    : the created results dir for the given test id in
    sv-data collector
    """

    # signal verification data collector dir generation /app/reports/<test_id>
    sv_data_collector_dir = os.path.join("/app", "reports", test_id)
    # remove the data directory
    kubectl_rmdir_str = f"kubectl exec -n {namespace} sv-data-collector -- rm -rf {sv_data_collector_dir}"
    _logger.info(kubectl_rmdir_str)
    subprocess.run(
        [kubectl_rmdir_str],
        shell=True,
        check=False,
    )
    # create the data directory
    kubectl_mkdir_str = f"kubectl exec -n {namespace} sv-data-collector -- mkdir {sv_data_collector_dir}"
    _logger.info(kubectl_mkdir_str)
    subprocess.run(
        [kubectl_mkdir_str],
        shell=True,
        check=False,
    )

    return sv_data_collector_dir
