Feature: Speedrun38
	#The "Delay models test" test sequence, run as part of the "speedrun38" marker

	Background:
		#@XTP-20799
		Given that the CBF Test Tools are in a nominal state
		And that the CBF is in a nominal state
	
	@XTP-74194 @XTP-28027
	Scenario: Speedrun38
		When the CBF receives the normal sequence of commands for performing a scan with the delay model specified for <test_id>
		Then the checkpoint(s) including visibilities generated by the CBF should meet the expected specification for <test_id>
		Then the observing state(s) of the applicable CBF subarray(s) for <test_id> should be returned to empty.
		
		Examples:
		| test_id |
		| Test_38 |
