Feature: Two scans for same scan configuration command (CORR & PST)

	Background:
		#@XTP-20799
		Given that the CBF Test Tools are in a nominal state
		And that the CBF is in a nominal state

	@XTP-74702 @XTP-71803
	Scenario Outline: Two scans for same scan configuration command (CORR & PST)

		Given a report on <main_scan_test_id> is desired
		And a report on <matching_scan_test_id> is desired
		And <main_scan_test_id> and <matching_scan_test_id> test variables match
		And the BITE is initiated and test parameters are validated and sent

		When the CBF prepares the subarray(s) for test parameters of <main_scan_test_id>
		And the CBF starts the visibility capture pod and executes scan 1 for test parameters of <main_scan_test_id>
		And the next scan is delayed 3 seconds
		And the visibility pod config is prepared to capture <matching_scan_test_id>
		And the CBF, on the same visibility capture pod, performs scan 2 for test parameters of <matching_scan_test_id>
		And the CBF stops the visibility capture pod

		Then the CBF visibilities generated for scan 1 for test parameters of <main_scan_test_id> should meet expectations
		And the CBF visibilities generated for scan 2 for test parameters of <matching_scan_test_id> should meet expectations
		And the observing state(s) of the applicable CBF subarray(s) for <main_scan_test_id> should be returned to empty.

		Examples:
		| main_scan_test_id | matching_scan_test_id |
		| Test_66           | Test_67               |