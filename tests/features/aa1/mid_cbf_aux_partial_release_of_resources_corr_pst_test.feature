Feature: Partial release of resources (CORR & PST)

	Background:
		#@XTP-20799
		Given that the CBF Test Tools are in a nominal state
		And that the CBF is in a nominal state

	@XTP-74701 @XTP-71803
	Scenario Outline: Partial release of resources between two scans test (CORR & PST)

		Given a report on <releasing_test_id> is desired
		And a report on <using_remaining_resource_test_id> is desired
		And the BITE is initiated and test parameters are validated and sent

		When the CBF prepares the subarray(s) for test parameters of <releasing_test_id>
		And the CBF starts the visibility capture pod and executes scan 1 for test parameters of <releasing_test_id>
		And the CBF receives the command to go to idle ObsState
		And the CBF executes the command in <releasing_test_id> to partially release resources
		And the CBF configures for scan 2 for test parameters of <using_remaining_resource_test_id>
		And the CBF, on the same visibility capture pod, performs scan 2 for test parameters of <using_remaining_resource_test_id>
		And the CBF stops the visibility capture pod

		Then the CBF visibilities generated for scan 1 for test parameters of <releasing_test_id> should meet expectations
		And the CBF visibilities generated for scan 2 for test parameters of <using_remaining_resource_test_id> should meet expectations
		And the observing state(s) of the applicable CBF subarray(s) for <using_remaining_resource_test_id> should be returned to empty.

		Examples:
		| releasing_test_id | using_remaining_resource_test_id |
		| Test_53           | Test_54                          |