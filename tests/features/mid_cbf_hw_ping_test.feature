@CIP-1277
Feature: Create basic BDD CBF startup test (Anton)
	#*As* a CIPA team member,
	#*I want to* test that the talon boards are reachable from the PSI
	#*so that* I can execute further tests in the PSI
	#
	#Notes:
	# * No XRAY/JIRA integration.
	# * No saved test artefacts.
	# * Info from Sep 2022: CIP-1180
	# * [BDD testing guide|https://developer.skatelescope.org/en/latest/tools/bdd-test-context.html#]
	#* See CIP-1101 as well.

	#*As* a CIPA team member,
	#*I want to* test that the talon boards are pingable from the PSI,
	#*so that* I can execute further tests in the PSI.
	@XTP-17074 @XTP-17076 @XTP-17075
	Scenario: MID CBF HW Ping Test
		Given that the CBF talon LRUs under test are powered on
		When the CBF talon boards under test are pinged
		Then the CBF talon boards under test should respond with an echo
