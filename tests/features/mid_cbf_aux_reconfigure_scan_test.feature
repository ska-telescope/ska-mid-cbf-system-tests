Feature: Reconfigure scan

	Background:
		#@XTP-20799
		Given that the CBF Test Tools are in a nominal state
		And that the CBF is in a nominal state

	@XTP-30194 @XTP-17075
	Scenario Outline: Overwriting a scan configuration test

		Given a report on <scan_2_test_id> is desired
		And <scan_2_test_id> has the same cbf_input_data as <scan_1_test_id>
		And <scan_2_test_id> has the same resources and delay models as <scan_1_test_id>
		And the BITE is initiated and test parameters are validated and sent

		When the CBF prepares the subarray(s) for test parameters of <scan_1_test_id>
		And the CBF starts a visibility capture pod and configures for scan 1 for test parameters of <scan_1_test_id>
		And the CBF configures for scan 2 for test parameters of <scan_2_test_id>
		And the CBF, on the same visibility capture pod, performs scan 2 for test parameters of <scan_2_test_id>
		And the CBF stops the visibility capture pod

		Then the CBF visibilities generated for scan 2 for test parameters of <scan_2_test_id> should meet expectations
		And the observing state(s) of the applicable CBF subarray(s) for <scan_2_test_id> should be returned to empty.

		Examples:
		| scan_1_test_id | scan_2_test_id |
		| Test_13        | Test_14        |