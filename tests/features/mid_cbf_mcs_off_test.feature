@CIP-1673
Feature: CBF Controller Off Command
	#*As a* CBF user
	#*I want to* have a CBF Off command available to be able to power off the PDUs supplying power to the talon LRUs, and be able to reset the CBF controller and subarrays to a clean operating and observing state
	#*So that* it is possible to power off the CBF at the top level instead of individually interacting with low level devices, and be able to recover the CBF if it gets into a bad state.

	@XTP-27031 @XTP-17075
	Scenario: CBF Controller Off Command
		When the Off command is sent to the CBF Controller
		Then the OpState of the CBF Controller is OFF
		Then the ObsState of the VCCs under test is IDLE
		Then the ObsState of the FSPs under test is IDLE
		Then the ObsState of the subarrays under test is EMPTY
		Then all the CBF FPGA hardware is powered off