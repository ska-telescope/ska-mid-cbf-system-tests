"""Create BDD test for signal chain verification feature tests."""

import json
import logging
import os
import warnings

import pytest
from dotenv import load_dotenv
from lib import (
    datacollector,
    get_parameters,
    scan_operations,
    vis_pod_operations,
)
from lib.constant import LOG_FORMAT
from pytest_bdd import given, parsers, scenario, then, when

load_dotenv()

DM_PACKAGE_DIR = os.path.join(
    os.getcwd(), "test_parameters/delay_model_package"
)
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


##########################################################################################################
@pytest.mark.speedrun1
@scenario(
    "features/mid_cbf_speedrun1_test.feature",
    "Speedrun1",
)
def test_mid_speedrun1(test_report_generator) -> None:
    """Speedrun1"""


@pytest.mark.speedrun2
@scenario(
    "features/mid_cbf_speedrun2_test.feature",
    "Speedrun2",
)
def test_mid_speedrun2(test_report_generator) -> None:
    """Speedrun2"""


@pytest.mark.speedrun3
@scenario(
    "features/mid_cbf_speedrun3_test.feature",
    "Speedrun3",
)
def test_mid_speedrun3(test_report_generator) -> None:
    """Speedrun3"""


@pytest.mark.speedrun4
@scenario(
    "features/mid_cbf_speedrun4_test.feature",
    "Speedrun4",
)
def test_mid_speedrun4(test_report_generator) -> None:
    """Speedrun4"""


@pytest.mark.speedrun6
@scenario(
    "features/mid_cbf_speedrun6_test.feature",
    "Speedrun6",
)
def test_mid_speedrun6(test_report_generator) -> None:
    """Speedrun6"""


@pytest.mark.speedrun15
@scenario(
    "features/mid_cbf_speedrun15_test.feature",
    "Speedrun15",
)
def test_mid_speedrun15(test_report_generator) -> None:
    """Speedrun15"""


@pytest.mark.speedrun17
@scenario(
    "features/mid_cbf_speedrun17_test.feature",
    "Speedrun17",
)
def test_mid_speedrun17(test_report_generator) -> None:
    """Speedrun17"""


@pytest.mark.speedrun21
@scenario(
    "features/mid_cbf_speedrun21_test.feature",
    "Speedrun21",
)
def test_mid_speedrun21(test_report_generator) -> None:
    """Speedrun21"""


@pytest.mark.speedrun22
@scenario(
    "features/mid_cbf_speedrun22_test.feature",
    "Speedrun22",
)
def test_mid_speedrun22(test_report_generator) -> None:
    """Speedrun22"""


@pytest.mark.speedrun23
@scenario(
    "features/mid_cbf_speedrun23_test.feature",
    "Speedrun23",
)
def test_mid_speedrun23(test_report_generator) -> None:
    """Speedrun23"""


@pytest.mark.speedrun27
@scenario(
    "features/mid_cbf_speedrun27_test.feature",
    "Speedrun27",
)
def test_mid_speedrun27(test_report_generator) -> None:
    """Speedrun27"""


@pytest.mark.speedrun28
@scenario(
    "features/mid_cbf_speedrun28_test.feature",
    "Speedrun28",
)
def test_mid_speedrun28(test_report_generator) -> None:
    """Speedrun28"""


@pytest.mark.aa05_fat_4rec
@pytest.mark.nightly
@scenario(
    "features/mid_cbf_signal_chain_verification_test_nightly_and_aa05_FAT_4_receptor.feature",
    "Mid CBF Signal Chain Verification Nightly and AA0.5 FAT 4 receptors",
)
def test_mid_cbf_signal_chain_verification(
    test_report_generator,
) -> None:
    """Mid CBF Signal Chain Verification."""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.aa1_signal_chain_verf_4_receptor_corr
@scenario(
    "features/aa1/mid_cbf_signal_chain_verf_4_receptor_corr_test.feature",
    "AA1 Signal Chain Verification tests (4 receptor set-up, CORR)",
)
def test_aa1_signal_chain_verf_4_receptor_corr(
    test_report_generator,
) -> None:
    """AA1 Signal Chain Verification tests (4 receptor set-up, CORR)"""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.aa1_signal_chain_verf_4_receptor_pst
@scenario(
    "features/aa1/mid_cbf_signal_chain_verf_4_receptor_pst_test.feature",
    "AA1 Signal Chain Verification tests (4 receptor set-up, PST)",
)
def test_aa1_signal_chain_verf_4_receptor_pst(
    test_report_generator,
) -> None:
    """AA1 Signal Chain Verification tests (4 receptor set-up, PST)"""


@pytest.mark.aa1_fat_sat_tests
@pytest.mark.aa1_signal_chain_verf_8_receptor_corr_pst
@scenario(
    "features/aa1/mid_cbf_signal_chain_verf_8_receptor_corr_pst_test.feature",
    "AA1 Signal Chain Verification tests (8 receptor set-up, CORR & PST)",
)
def test_aa1_signal_chain_verf_8_receptor_corr_pst(
    test_report_generator,
) -> None:
    """AA1 Signal Chain Verification tests (8 receptor set-up, CORR & PST)"""


# XTP-20799 Pre-condition


@given(
    parsers.parse(
        "that the observing state of the applicable CBF subarray(s) for {test_id} is/are empty."
    )
)
def that_the_observing_state_of_the_applicable_cbf_subarrays_is_empty(
    run_parameter_guardrail_for_k_values,
    validate_test_parameters,
    send_test_parameters_to_cbf_tools,
    test_id: str,
    all_test_ids_for_scenario: list[str],
    fqdn_subarray,
    event_tracer,
) -> None:
    """that the observing state of the applicable CBF subarray(s) for {test_id} is/are empty."""
    # 'validate_test_parameters' and 'send_test_parameters_to_cbf_tools' are passed here so that the corresponding fixture is executed at the start of the test.
    # 'test_id' will also be used when the subarray is extracted from the test parameters
    _logger.info(
        "system-tests: appending test_id to a list of test ids for report generation"
    )
    all_test_ids_for_scenario.append(test_id)
    _logger.info(
        f"system-tests: test ids for the scenario now include: {all_test_ids_for_scenario}"
    )
    _logger.info(
        "system-tests: checking the state of the subarray and aborting and/or restarting if necessary..."
    )
    pytest.subarray_proxy = get_parameters.create_device_proxy(fqdn_subarray)

    pytest.bite_proxy = get_parameters.create_device_proxy(
        "mid_csp_cbf/ec/bite"
    )

    try:
        scan_operations.subarray_to_empty_state(
            pytest.subarray_proxy, fqdn_subarray, event_tracer
        )
    except AssertionError as e:
        warnings.warn(f"Failed to send subarray to EMPTY; {e}")


@when(parsers.parse("the visibilities pod is created"))
def the_visibilities_pod_is_created(namespace):
    """the visibilities pod is created"""
    vis_pod_operations.create_vis_pod(namespace)


@when(
    parsers.parse(
        "the CBF receives the normal sequence of commands for performing a scan with the test parameters specified for {test_id}"
    )
)
def the_cbf_receives_the_normal_sequence_of_commands_for_performing_scan_with_test_parameters(
    results_dir,
    namespace,
    test_id,
    fqdn_subarray,
    event_tracer,
):
    """the CBF receives the normal sequence of commands for performing a scan with the test parameters specified for {test_id}"""

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    lstv_replay_before_configure_scan = True

    vis_pod_operations.create_capture_config_json(
        test_param_for_test_id, namespace
    )

    scan_operations.configure_subarray_sequence(
        test_param_for_test_id,
        test_id,
        namespace,
        pytest.subarray_proxy,
        pytest.bite_proxy,
        event_tracer,
        fqdn_subarray,
        lstv_replay_before_configure_scan,
    )


@then(
    parsers.parse(
        "the checkpoint(s) in the CBF and visibilities generated by the CBF should meet the expected specification for {test_id}"
    )
)
def the_checkpoints_in_the_cbf_and_visibilities_generated_by_the_cbf_should_meet_the_expected_specification(
    test_id,
    results_dir,
    namespace,
    tango_host,
    event_tracer,
):
    """the checkpoint(s) in the CBF and visibilities generated by the CBF should meet the expected specification for {test_id}"""

    # subfolder within results folder to store the results for this test-id
    # one for system tests, one for the sv-data-collector
    # note that old directories of the same name will be deleted
    sys_tests_results_dir = datacollector.create_data_directory_for_sys_tests(
        namespace, results_dir, test_id
    )

    svdc_results_dir = (
        datacollector.create_data_directory_for_sv_data_collector(
            namespace, results_dir, test_id
        )
    )

    test_params_file = os.path.join(
        results_dir, "test_parameters/test_parameters.json"
    )

    with open(f"{test_params_file}", "r", encoding="utf-8") as params_file:
        test_params = json.load(params_file)

    test_param_for_test_id = test_params[test_id]

    capture_subprocs = scan_operations.vis_pod_setup_capture_procs(
        test_param_for_test_id,
        namespace,
    )

    checkpoints = test_param_for_test_id["checkpoints"]
    dm_config = test_param_for_test_id["delay_model_config"]
    assign_resources = test_param_for_test_id["assign_resources"]

    publisher_proc = (
        scan_operations.subarray_update_delay_models_and_begin_publishing(
            namespace, dm_config, results_dir, assign_resources
        )
    )

    # enabling the timing of data capture to be based on scv tests
    scv_timing = True

    scan_operations.checkpoint_data_capture(
        test_param_for_test_id,
        namespace,
        pytest.subarray_proxy,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        results_dir,
        capture_subprocs,
        scv_timing,
        event_tracer,
    )
    vis_pod_operations.delete_vis_pod(namespace)

    # complete checkpoints for visibilities, which is now collected
    # copies visibilities zip file to sv for validation
    if "visibilities" in checkpoints.keys():
        datacollector.run_data_collector_for_checkpoint(
            "visibilities",
            checkpoints["visibilities"],
            namespace,
            sys_tests_results_dir,
            svdc_results_dir,
            test_id,
            tango_host,
        )

    checks_file_name = f"{test_id}_checkpoints_checks.json"
    scan_operations.validate_checkpoints(
        test_param_for_test_id,
        namespace,
        sys_tests_results_dir,
        svdc_results_dir,
        test_id,
        tango_host,
        checks_file_name,
    )

    checks_json = os.path.join(
        sys_tests_results_dir, str(test_id) + "_checkpoints_checks.json"
    )

    with open(f"{checks_json}", "r", encoding="utf-8") as params_file:
        checks_dic = json.load(params_file)

    high_level_result = checks_dic["high_level_result"]

    _logger.info(
        "Visibility capture step completed. Killing DM publisher process..."
    )
    publisher_proc_stout = (
        scan_operations.subarray_kill_delay_model_publishing(
            namespace, publisher_proc
        )
    )
    _logger.info(f"captured stdout: {publisher_proc_stout}")

    if high_level_result == "PASSED":
        assert True, f"All checkpoints passed for : {test_id}"
    else:
        assert False, f"One or more checkpoints failed for : {test_id}"


# conftest.py: "the observing state(s) of the applicable CBF subarray(s) for {test_id} should be returned to empty."

# pytest_bdd_after_scenario hook - perform clean-up before next iteration of the test

##########################################################################################################
