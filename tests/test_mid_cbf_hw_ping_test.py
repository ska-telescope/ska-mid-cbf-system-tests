"""Basic MID CBF HW Ping Test"""

import logging

import pytest
from dotenv import load_dotenv
from lib import get_parameters, powerswitch, utils
from lib.constant import LOG_FORMAT
from pytest_bdd import given, scenario, then, when

load_dotenv()  # Load environment variables from .env file

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)

NUMBER_OF_PING_ATTEMPTS = 1
NUMBER_OF_PING_REPLIES_REQUIED_FOR_PASS = 1
talon_ping_results = []


@pytest.mark.nightly
@scenario("features/mid_cbf_hw_ping_test.feature", "MID CBF HW Ping Test")
def test_mid_cbf_hw_ping_test():
    """MID CBF HW Ping Test."""


# check the lru status
@given("that the CBF talon LRUs under test are powered on")
def that_the_cbf_talon_lrus_under_test_are_powered_on(
    talon_list, hw_config_file
):
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talon_lrus_under_test_list = get_parameters.get_talon_lrus_under_test_list(
        talon_list, hw_config
    )
    for lru in talon_lrus_under_test_list:
        lru_status_check = powerswitch.lru_status(lru, hw_config)
        _logger.info(f"lru {lru} status check is: {lru_status_check}")
        assert lru_status_check


# ping talon boards
@when("the CBF talon boards under test are pinged")
def the_cbf_talon_boards_under_test_are_pinged(talon_list, hw_config_file):
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talon_ping_results = utils.return_talon_ping_results(
        talon_list, NUMBER_OF_PING_ATTEMPTS, hw_config
    )
    _logger.info(talon_ping_results)


# Evaluating test pass/fail status based on how many /
# replies were received and how many were required
@then("the CBF talon boards under test should respond with an echo")
def the_cbf_talon_boards_under_test_should_respond_with_an_echo():
    for ping_result in talon_ping_results:
        talon = ping_result.split(" ")[1]
        number_of_replies_received = ping_result.split(" ")[5]
        assert (
            int(number_of_replies_received)
            >= NUMBER_OF_PING_REPLIES_REQUIED_FOR_PASS
        ), (str(talon) + "failed")
