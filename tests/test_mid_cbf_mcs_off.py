"""Create BDD test for MCS off command."""

import logging

import pytest
from assertpy import assert_that
from dotenv import load_dotenv
from lib import get_parameters, powerswitch, utils
from lib.constant import LOG_FORMAT
from pytest_bdd import scenario, then, when
from ska_control_model import ObsState, ResultCode
from tango import DevState

load_dotenv()

logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


@pytest.mark.nightly
@pytest.mark.off_test
@scenario(
    "features/mid_cbf_mcs_off_test.feature",
    "CBF Controller Off Command",
)
def test_mid_cbf_mcs_off_test():
    """CBF Controller Off Command"""


@when("the Off command is sent to the CBF Controller")
def the_off_command_is_sent_to_the_cbf_controller(
    cbfcontroller_proxy, event_tracer, fqdn_cbfcontroller
):
    _logger.info("executing MCS off command")

    result = cbfcontroller_proxy.command_read_write("Off")
    utils.assert_result_code_is_expected(
        "cbfcontroller", "Off", result, ResultCode.QUEUED
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_MEDIUM
    ).has_change_event_occurred(
        device_name=fqdn_cbfcontroller,
        attribute_name="state",
        attribute_value=DevState.OFF,
    )

    assert_that(event_tracer).within_timeout(
        utils.TIMEOUT_MEDIUM
    ).has_change_event_occurred(
        device_name=fqdn_cbfcontroller,
        attribute_name="longRunningCommandResult",
        attribute_value=(
            f"{result[1][0]}",
            '[0, "Off completed OK"]',
        ),
    )


@then("the OpState of the CBF Controller is OFF")
def the_opstate_of_the_cbf_controller_is_off(
    cbfcontroller_proxy, state_str, off_state_str
):
    utils.check_dev_state(
        "cbfcontroller", cbfcontroller_proxy, state_str, off_state_str
    )


@then("the ObsState of the VCCs under test is IDLE")
def the_obsstate_of_the_vccs_under_test_is_idle(vcc_max, obs_state_str):
    for i in range(1, int(vcc_max) + 1):
        vcc_fqdn = "mid_csp_cbf/vcc/00" + str(i)
        vcc_proxy = get_parameters.create_device_proxy(vcc_fqdn)
        utils.check_dev_state(
            vcc_fqdn, vcc_proxy, obs_state_str, ObsState.IDLE
        )


@then("the ObsState of the FSPs under test is IDLE")
def the_obsstate_of_the_fsps_under_test_is_idle(fsp_max, obs_state_str):
    for i in range(1, int(fsp_max) + 1):
        fsp_corr_fqdn = "mid_csp_cbf/fspcorrsubarray/0" + str(i) + "_01"
        fsp_corr_proxy = get_parameters.create_device_proxy(fsp_corr_fqdn)
        utils.check_dev_state(
            fsp_corr_fqdn, fsp_corr_proxy, obs_state_str, ObsState.IDLE
        )


@then("the ObsState of the subarrays under test is EMPTY")
def the_obsstate_of_the_subarrays_under_test_is_empty(
    subarray_proxy_default,
    obs_state_str,
):
    utils.check_dev_state(
        "subarray", subarray_proxy_default, obs_state_str, ObsState.EMPTY
    )


@then("all the CBF FPGA hardware is powered off")
def all_the_cbf_fpga_hardware_is_powered_off(talon_list, hw_config_file):
    hw_config = get_parameters.read_yaml_file(hw_config_file)
    talon_lrus_under_test_list = get_parameters.get_talon_lrus_under_test_list(
        talon_list, hw_config
    )
    for lru in talon_lrus_under_test_list:
        lru_status_check = powerswitch.lru_status(lru, hw_config)
        _logger.info(f"lrus are off= {not lru_status_check}")
        assert not lru_status_check
