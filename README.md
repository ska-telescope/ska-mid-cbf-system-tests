# SKA MID CBF System Tests

This repository is used for BDD component-level testing of the MID CBF.

Code repository: [ska-mid-cbf-system-tests](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests)

Documentation on the Developer's portal:
[ReadTheDocs](https://developer.skao.int/projects/ska-mid-cbf-system-tests/en/latest/)

## Deploying the Namespace

The deployment of the namespace in the MID PSI is now controlled from the system-tests pipeline and no longer via SKAMPI.

### Selecting the CBF versions to deploy

The versions of MCS, Signal Verification (SV), and Engineering Console (EC) deployed to your namespace are all controlled in system-tests. If `USE_DEV_BUILD` is set to `true` (default behaviour), the latest hash build versions of MCS, SV, and EC from their respective main branch will be deployed. 

To override the default behaviour in order to test a specific version of MCS from another branch (not main), manually set the `MCS_HASH_VERSION` Gitlab pipeline variable during the `on_demand_psi` stage. And similarly for SV and EC. The hash version must follow the pattern: `<latest_tag>-dev.c<commit_hash>`. 

If `USE_DEV_BUILD` is set to `false`, the latest tagged versions will be deployed. 

The Makefile logic will also handle updating the internal-schemas version and source in the `pyproject.toml` and regenerate the `poetry.lock` file, if necessary.

During `test` stage, the Makefile logic will also update the `dish_packet_capture_$(TARGET_SITE).yaml` and `visibilities_pod_$(TARGET_SITE).yaml` appropriately.

#### MCS

The version of MCS is controlled via the the Chart.yaml file in the following directory: charts/ska-mid-cbf-system-tests/Chart.yaml under the header ska-mid-cbf-tdc-mcs and ska-mid-cbf-tdc-tmleafnode.

#### Engineering Console (EC)

The version of EC is controlled via the the Chart.yaml file in the following directory: charts/ska-mid-cbf-system-tests/Chart.yaml under the header ska-mid-cbf-engineering-console.

#### Signal Verification (SV) 

The version of SV is controlled via the values.yaml file in the following directory: charts/ska-mid-cbf-system-tests/values.yaml under the section ska-mid-cbf-tdc-mcs.

#### Internal Schemas

The version of internal-schemas that gets installed during the poetry install command is defined in the poetry.lock file. 

To change the desired version of internal-schemas, you must update the version on the ska-mid-cbf-internal-schemas line of pyproject.toml. If you want to install the internal-schemas package from CAR, also change the source from "gitlab-internal-schemas" to "nexus-internal". Once those changes are saved, run "poetry lock --no-update" to refresh the lock file with the correct internal-schemas version and source.

### Starting the namespace

Once you have made changes to your feature branch. 

<ol>
  <li>Commit your changes and push them to remote. </li>
  <li>Open https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/pipelines</li>
  <li>A new pipeline should appear in the gitlab GUI associated to your feature branch.</li>
  <li>Click on the stage on_demand_psi (2nd stage in the pipeline). Manually click play button next to the job: psi-mid-on-demand-deploy </li>
  <li>To know which namespace is associated to your pipeline. Record the Pipeline ID associated to your pipeline in the gitlab GUI (starts with "#").</li>
  <li>On the PSI Head node (rmdskadevdu011). Run the following command: kubectl get ns </li>
  <li>Your namespace will be ci-ska-cbf-sys-tests-(PIPELINE_ID)-mid </li>
  <li>Record this namespace name if you want to run command line tests against this namespace or if you want to run kubectl commands to monitor/track the services and pods</li>
</ol>

## Running Tests

There are two methods for runnning the system-tests: via gitlab or via commandline. They are described below. For more information on using the PSI, see <https://confluence.skatelescope.org/display/SE/MID+PSI+Preliminary+MID+CBF+Testing>

### Testing your feature branch via gitlab

<ol>
  <li>Open https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/pipelines</li>
  <li>Find your PIPELINE ID recorded from when you deployed the namespace. Under Stages, select the bubble for test. Select the edit wheel to configure the test job.</li>
  <li>Add PYTEST_MARKER (e.g., nightly)</li>
  <li>Add TALON_LIST (e.g., 002)</li>
  <li>Set HW_CONFIG_FILE to the yaml file that matches your environment in the test_parameters/hw_config directory. Default to hw_config_psi.yaml.
  <li>If using multiple VCCs or FSPs, set SLIM_FS_CONFIG_FILE to the fs_slim_*.yaml file in the test_parameters/slim_config directory. All frequency slice SLIM links are disabled if not provided, in which case only single board tests are supported.</li>
  <li>If using multiple FSPs, set SLIM_VIS_CONFIG_FILE to the vis_slim_*.yaml file in the test_parameters/slim_config directory. All visibility SLIM links are disabled if not provided, in which case only one FSP is supported.</li>
  <li>If testing a particular correlation bitstream set CORR_BITSTREAM_VERSION to the desired release version (e.g. 0.5.0). This manual setting is dependent on TARGET_SITE as manual bitstream programming does not occur with "itf" or "karoo".</li>
  <li>Select "Run job"</li>
</ol>

### Testing your feature branch via commandline

#### INITIAL SETUP
The following steps are necessary to configure the environment for the first time. Run the following steps in an SSH terminal on the PSI head node (rmdskadevdu011.mda.ca):
```bash
git clone https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests.git

#After initial clone or a submodule (e.g., ska-cicd-makefile) is updated:
	cd .../ska-mid-cbf-system-tests
	git submodule init
	git submodule update
		
#Run the following commands to setup your python virtual environment so that the pytests can be executed:
	python3.10 -m venv venv
	source venv/bin/activate
	python -m pip install poetry==1.3.2
	poetry install
```

#### SUBSEQUENT TESTING
Once the environment has been set up, only the following commands need to be executed.
```bash
cd .../ska-mid-cbf-system-tests
source venv/bin/activate
make python-test KUBE_NAMESPACE=... PYTEST_MARKER=... TALON_LIST=...
```
Note the KUBE_NAMESPACE is the namespace recorded when following the deployment instructions above

Note that there are other options for python-test as well.

## Pylint

Note that some of the lint issues cannot be resolved due to the nature of the BDD tests and those errors have been added to the disabled list when pylint is run in the pipeline. 

Run the following command locally to make sure your code will pass the lint test in the pipeline:

```bash
cd .../ska-mid-cbf-system-tests
source venv/bin/activate
make python-lint
```

## Notes

Even though the receptors to be released can be specified in release_receptors.json, currently, all the receptors will be released at the beginning and end of each test as part of subarray_to_empty_state() call. 

## Timeouts

If a system-tests job fails due to a job exceeding a duration timeout, the shorter of the following two timeout parameters has been reached and must be increased:
- The system-tests project CI/CD timeout, found in Settings -> CI/CD -> General Pipelines.
- The Gitlab ska-psi-mid-runner timeout set by the systems team (this can be requested by creating an STS ticket).
It is recommended to keep both timeout parameters in sync and beyond the expected maximum duration of a python-test job.
