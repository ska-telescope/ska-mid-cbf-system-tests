############
Change Log
############

All notable changes to this project will be documented in this file.

2025-Feb-20
***********
* CIP-3433 update lru5 and lru8 related outlets in hw_config_swap_9_10_15_16_psi.yaml for HW change

2025-Feb-11
***********
* CIP-3403 change talon2 ip to talon17 ip according to renaming, remove hw_config_swap_11_14_psi.yaml as corresponding boards will be sent to karoo and will be unavailable

2025-Feb-10
***********
* CIP-2984 add script to compile list of test ids that support a given requirement based on the tests.json.

2025-Feb-07
***********
* CIP-3129 Add Test_68/69 8-receptor correlation coupling tests, remove old AA0.5 8 receptor FAT test infrastructure.
* CIP-3350 Test_35 to use Matlab delay models rather than 50ns constant offset. Adjust Test_28 visibilities plots to not plot detailed baselines.

2025-Feb-06
***********
* CIP-3386 Fix incorrect fqdn_subarray access in after_scenario, improve after_scenario global var interaction, and decouple cleanup from feature names

2025-Feb-05
***********
* CIP-2984 Test parameter changes and corresponding infrastructure for AA1 PST Tests, also adjustment to vis_capture_config to link pst output_host to the visibility pod.
* CIP-3331 Upload new delay models with signal generation t_start of zero.

2025-Feb-04
***********
* CIP-3350 Save eeprom information as part of test execution in test artefacts test_parameters folder talon_board_info.txt.

2025-Jan-31
***********
* CIP-3368 Prioritize correlation bistream QSPI for programming and improve assertion points for QSPI programming sequence

2025-Jan-28
***********
* CIP-3205 Extend all 4 VCC 4 checkpoint checkpoints to 4s capture_duration to avoid RDT collection coincidence with EndScan

2025-Jan-27
***********
* CIP-3314 Add alternate delay model for 4_VCC_T3 test to workaround mcs ignoring of repeating of delay models for common occurences of Test_37 tested after Test_36 (and similarly Test_44 after Test_43)

2025-Jan-22
***********
* CIP-3127 Add variable bitstream in env var CORR_BITSTREAM_VERSION
* CIP-3092 Add minor changes to slim_config to match current understanding of AA1 plan
* CIP-3311 Fix version of poetry for ReadtheDocs build to 1.8.0

2025-Jan-21
***********
* CIP-3160 Remove unused eth100g diagnostics

2025-Jan-17
***********
* CIP-3288 Add Test_42-46 sample sample rate delay model tests (same k versions of Test_35-39) under marker same_sample_rate_delay_model_tests and add init_sys_param with same sample rate config 4_board_same_k

2025-Jan-16
***********
* CIP-3233 add Test_20 with a tone to nightlies and create speedrun38
* CIP-3284 fix start_freq of scans to place range fully within a single valid frequency band

2025-Jan-10
***********
* CIP-3264 adjust mcs default timeout to 100, minor fix to off_test timing, and make CONTROLLER_TIMEOUT adjustable

2025-Jan-07
***********
* CIP-3015 change delay model packages for BITE and the signal chain for the delay model tests. Change polarization coupling of delay model tests to match Test_21 and Test_41.

2024-Jan-06
***********
* CIP-3249 add new swap hw_config file for talon9-12 and standardize swap to similar naming format
           minor timing fix to default MCS on timeout.
* CIP-3242 temporarily disabled 100g checkpoint

2024-Dec-23
***********
* CIP-3160 add temporary hooks to help collect diagnostic network data

2024-Dec-19
***********
* CIP-3231 added configurable MCS device timeouts to values, and timeout for controller HPS master calls as CONTROLLER_TIMEOUT var
* CIP-3246 remove never rules for newly incompatible ci job oci-image-scan, oci-image-scan-from-car, and python-gemnasium-scan
* MAP-215  add cleanup_artifacts.sh script to clean up the system-test-artifacts PVC

2024-Dec-18
***********
* CIP-3131 added requirement mapping to the tests for the AA0.5 requirements, as per post-QDR VCD that will be submitted to SKA shortly

2024-Dec-17
***********
* CIP-3216 point to forked MCS repo ska-mid-cbf-tdc-mcs rather than previous ska-mid-cbf-mcs

2024-Dec-13
***********
* CIP-3211 change visibility checkpoint mean phase check to a mean delay check

2024-Dec-09
***********
* CIP-2265 add target_site var and conditional to disable qspi programming if it is in "itf" or "karoo"
* CIP-2953 change PVC pod name from sys-tests-artifacts-pod to artifacts-sts-0

2024-Dec-06
***********
* CIP-3160 increase integration factor of Test_5/6/7/8/10/11/12/13/14/15/16/17/23/31/32/33/34, remove redundant Test_20, minor fixes
* CIP-3148 create Test_41 and modify Test_21 for testing sources with correlated and uncorrelated noise in the y pol

2024-Dec-05
***********
* CIP-3099 adding controller and subarray health checks to CBF nominal state pre-condition - to be enabled upon completion of CIP-2851 and CIP-2868
* CIP-3160 increase integration factor of Test_10/22/27/28/29/30 to 10 to reduce network load

2024-Nov-29
***********
* CIP-1398 adding 100G ethernet checkpoint

2024-Nov-27
***********
* CIP-1396 adding resampler delay tracker checkpoint

2024-Nov-26
***********
* CIP-3148 reduce delay model visibilities capture time and lstv gen time by 10 seconds each
* CIP-2494 new bite config added for delay model tests and sequence diagrams for delay models tests were added to README

2024-Nov-25
***********
* CIP-3071 delay models generate vis zip file and vis capture was moved to a separate function

2024-Nov-21
***********
* CIP-3149 typo in given step was fixed, test_39 passing now
* CIP-2953 Add redirection of visibilities to PVC in pod sys-tests-artifacts-pod in namespace system-tests-artifacts

2024-Nov-20
***********
* CIP-2953 Revert artifact retention to 2 weeks and temporarily delay schedule pipeline deletion for 12h

2024-Nov-19
***********
* CIP-2265 Construct notebook for a manual test (AA0.5 5.3.5) demonstrating system monitoring, failure, recovery, and investigation
* CIP-2953 Temporarily reduce gitlab job artifact retention to 2 days and add script for deletion of job artifacts before a specified cutoff

2024-Nov-18
***********
* CIP-2494 one separate feature file and test were added for post rdt changes delay model test

2024-Nov-15
***********
* CIP-3071 added first draft of the fix for delay model tests

2024-Nov-07
***********
* CIP-2836 remove unused pre adr-99 functionality

2024-Nov-06
***********
* CIP-2494 added nightly tag to delay tests.
* CIP-2659 MCS On asserts all TalonBoard healthState are OK, as Controller On might pass with partial success

2024-Nov-05
***********
* CIP-2494 delay model tests added.

2024-Nov-01
***********
* CIP-2836 All configure scan versions in tests changed to ADR-99 version. Add speedruns 17, 19, and 28.
* CIP-3091 Fix Makefile hash update to grab latest tag from .release file in each main repo.
* CIP-2836 All configure scan versions in tests changed to ADR-99 version. Add speedruns 17, 21, 22, 27 and 28.

2024-Oct-28
***********
* Additional 'chmod 777' step and better instructions added to the *HACK* section for local testing custom HPS binaries.

2024-Oct-24
***********
* CIP-2967 Delay model publish sequence now standardized across aux tests and SCV

2024-Oct-23
***********
* CIP-3037 Add hw_config_13_16_psi.yaml to allow running of targets 001-004 on talon13-16

2024-Oct-22
***********
* CIP-3023 Fix Makefile hash auto-update to work around rc version, defaulting back to latest non-rc is not associated with it

2024-Oct-21
***********
* CIP-3028 Updated hw config after systems room re-organization.

2024-Oct-10
***********
* CIP-2020 Reboot talon boards during session start

2024-Oct-04
***********
* CIP-2824 Remove ska-tango-alarmhandler, add project to ReadtheDocs (RTD), and minor CI/helm/make updates

2024-Sept-30
***********
* CIP-2947 Temporarily change all talons in hw_config to use PDU 004
* CIP-2889 copy name of slim_fs_config_file used by the test to sv pod to be used in PSR and DCT checkpoint validation

2024-Sept-25
***********
* CIP-2265 major refactor and multiple fixes/changes for auxiliary tests

2024-Sept-25
***********
* CIP-1143 support LSTV resampler, provide delay models to BITE. Use talon target (e.g. 001) for BITE.
           added a set of 4 receptor CSVs and delay model package.

2024-Sept-18
***********
* CIP-2425 update to use long running commands from base class upgrade

2024-Sept-16
***********
* CIP-2847 move low/high k tests into Test_1/2 removing old 4_boards_low_k, 4_boards_high_k, and Test/Speedrun_25/26

2024-Sept-13
***********
* CIP-2312 multiple fsps can be processed with vis data for each saved to an individual zip file

2024-Aug-30
***********
* CIP-2617 re-enabling validation of test parameters
* CIP-2102 adding expected channels validation to visibilities checkpoint

2024-Aug-29
***********
* CIP-2545 replace talon board hostname (e.g. talon1) with talon target (e.g. 001).

2024-Aug-26
***********
* CIP-1873 add script to convert delay models from CSV to JSON format

2024-Aug-21
***********
* CIP-2637 storing all test parameters for the test being run into the visibilities pod config file

2024-Aug-19
***********
* CIP-2243 Add 8 receptor tests
* CIP-2003 Remove Test_14 and update other tests to use the "37 consecutive delay models"

2024-Aug-13
***********
* CIP-2243 add filters to tests with multiple receptors to be able to distinguish between baselines

2024-Aug-09
***********
* CIP-2003 add Test_14 for signal chain verification with multiple delay models and add delay_model_config to test parameters
* CIP-2243 Add Test 27 and 28 with multiple correlator processing regions (CPRs)

2024-Aug-07
***********
* CIP-2508 Update System Tests to send 3.0 versions of configure_scan, assign_resources, and release_resources

2024-Aug-02
***********
* CIP-2615 Refactoring and changing location of k-value guardrail.

2024-Jul-25
***********
* MAP-99 Updating the fs and vis slim directory paths for the file copy into the pods

2024-Jul-23
***********
* CIP-2243 Add 2 VCC, 2 FSP test with pre ADR-99 scan configuration

2024-Jul-22
***********
* CIP-2243 Add suite of tests for AA0.5 focusing on configure scan, including post ADR-99 tests.

2024-Jul-15
***********
* MAP-117  Add USE_DEV_HASH, MCS_HASH_VERSION, EC_HASH_VERSION, SV_HASH_VERSION, and INTERNAL_SCHEMA_HASH_VERSION.
           If USE_DEV_HASH=true, update the relevant chart files with the latest MCS, EC, SV gitlab hash from the main branch.
           If USE_DEV_HASH=false, the latest tagged versions in CAR will be used and deployed.

2024-JUL-05
***********
* CIP-1397 Updated the DDR4 Corner Turner checkpoint verification to align with the changes to the DDR4 Corner Turner schema.

2024-Jul-02
***********
* CIP-2510 Added delay_model_package subarray to parameter guardrails.

2024-Jun-26
***********
* CIP-2552 pull internal-schemas python package from gitlab package registry
* CIP-2572 Added tcpdump to run and capture packets alongside of visibility capture

2024-Jun-21
***********
* CIP-2552 pull mcs, engineering-console, and signal-verification from gitlab hash instead of CAR

2024-Jun-11
***********
* CIP-2506 Made visibility capture and checkpoint data collector run concurrently
* CIP-1750 Create logger for system tests and use in most BDD tests.

2024-Jun-07
***********
* MAP-59: Swap to use new TANGO device for BITE in utils.py. Instead of using namespace and mac address, now need to supply a bite device and test_id to run. Update k-value guardrails to deal with not needing to upload config files. Update log collection to capture all logs
* MAP-60: Replace calls to ec-deployer with DP calls to ds-deployer. Update device_check test to use new log format from device. Replaced extraneous calls to ec-deployer where found in repo. 

2024-May-29
***********
* SKB-367 refactored logic in FSP-App to handle an alternate format for configure_scan["output_port"]
* Various other EC changes included in the 0.10.8 release by John to address test6 bugs (see REL-1489)

2024-May-27
********
* CIP-1982 update to packet stream repair checkpoint properties, including internal-schemas bump to 0.4.2, sv bump to 0.2.24
* CIP-1982 temporarily disable validate_test_parameters schema validation in conftest.py - to be added back in CIP-2003

2024-May-21
********
* CIP-2524 sending the correct file to update_qspi_remote.sh 

2024-May-16
********
* CIP-2491 bump sv to 0.2.22 so that memory usage data is shown in vis capture output

2024-May-14
***********
* added commented hacks to bypass eng console release to update HPS binary versions 
  for test runs in conftest.py
* CIP-2498: uncommented above hack to update ska-mid-cbf-fsp-app version to 0.4.11

2024-May-09
***********
* CIP-2492 K8S_TIMEOUT and visibility capture timeout

2024-May-02
***********
* CIP-1938 guardrails added for k_value and subarray_id.
* CIP-2453 stop turning off LRUs between test cases again
* CIP-2445 Adding script to run after logs are captured in log-collector. Generates a summary file containing logging level counts for all logs.

2024-May-01
***********
* CIP-2368 baseline ordering fix and EC 0.10.6

2024-Apr-29
***********
* CIP-1708 setting all test parameter integration factors to 1 in confgure_scan.json and visibility capture durations to 1.5 seconds in checkpoints.json
* CIP-2453 reverted to turning off LRUs between test cases

2024-Apr-26
***********
* CIP-2326 fix the visibility capture duration, including SV bump to 0.2.21

2024-Apr-25
***********
* CIP-2266: subarray_proxy was parametrized

2024-Apr-23
***********
* CIP-1983 stop power cycling LRU with tests again, update MCS to 0.14.2

2024-Apr-19
***********
* CIP-2307: SV bump to 0.2.20 to fix horizontal line through new visibilities plots

2024-Apr-17
***********
* CIP-1989: repo improvements

2024-Apr-16
***********
* CIP-2418 temporarily fix: turn lru off after QSPI update

2024-Apr-12
***********
* CIP-1983 stop power cycling LRU with tests, update MCS to 0.13.3

2024-Apr-11
***********
* CIP-2103: update visibilities checkpoint properties and critiera, including updating SV to 0.2.19 and internal schemas to 0.3.8

2024-Apr-10
***********
* MAP-78: bumped EC version to 0.10.2 which fixes change in CAR url

2024-Apr-08
***********
* CIP-2238: bumped MCS version to 0.13.1, eng console to 0.10.1, bumped ska-tango-base/util charts to latest

2024-Apr-05
***********
* CIP-1582 - Added details on the hardware and software configuration of the system under test to the test results.

2024-Mar-25
***********
* CIP-2114: Added a warning log to bdd_after_scenario when lru powerswitch is not in the correct state.

2024-Mar-22
***********
* CIP-2265: Aux tests added, repo enhancement.

2024-Mar-21
***********
* CIP-2264: Updated EC to 0.10.0. FPGA bitstream updated to 0.2.6.

2024-Mar-19
***********
* CIP-1998: Deploy alarm handler
* CIP-2007: Updated EC to 0.9.24. Bitstream updated to fix visibilities SPEAD heaps being sent to different ports.

2024-Mar-14
***********
* CIP-2306: Updated MCS to 0.12.28, stops ConfigureScan from being callable from scanning state.

2024-Mar-13
***********
* CIP-1866: Entire results folder was added to artifacts
* CIP-2109: SV bump to 0.2.17

2024-Mar-12
***********
* CIP-2228: Rearranged test parameter locations: "sample_rate_k" now located at cbf_input_data.json level, instead of in bite_configs.json.
* CIP-2228: Added --test (test ID) argument to talon_bite_lstv_replay(), since correct k-value must be used in configuring BITE for LSTV replay.
* CIP-2228: Removed section of conftest.py that copied init_sys_param.json to EC-BITE pod, which was unnecessary, misleading, and would lead to problems.
* CIP-2228: Updated EC to 0.9.22, updated internal-schemas to 0.3.7.

2024-Mar-04
***********
* CIP-1825: Removed unused test parameters

2024-Mar-01
***********
* CIP-1825: Added a function to find unused test parameters

2024-Feb-28
***********
* CIP-2007: Added DCT checkpoint

2024-Feb-27
***********
* CIP-1880: Updated SV to 0.12.16; Add test parameters to report generation call
* CIP-1979: Removed channel_offset from all basic scan test configurations.
* CIP-2105: Updated MCS to 0.12.26; Fixed FSP error from trying to remove group proxy from IDLE funciton mode.

2024-Feb-26
************
* CIP-1718: Updated Signal Verification to 0.2.15; Removed unused entries from configure scan test parameters; Removed vis_config and replaced by generating from test fsp config

2024-Feb-22
***********
* CIP-2053: Updated EC to 0.9.16; Fixes SPEAD Descriptor's channel_id assignment in FSP App
* CIP-1979: Updated MCS to 0.12.25; Sets default FSP channel_offset when configuration does not provide one.

2024-Feb-16
***********
* CIP-2048: Updated MCS to 0.12.23. Implements SlimLink ping check.
* Created missing fs slim configs for single board testing (only one for talon1 existed previously).

2024-Feb-14
***********
* CIP-1852 Updated Signal Verification to 0.2.14, removed report generation feature files, and moved report generation to fixtures

2024-Feb-13
***********
* CIP-2050 Updated MCS from 0.12.21 to 0.12.22

2024-Feb-07
***********
* CIP-1356 Updated MCS from 0.12.20 to 0.12.21

2024-Feb-02
***********
* CIP-2050 Updated MCS from 0.12.19 to 0.12.20
* EC 0.9.11 -> 0.9.14: 
  * CIP-1940 EC 0.9.13 to update VCC App and WIB versions
  * CIP-2000 EC 0.9.14 to fix RDT issue during delay model initialization. Shorten scan and capture time.

2024-Feb-01
***********
* CIP-2098 Modified sample_rate_k values in BITE config parameter file (bite_configs.json) so that all k values meet requirements

2024-Jan-31
***********
* CIP-2048 Updated the MCS from 0.12.18 to 0.12.19
* CIP-1984 add timeout to nc command, reduce sleep after update delay model to 5s

2024-Jan-30
***********
* CIP-2020 Added networking debug logs for when MCS on command fails
* CIP-1984 updated MCS to 0.12.18, Taranta to 2.5.1 for dashboard supports

2024-Jan-24
***********
* CIP-2052 updated MCS from 0.12.16 to 0.12.17.

2024-Jan-22
***********
* CIP-2047 Bumped EC to 0.9.11, Bumped SV to 0.2.13, and fixed frequency band and frequency slice to circuit switch conversion off by one error
* CIP-2052: Updated DsSlimTxRx to v2.1.2

2024-Jan-22
***********
* CIP-2004 Test_6 updates for the histograms checkpoint.

2024-Jan-19
***********
* MAP-28 Moving the visibility pod to inside the app namespace

2024-Jan-17
***********
* CIP-2004 updated MCS from 0.12.14 to 0.12.16
* CIP-2006 SLIM_CONFIG_FILE marker renamed to SLIM_FS_CONFIG_FILE and added Slim Vis config as additional CI/Makefile marker SLIM_VIS_CONFIG_FILE.

2024-Jan-16
***********
* CIP-1752 Data collection steps moved out of utils.py to datacollector.py 
* CIP-1752 general clean up for data collection steps
* CIP-1752 PYTEST_MARKER added to xray publish

2024-Jan-16
***********
* MAP-28 Updating files and adding CI and Makefile variables to allow testing at both the PSI and and the ITF. Updated python tests to use these new variables


2024-Jan-12
***********
* CIP-1987 Bumped EC to 0.9.10, updated FQDNs in SlimLink configs to align with EC update.
* CIP-1770 Added Slim and SlimLink devices to tests/.env to improve logging.
* CIP-1770 update SV to 0.2.12 which slightly modifies slim link checkpoint functionality. 0.2.11 was updates to talon status checkpoint and modifications to visibility capture to improve Test 6.

2024-Jan-08
***********
* CIP-1980 Save hw_config_file and slim_config_file file in artifacts on gitlab after test run

2024-Jan-05
***********
* CIP-1980 Updated SV to 0.2.10 which contains new reports and updated report generation accordingly in signal-chain-verification bdd tests (note: breaking change).
* CIP-1524 Removed unused Test_8 and cbf_input_data parameters. Fixed Test_7 description.

2024-Jan-05
***********
* CIP-2004 Add Test_6 speedrun so it can be tested faster

2024-Jan-04
***********
* CIP-1965 Updated EC to 0.9.9, which in turn updates the mesh-test to parse for generic configurations.

2024-Jan-03
***********
* CIP-1742 Link to full report on Jira execution no longer redirects but goes directly to report

2024-Jan-02
***********
* MAP-36 Adding powerswitch functions for DLI PDU and APC SNMP PDU
* MAP-36 Refactoring code so that all powerswitch functions are in separate file

2023-Dec-22
***********
* CIP-1770 added slim links checkpoint functionality and add checkpoints to Test_6
* CIP-1770 bump SV to 0.2.9 which also contains necessary slim links checkpoint functionality

2023-Dec-21
***********
* CIP-2006 Fixed bug that set default slim config to hw_config.yaml. Added missing SLIM_CONFIG_FILE definition to gitlab-ci.yml

2023-Dec-20
***********
* CIP-2004: updates to checkpoint_historgrams so the correct timeout map is sent to data collector

2023-Dec-19
***********
* CIP-1830: bump EC to 0.9.8, MCS to 0.12.12, cast delay model epoch to integer

2023-Dec-18
***********
* CIP-2001: bump MCS to 0.12.11 to set fo_validity_interval back to 0.01
* CIP-1883: change subarray ID from 0-index to 1-index

2023-Dec-16
***********
* CIP-2004: Added Test 6 to the nightly tag with temporary configuration

2023-Dec-15
***********
* CIP-2006: Updated MCS to 0.12.10, which renames SlimMesh to just Slim. Updated EC to 0.9.6, which updates ds-slim-tx-rx to v2.1.1.

2023-Dec-14
***********
* CIP-1674: Updated MCS to 0.12.9, which fixes the issue where talondx-logconsumer was logging each message twice.

2023-Dec-12
***********
* CIP-1769: Updated MCS to 0.12.8, which implements SLIM and SLIM Link device servers.
* CIP-1524 Add new gaussian noise configurations to bite_configs.json, to exercise non-standard mean and standard deviation parameters written to the gaussian noise generator as normalized values (in the ranges of -1.0 to 1.0 for mean, and 0.0 to 1.0 for standard deviation)
* Add new tests that use these configurations: test_7 configures non-standard gaussian noise on talons 1-4; test_8 configures non-standard gaussian noise for just talon1. Add Test_7 to nightly test marker. 

2023-Dec-11
***********
* MAP-28: Dynamic visibility pod spawning upgrade

2023-Dec-08
***********
* CIP-1758: Bump EC to 0.9.5

2023-Dec-06
***********
* CIP-1967 Update MCS to 0.12.7 to revert fo_validity_interval back to 0.001, while CIP-2001 is still unresolved.
* Bump SV to 0.2.7

2023-Dec-04
***********
* CIP-1967, CIP-1870 Update MCS to 0.12.5 to fix fo_validity_interval and talon ds timeout 

2023-Nov-30
***********
* CIP-1957 Updated MCS to 0.12.4 to remove vcc gains file for receptor 1 band 1, also remove internal_params.json from system tests as it is no longer copied to MCS

2023-Nov-29
***********
* Bump SV to 0.2.6
* Bump EC to 0.9.4
* CIP-1935 visibilities pod "Pending" - print timestamp of pod creation and status, delete pod if stuck in "Pending"

2023-Nov-23
***********
* CIP-1933 Updated MCS to 0.12.3, SV to 0.2.5, EC to 0.9.2
* CIP-1917 Increased the ping timeout in update_qspi_remote.sh to 40 sec, pulled in latest changes from original file 
(https://gitlab.drao.nrc.ca/digital-systems/talon-dx/operating-system/talon-dx-utilities/-/blob/main/bin/update_qspi_remote.sh)

2023-Nov-20
***********
* CIP-1791 Timescale database deployed to ska-eda-mid-db namespace
* CIP-1792 Optionally deploy EDA to system tests if SKA_TANGO_ARCHIVER is true 

2023-Nov-17
***********
* CIP-1844 SV is updated to 0.2.4, emif_amm_ready criteria for talon_status is removed from checkpoints file and fraction_of_data check is added back in for talon2,talon3, and talon4
***********
* CIP-1764 Calls validation against the Mid.CBF InitSysParams command at the start of the BDD test session.

2023-Oct-17
***********
* CIP-1780 Resets CBF subarray to EMPTY between Signal Chain Verification tests using abort pathway
* CIP-1758 Add functionality to handle multiple talon boards per test id. Only wb_state_count check passing.

2023-Sept-19
************
* CIP-1817 Added device-check ping test for Tango devices after mcs-on command.
