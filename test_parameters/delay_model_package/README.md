## Delay Model Package Overview

Delay model packages contain information about delay models to be used in the signal processing chain. They are generated from the delay model csv files in the [delay_model_csv folder](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters/delay_models_csv), which additionally have information about the delay models to be used in BITE (see the [README](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters/delay_models_csv/README.md) in that folder for more information).

Note that the json files in the `delay_model_package` folder and the folders of csv models in the `delay_model_csv` folder are 1:1 and that manually modifying the delay model package files may cause the delay models to grow out of sync between BITE and the signal chain. The `4_VCC_T3` package has been intentionally modified to remove the second delay model as the delay model test requires only one.
