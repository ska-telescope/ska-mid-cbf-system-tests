#!/usr/bin/env python3

import argparse
import json
import os
import pprint
import sys

import jsonref

sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
TEST_PARAMS_DIR = os.path.dirname(os.path.abspath(__file__))
FEATURES_DIR = os.path.join(os.path.dirname(TEST_PARAMS_DIR), "tests/features")

TEST_DATA_DIR = os.path.join(TEST_PARAMS_DIR, "cbf_input_data")
BITE_CONFIG_DIR = os.path.join(TEST_DATA_DIR, "bite_config_parameters")
DELAY_MODEL_PACKAGE_DIR = os.path.join(TEST_PARAMS_DIR, "delay_model_package")
DELAY_MODEL_CONFIG_DIR = os.path.join(TEST_PARAMS_DIR, "delay_model_config")
ASSIGN_RESOURCES_DIR = os.path.join(TEST_PARAMS_DIR, "assign_resources")
RELEASE_RESOURCES_DIR = os.path.join(TEST_PARAMS_DIR, "release_resources")
CONFIGURE_SCAN_DIR = os.path.join(TEST_PARAMS_DIR, "configure_scan")
SCAN_DIR = os.path.join(TEST_PARAMS_DIR, "scan")
CHECKPOINTS_DIR = os.path.join(TEST_PARAMS_DIR, "checkpoints")

TEST_PARAMS_DIRS = [
    TEST_PARAMS_DIR,
    TEST_DATA_DIR,
    BITE_CONFIG_DIR,
    DELAY_MODEL_PACKAGE_DIR,
    DELAY_MODEL_CONFIG_DIR,
    ASSIGN_RESOURCES_DIR,
    RELEASE_RESOURCES_DIR,
    CONFIGURE_SCAN_DIR,
    SCAN_DIR,
    CHECKPOINTS_DIR,
]


def get_bite_config(bite_config_id):
    file = os.path.join(BITE_CONFIG_DIR, "bite_configs.json")
    with open(f"{file}", "r") as fd:
        configs = json.load(fd)["bite_configs"]

    if bite_config_id in configs:
        config = configs[bite_config_id]
        for source in config["sources"]:
            filter_x_id = source["gaussian"]["pol_x"]["filter"]
            filter_x = get_config("filters", filter_x_id, BITE_CONFIG_DIR)
            filter_y_id = source["gaussian"]["pol_y"]["filter"]
            filter_y = get_config("filters", filter_y_id, BITE_CONFIG_DIR)
            source["gaussian"]["pol_x"]["filter"] = {filter_x_id: filter_x}
            source["gaussian"]["pol_y"]["filter"] = {filter_y_id: filter_y}
        return config

    print(f"'{bite_config_id}' not found in {file}")
    return None


def get_cbf_input_data(cbf_input_data_id):
    file = os.path.join(TEST_DATA_DIR, "cbf_input_data.json")
    with open(f"{file}", "r") as fd:
        cbf_input_data = json.load(fd)["cbf_input_data"]

    if cbf_input_data_id in cbf_input_data:
        data = cbf_input_data[cbf_input_data_id]
        for receptor in data["receptors"]:
            bite_config_id = receptor["bite_config_id"]
            bite_config = get_bite_config(bite_config_id)
            receptor["bite_config_id"] = {bite_config_id: bite_config}
        return data

    print(f"'{cbf_input_data_id}' not found in {file}")
    return None


def get_config(config_name, config_id, config_dir):
    file = os.path.join(config_dir, f"{config_name}.json")
    with open(f"{file}", "r") as fd:
        param_config = json.load(fd)[config_name]

    if config_id in param_config:
        return param_config[config_id]

    print(f"'{config_id}' not found in {file}")
    return None


def generate_test_parameters_section(test_id):
    """Returns the parameter section for a given test id"""
    print(f"Generating test parameter section for test id: '{test_id}'...")
    test_summary = get_config("tests", test_id, TEST_PARAMS_DIR)
    try:
        # get cbf_input_data
        cbf_input_data_id = test_summary["cbf_input_data"]
        cbf_input_data = get_cbf_input_data(cbf_input_data_id)
        test_summary["cbf_input_data"] = cbf_input_data

        # get delay_model_config
        delay_model_config_id = test_summary["delay_model_config"]
        delay_model_config = get_config(
            "delay_model_config",
            delay_model_config_id,
            DELAY_MODEL_CONFIG_DIR,
        )
        test_summary["delay_model_config"] = delay_model_config

        # get assign_resources
        assign_resources_id = test_summary["assign_resources"]
        assign_resources = get_config(
            "assign_resources", assign_resources_id, ASSIGN_RESOURCES_DIR
        )
        test_summary["assign_resources"] = assign_resources

        # get configure_scan
        scan_config_id = test_summary["configure_scan"]
        scan_config = get_config(
            "configure_scan", scan_config_id, CONFIGURE_SCAN_DIR
        )
        test_summary["configure_scan"] = scan_config

        # get scan
        scan_id = test_summary["scan"]
        scan = get_config("scan", scan_id, SCAN_DIR)
        test_summary["scan"] = scan

        # get checkpoints
        checkpoints_id = test_summary["checkpoints"]
        checkpoints = get_config(
            "checkpoints", checkpoints_id, CHECKPOINTS_DIR
        )
        test_summary["checkpoints"] = checkpoints

        # get release_resources
        release_resources_id = test_summary["release_resources"]
        release_resources = get_config(
            "release_resources", release_resources_id, RELEASE_RESOURCES_DIR
        )
        test_summary["release_resources"] = release_resources

        return test_summary
    except Exception:
        print(
            f"Error: Couldn't generate test parameter section for test id: '{test_id}'"
        )


def generate_test_parameters_json(
    test_ids=[],
    directory=TEST_PARAMS_DIR,
):
    # data structure for holding test parameters which will be written to file
    test_parameters = {}

    if test_ids == []:
        file = os.path.join(directory, "test_parameters.json")
        if os.path.exists(file):
            os.remove(file)
        tests_file = f"{TEST_PARAMS_DIR}/tests.json"
        with open(f"{tests_file}", "r") as fd:
            tests = jsonref.load(fd)["tests"]
        for test_id, _ in tests.items():
            test_parameters[test_id] = generate_test_parameters_section(
                test_id
            )
    else:
        file = os.path.join(directory, "test_parameters_subset.json")
        if os.path.exists(file):
            os.remove(file)
        for test_id in test_ids:
            test_parameters[test_id] = generate_test_parameters_section(
                test_id
            )

    # generate the test parameters json file
    test_json = json.dumps(test_parameters, indent=2)
    with open(f"{file}", "a") as fd:
        fd.write(test_json)

    print(f"{file} generated")


def find_unused_test_params():
    """
    Prints out all unused parameters if found

    :returns: None
    """
    # Flag to indicate if any unused parameters are found
    found_unused_params = False

    # Find all unused tests.json parameters
    used_test_params = get_used_params("tests", TEST_PARAMS_DIR)
    param_dirs = {
        "assign_resources": ASSIGN_RESOURCES_DIR,
        "cbf_input_data": TEST_DATA_DIR,
        "checkpoints": CHECKPOINTS_DIR,
        "configure_scan": CONFIGURE_SCAN_DIR,
        "delay_model_config": DELAY_MODEL_CONFIG_DIR,
        "release_resources": RELEASE_RESOURCES_DIR,
        "scan": SCAN_DIR,
    }
    for param, dir in param_dirs.items():
        found_unused_params = (
            find_and_print_unused_params(
                used_test_params,
                param,
                dir,
                f"Found {param} parameter(s) unused by tests.json",
            )
            or found_unused_params
        )

    # Find all unused bite_configs in cbf_input_data.json
    used_bite_configs = get_used_params("cbf_input_data", TEST_DATA_DIR)
    found_unused_params = (
        find_and_print_unused_params(
            used_bite_configs,
            "bite_configs",
            BITE_CONFIG_DIR,
            "Found bite_configs block(s) unused by cbf_input_data.json",
        )
        or found_unused_params
    )

    # Find all unused filters in bite_configs.json
    used_filters = get_used_params("bite_configs", BITE_CONFIG_DIR)
    found_unused_params = (
        find_and_print_unused_params(
            used_filters,
            "filters",
            BITE_CONFIG_DIR,
            "Found filters block(s) unused by bite_configs.json",
        )
        or found_unused_params
    )

    # Find all unused delay model package files
    used_dm_files = []
    with open(f"{DELAY_MODEL_CONFIG_DIR}/delay_model_config.json", "r") as f:
        config = json.load(f)["delay_model_config"]
        for config in config.values():
            used_dm_files.append(config["source_file"])

    unused_files = []
    for file in os.listdir(DELAY_MODEL_PACKAGE_DIR):
        if file.endswith(".json") and file not in used_dm_files:
            unused_files.append(file)

    if len(unused_files) != 0:
        message = "Found delay model file(s) unused by delay_model_config.json"
        print(f"\n{message}:\n{pprint.pformat(unused_files)}\n")

    # Find all unused tests in feature files
    used_tests = get_used_tests()
    found_unused_params = (
        find_and_print_unused_params(
            used_tests,
            "tests",
            TEST_PARAMS_DIR,
            "Found test(s) unused by any feature files",
        )
        or found_unused_params
    )

    # Print if no unused parameters are found
    if not found_unused_params:
        print("\nNo unused parameters found.\n")


def find_and_print_unused_params(
    used_params: str, param_name: str, param_dir: str, message: str
):
    """
    Prints out all unused parameters if found and indicates if any unused parameters are found

    :used_params: hashmap of used parameters in parent file
    :param_key: name of the parameter file
    :param_dir: directory of the parameter file, must correspond to the param_name
    :message: message to print if unused parameters are found
    :returns: True if unused parameters are found, False otherwise

    """
    unused_params = find_unused_params(used_params, param_name, param_dir)
    if len(unused_params) != 0:
        print(f"\n{message}:\n{pprint.pformat(unused_params)}\n")
        return True
    return False


def find_unused_params(used_params: str, param_name: str, param_dir: str):
    """
    Find unused parameters in a given parameter file

    :used_params: hashmap of used parameters in parent file
    :param_name: name of the parameter file
    :param_dir: directory of the parameter file, must correspond to the param_name
    :return: list of unused parameters
    """
    file = os.path.join(param_dir, f"{param_name}.json")
    with open(f"{file}", "r") as fd:
        config_json = json.load(fd)[param_name]
        unused_params = []
        # Find unused parameters by comparing used_params with the parameters in the file
        for param in config_json:
            if param not in used_params:
                unused_params.append(param)
    return unused_params


def get_used_params(file_name: str, file_dir: str):
    """
    Get all used parameters from a tests/cbf_input_data/bite_congigs file

    :file_name: name of the file, must be one of "tests", "cbf_input_data", "bite_configs"
    :file_dir: directory of the file, must correspond to the file_name
    :returns: hashmap of used parameters
    """
    used = {}
    file = os.path.join(file_dir, f"{file_name}.json")
    with open(f"{file}", "r") as fd:
        config_json = json.load(fd)[file_name]
        params = list(config_json.keys())
        for param in params:
            if file_name == "tests":
                # Get all test.json parameters
                tests = config_json[param]
                for test in tests.values():
                    used[test] = True
            elif file_name == "cbf_input_data":
                # Get all cbf_input_data.json parameters
                receptors = config_json[param]["receptors"]
                for receptor in receptors:
                    bite_config_id = receptor["bite_config_id"]
                    used[bite_config_id] = True
            elif file_name == "bite_configs":
                # Get all bite_configs.json parameters
                sources = config_json[param]["sources"]
                for source in sources:
                    filter_x_id = source["gaussian"]["pol_x"]["filter"]
                    filter_y_id = source["gaussian"]["pol_y"]["filter"]
                    used[filter_x_id] = True
                    used[filter_y_id] = True
            else:
                print(
                    f"File name given ({file_name}) is not supported by get_used_param's parsing. Exiting."
                )
    return used


def get_used_tests():
    """
    Get all used tests from feature files

    :returns: hashmap of used tests
    """
    used = {}
    for feature_name in os.listdir(FEATURES_DIR):
        feature_path = os.path.join(FEATURES_DIR, feature_name)
        with open(feature_path, "r") as file:
            feature = file.read()

            # Flag to indicate if the parser is in the examples section
            # (where used tests are listed at the bottom of the feature file)
            in_examples = False

            # Iterate through each line after the "Examples:" line and add listed tests to used
            for line in feature.split("\n"):
                if in_examples and line.strip():
                    # Split the line by '|' and add each test to used if it's not empty
                    tests = line.split("|")
                    for test in tests:
                        test_id = test.strip()
                        if test_id:
                            used[test_id] = True
                elif "Examples:" in line:
                    in_examples = True
    return used


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-t",
        action="store_true",
        help="optional flag to generate the test parameters json file. Note that at least one of -t or --unused_params must be specified.",
    )
    parser.add_argument(
        "-o",
        dest="output_dir",
        nargs="?",
        default=None,
        help=f"optional argument to specify the output directory to save the test parameters json file; the default output directory is {os.getcwd()}",
    )
    parser.add_argument(
        "--test_ids",
        dest="test_ids",
        nargs="?",
        default=None,
        help="optional argument to specify the test ids for which to generate the test parameters json file. "
        + "Specify as a comma-separated list with no spaces. For example, add the following argument: "
        + "--test_ids 'Test 1,Test 2,Test 3' to generate the test parameters json file and/or feature file for Test 1, Test 2, and Test 3 only.",
    )
    parser.add_argument(
        "--unused_params",
        action="store_true",
        help="optional flag to find unused parameters in the test parameters json file. Note that at least one of -t or --unused_params must be specified.",
    )

    args = parser.parse_args()
    test_ids = []
    if args.test_ids:
        test_ids = args.test_ids.split(",")
    if args.unused_params:
        find_unused_test_params()

    if args.t:
        if args.output_dir is None:
            generate_test_parameters_json(test_ids=test_ids)
        else:
            generate_test_parameters_json(
                directory=args.output_dir, test_ids=test_ids
            )

    if (not args.t) and (not args.unused_params):
        print(
            "No flags provided: specify -t to generate the test parameters json file or --unused_params to check for unused parameters in the test parameters json file."
        )
        print(
            "Specify -h or --help to list all options for generating the test parameters json file and/or feature file."
        )
