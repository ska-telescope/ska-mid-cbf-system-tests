# Convenience script to help calculate the number of channels needed to cover certain frequency ranges.
# Also developed to help remind designers to be aware of both frequency slice and band edges when determining frequency ranges and number of FSPs required.
# How-to: adjust the start and end frequencies and run the script.
# Note: possible duplicate of whatever Devan's got and maybe should be removed.

# digitized bw edge frequencies taken from table here: https://confluence.skatelescope.org/x/sHHYDw
# digitized bandwidth edges in hz
fs_edges = [
    -99090432,
    99090432,
    297271296,
    495452160,
    693633024,
    891813888,
    1089994752,
    1288175616,
    1486356480,
    1684537344,
    1882718208,
]

ch_bw = 13440  # hz

band_1 = [350000000, 1050000000]
band_2 = [950000000, 1760000000]

# you can set start and end frequency to whatever you want, fsp and band values above for convenience
start_freq = band_2[0]
end_freq = fs_edges[9]
bw = end_freq - start_freq


print(f"start_freq: {start_freq}")
print(f"end_freq: {end_freq}")
print(f"bw: {bw}")
chans = bw / ch_bw
chan_groups = int(chans / 20)
chans_rounded = chan_groups * 20

print(f"number of chans are : {chans}")
print(f"rounding to {chans_rounded} for {chan_groups} groups of 20 channels")

print("\nSUMMARY of useful values for configure scan specification:")
print(f"start_freq: {start_freq}")
print(f"channel_count: {chans_rounded}")
print(f"number of ports required: {chan_groups}")
