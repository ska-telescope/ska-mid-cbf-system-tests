# quick script for aiding in generating the output_port in a way that is easy and quick to copy to the json file
import numpy as np

sdp_channel_offset = 0
channel_count = 14880  # adjust based on your scan config
chan_group_size = 20  # fixed
port_start = 21000
# note: ports assumed to increment by 1 per group of 20 channels via the enumerate

data = []

for port_inc, chan in enumerate(
    range(
        0 + sdp_channel_offset,
        channel_count + sdp_channel_offset,
        chan_group_size,
    )
):
    data.append([chan, port_start + port_inc])

disp_num = 7  # adjusts how many output_port entries are displayed on one line for easy copy/paste
count = 0
while count < len(data):
    print(str(data[count : count + disp_num])[1:-1] + ",")
    count = count + disp_num
print(str(data[count:])[1:-1])

print(
    "sanity check, expect last port to be :",
    port_start + np.ceil(channel_count / chan_group_size) - 1,
)
