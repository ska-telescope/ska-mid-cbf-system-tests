#!/usr/bin/env python3
import argparse
import copy
import csv
import json
import logging
import os
import pathlib
import re

from test_parameters import test_parameters_validation

# TODO: CIP-2810 Replace with "from lib.constant import LOG_FORMAT"
LOG_FORMAT = "[%(asctime)s|%(levelname)s|%(filename)s#%(lineno)s] %(message)s"
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
_logger = logging.getLogger(__name__)


def convert_delay_models(
    out_file_full_path: str,
    csv_dir_path: str,
    dish_parameter_mapping: str,
) -> None:
    """Convert delay models from CSV to JSON format
    Args:
    : out_file_full_path: full path for the output JSON file
    : csv_dir_path: full path to the directory containing delay model CSV files
    : dish_parameter_mapping: name of the init_sys_param definition for vcc to dish ID mapping.
    Assumptions:
    1. Each _polX.csv file in csv_dir_path has a corresponding _polY.csv file
    2. Each file in csv_dir_path has the same number of rows (models) and each row
       has the same t_start and t_stop values.
    3. CSV file header format: t_start[s],t_stop[s],c6[ns/s^6],...,c1[ns/s^1],c0[ns]
    4. The delay model for a receptor is a 5th order polynomial (6 coefficients),
       so c6 is not used in the X polarization coefficient calculation.
    5. ypol_offset_ns = (y_polynomial c0[ns] - x_polynomial c0[ns]) for a given model,
       so only c0 is used in the Y polarizatoin offset calculation.
    6. dm_template contains the correct interface, config_id, subarray fields.
       These values will not be overwritten.
    7. dm_template contains exactly the receptors that should be generated.
       Each receptor in the template file will be included in the package file,
       even if it has no corresponding receptor CSV file.
    8. At least 2 models must be specified in the CSV files
    9. The models (rows) are evenly spaced in time. The difference in start times for
       the first and second model is used as the cadence for all models.
    """
    models = []

    cwd = os.getcwd()
    proj_name = "ska-mid-cbf-system-tests"
    proj_root = cwd[0 : cwd.find(proj_name) + len(proj_name)]

    test_params_dir = os.path.join(proj_root, "test_parameters")
    delay_model_package_dir = os.path.join(
        test_params_dir, "delay_model_package"
    )
    dm_template = os.path.join(delay_model_package_dir, "dm_template.json")

    # find x and y receptor coefficient CSV files
    x_files = list(pathlib.Path(csv_dir_path).glob("*/*_polX.csv"))
    y_files = list(pathlib.Path(csv_dir_path).glob("*/*_polY.csv"))

    if len(x_files) != len(y_files):
        raise RuntimeError(
            f"there should be the same number of x-polynomial and y-polynomial CSV files (found {len(x_files)} and {len(y_files)} respectively)"
        )

    for i, x_file in enumerate(x_files):
        y_file = y_files[i]

        x_models = models_from_csv_file(x_file)
        y_models = models_from_csv_file(y_file)

        # fill models list once with the correct number of model templates
        if not models:
            with open(dm_template) as dm_template:
                dm_template_json = json.load(dm_template)
            for _ in x_models:
                models.append(copy.deepcopy(dm_template_json))

        # get dish ID from the vcc receptor number
        x_name = os.path.basename(x_file)
        receptor = re.search(r"Receptor(\d+)", x_name).group(1)

        with open(
            os.path.join(test_params_dir, "init_sys_param.json"), "r"
        ) as f:
            init_sys_json = json.load(f)
        dish_parameters = init_sys_json["init_sys_param"][
            dish_parameter_mapping
        ]["dish_parameters"]

        for id, val in dish_parameters.items():
            if val["vcc"] == int(receptor):
                _logger.info(
                    f"mapped receptor vcc number {receptor} to dish ID {id}"
                )
                dish_id = id
                break

        _logger.info(
            f"converting delay models from CSV to JSON for files: {x_name} and {os.path.basename(y_file)}"
        )

        coefficients = coefficients_from_models(x_models)
        save_delay_model_coefficients(coefficients, models, dish_id)

        y_offsets = y_offsets_from_models(x_models, y_models)
        save_y_offsets(y_offsets, models, dish_id)

        # cadence, start validity, and validity period only need to be updated once per model
        if i == 0:
            cadences = cadence_from_models(x_models)
            save_cadences(cadences, models)
            save_start_validity(
                models,
                os.path.join(csv_dir_path, "../dm_base_time.json"),
                x_models,
            )
            save_validity_period_sec(models, x_models)

    with open(out_file_full_path, "w") as dm_json_file:
        json.dump({"models": models}, dm_json_file, indent=4)

    # validate delay models after CSV to JSON conversion
    for model in models:
        test_parameters_validation.validate_telmodel_config(
            "delay_model_package", model["model"]
        )


def models_from_csv_file(full_path: str) -> list:
    """Extracts delay models from a CSV file into a list of dict objects
    Args:
    : full_path: path to delay models CSV file
    """
    models = []
    with open(full_path, newline="") as f:
        reader = csv.DictReader(f)
        for row in reader:
            models.append(row)
    return models


def cadence_from_models(models: list) -> float:
    """Calculates cadence values for a list of delay model objects
    Args:
    : models: list of delay model objects
    Assumptions:
    At least 2 models must be specified in the CSV files
    The models (rows) are evenly spaced in time. The difference in start times for
        the first and second model is used as the cadence for all models.
    """
    if models and len(models) >= 2:
        second_model_start = float(models[1]["t_start[s]"])
        first_model_start = float(models[0]["t_start[s]"])
        cadence = second_model_start - first_model_start
    return cadence


def save_cadences(cadence: float, models: list) -> None:
    """Updates models' cadence_sec fields with value from cadence
    Args:
    : cadence: a cadence value
    : models: list of models to update
    """
    _logger.info(f"cadence_sec for all delay models is {cadence}")
    for model in models:
        model["model"]["cadence_sec"] = cadence


def coefficients_from_models(models: list) -> list:
    """Extracts coefficients from a list of delay model objects
    Returns a list of coefficients for each model
    Args:
    : models: list of delay model objects
    """
    num_coefficients = 6
    coeff_for_models = []

    for i, model in enumerate(models):
        ordered = dict(reversed(model.items()))
        ordered_values = list(ordered.values())
        model_coefficients = []
        for i in range(num_coefficients):
            model_coefficients.append(float(ordered_values[i]))
        coeff_for_models.append(model_coefficients)

    return coeff_for_models


def save_delay_model_coefficients(
    coeff_for_models: list,
    models: list,
    receptor: str,
) -> None:
    """Saves updated delay model coefficients in a delay models JSON file
    Args:
    : coeff_for_models: a list of xy_pol_coeffs_ns (coefficients) for each model in the CSV
    : models: the models to update
    : receptor: the coeff_for_models apply to this receptor (not all receptors in the model)
    """
    for index, cgroup in enumerate(coeff_for_models):
        for delay in models[index]["model"]["receptor_delays"]:
            if delay["receptor"] == receptor:
                delay["xypol_coeffs_ns"] = cgroup
                _logger.info(
                    f"updated coefficients {cgroup} for model[{index}] and receptor {receptor}"
                )


def y_offsets_from_models(x_models: list, y_models: list) -> list:
    """Calculates ypol_offset_ns (offset of polarization Y with respect to polarization X)
        by comparing two lists of delay model objects
    Returns the y offset
    Args:
    : x_models: list of x-polynomial delay model objects
    : y_models: list of y-polynomial delay model objects
    """
    y_offsets = []
    for i, x_model in enumerate(x_models):
        y_model = y_models[i]

        ordered_x = dict(reversed(x_model.items()))
        ordered_x_values = list(ordered_x.values())
        ordered_y = dict(reversed(y_model.items()))
        ordered_y_values = list(ordered_y.values())

        y_offset = float(ordered_y_values[0]) - float(ordered_x_values[0])
        y_offsets.append(y_offset)
    return y_offsets


def save_y_offsets(
    y_offsets: list,
    models: list,
    receptor: str,
) -> None:
    """Updates models' ypol_offset_ns fields with values from y_offsets
    Args:
    : y_offsets: constant delay offsets for y-polarizations with respect to x-polarizations
    : models: models to update
    : receptor: the y offsets apply to this receptor only (not all receptors in the models)
    """
    for idx, model in enumerate(models):
        for delay in model["model"]["receptor_delays"]:
            if delay["receptor"] == receptor:
                delay["ypol_offset_ns"] = y_offsets[idx]
                _logger.info(
                    f"updated y offset {y_offsets[idx]} for model[{idx}] and receptor {receptor}"
                )


def save_start_validity(
    models_obj: list, path_to_base_time: str, models_csv: list
) -> None:
    """Updates models' start_validity_sec fields
    Args:
    : models: list of models to update
    """
    with open(path_to_base_time, "r") as base_time_file:
        base_time_json = json.load(base_time_file)
    base_time = float(base_time_json["base_time"])
    for index, model in enumerate(models_obj):
        start_validity = base_time + float(models_csv[index]["t_start[s]"])
        model["model"]["start_validity_sec"] = start_validity

        _logger.info(
            f"start_validity_sec for model at index {index + 1} is {start_validity}"
        )


def save_validity_period_sec(models_obj: list, models_csv: list) -> None:
    """Updates models' validity_period_sec fields based on their t_start and t_stop values
    Args:
    : models_obj: list of models to update
    : models_csv: list of models obtained from CSV format (has t_start and t_stop values)
    """
    for i, model in enumerate(models_obj):
        validity = float(models_csv[i]["t_stop[s]"]) - float(
            models_csv[i]["t_start[s]"]
        )
        model["model"]["validity_period_sec"] = validity
        _logger.info(
            f"validity_period_sec for model at index {i} is {validity}"
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Convert delay models from CSV to JSON format
    Assumptions:
    1. Each _polX.csv file in csv_dir_path has a corresponding _polY.csv file
    2. Each file in csv_dir_path has the same number of rows (models) and each row
       has the same t_start and t_stop values.
    3. CSV file header format: t_start[s],t_stop[s],c6[ns/s^6],...,c1[ns/s^1],c0[ns]
    4. The delay model for a receptor is a 5th order polynomial (6 coefficients),
       so c6 is not used in the X polarization coefficient calculation.
    5. ypol_offset_ns = (y_polynomial c0[ns] - x_polynomial c0[ns]) for a given model,
       so only c0 is used in the Y polarizatoin offset calculation.
    6. dm_template contains the correct interface, config_id, subarray fields.
       These values will not be overwritten.
    7. dm_template contains exactly the receptors that should be generated.
       Each receptor in the template file will be included in the package file,
       even if it has no corresponding receptor CSV file.
    8. At least 2 models must be specified in the CSV files
    9. The models (rows) are evenly spaced in time. The difference in start times for
       the first and second model is used as the cadence for all models.
        """,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "--out-file-full-path",
        help="Path to the output JSON file that will be created",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--csv-dir-path",
        help="Path to the gdm_sig_proc directory containing the delay model CSV files, which will be used to generate the JSON file",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--dish-parameter-mapping",
        help="Name of the definition in test_parameters/init_sys_param.json that will be used for vcc to dish ID mapping",
        required=True,
        type=str,
    )
    args = parser.parse_args()

    _logger.info(parser.description)
    convert_delay_models(
        out_file_full_path=args.out_file_full_path,
        csv_dir_path=args.csv_dir_path,
        dish_parameter_mapping=args.dish_parameter_mapping,
    )
