## Delay Model CSV Overview

Delay model CSV files are generated from the [NRC mid-cbf-signal-proc-model](https://gitlab.drao.nrc.ca/SKA/Mid.CBF/prototype/mid-cbf-signal-proc-model/-/tree/delay-models-gen-main-branch) repo.

Two types of csv files are generated:

* `gdm_sig_gen` folder csv files to be used in the BITE signal generation
* `gdm_sig_proc` folder csv files to be used in the signal processing chain

The `gdm_sig_proc` file is converted into a delay model package json file using the `delay_models_csv_to_json.py` file. The dm package files live in the [delay_model_package](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters/delay_model_package) folder and are referenced in the [delay_model_config.json](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/delay_model_config/delay_model_config.json) for use in the tests.
