# This script reads through the tests.json file (adjust tests_file_path as necessary)
# and compiles a dictionary of {req: [Tests with that requirement mentioned in their description]}.

# Use the description_tag to further sort which tests you check; only test descriptions
# with that tag present will be searched. Eg. search only for tests used in AA0.5 by
# specifying the description_tag as [AA0.5].

import json
import os
import re

py_dir_path = os.path.dirname(os.path.abspath(__file__))
tests_file_path = os.path.join(py_dir_path, "..", "tests.json")

# prefix used to find references to the requiements in the test description
req_prefix = "SKAO-CSP_Mid_CBF_REQ-"
req_prefix_len = len(req_prefix)

# add a tag that must be present in the description for it to check what requirements are being used
# specify 'None' to have no tag
description_tag = None  # try "[AA0.5]" or "[AA1]" to check only those tests

with open(tests_file_path, "r") as file:
    tests = json.load(file)["tests"]

reqs = {}

for test_id, test in tests.items():
    if description_tag is None or description_tag in test["description"]:
        req_strs = [
            req.group()
            for req in re.finditer(req_prefix + r"\d+", test["description"])
        ]
        for req in req_strs:
            if req in reqs.keys() and test_id not in reqs[req]:
                reqs[req].append(test_id)
            else:
                reqs[req] = [test_id]


# can adjust how they are printed out as desired. this is just for nice easy visual
req_nums = sorted(
    [int(re.findall(r"\d+", req)[0]) for req in reqs.keys()]
)  # sort requirements by number
req_str_len = max([len(req_key) for req_key in reqs.keys()])
for req_num in req_nums:
    req = req_prefix + str(req_num)
    print(f"{(req+':').ljust(req_str_len+1, ' ')}  {sorted(reqs[req])}")
