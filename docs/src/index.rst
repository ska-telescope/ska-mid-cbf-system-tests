.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to ska-mid-cbf-system-tests' documentation!
===========================================================

.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

.. Main Repository README =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Main Repository README

   main_README.md

.. Test Parameters README =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Test Parameters README

   test_param_README.md

.. Log Utilities README =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Log Utilities README

   log_utils_README.md
